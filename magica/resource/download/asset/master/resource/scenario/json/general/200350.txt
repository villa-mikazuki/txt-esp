¡F-Feliz Año Nuevo!
¿Me veo bien? 
N-Nunca había salido con@un kimono antes, así que...
Good morning!
Hoy voy a jugar al hanetsuki con Madoka@y las demás. Me levanté temprano para@no llegar tarde... tal vez muy temprano...
Pastel de arroz al horno,@sopa de pastel de arroz y@otros platos de Año Nuevo...
Siempre pasaba el Año Nuevo en la cama@del hospital, así que se siente diferente...
El sol se pone muy temprano en invierno...
Por favor, abrígate bien antes de salir@para no resfriarte.
Sigo diciéndome esto a mí misma@antes de dormir, desde hace años...
Nunca olvides por qué te convertiste@en una Chica Mágica, o ese juramento@que hiciste...
Madoka ha de verse tan hermosa@en su kimono...
¡Oh, um, lo siento! Solo estaba, ah...@soñando despierta un poco...
U-Um, Feliz Año Nuevo...
M-Me alegro de que nos hayamos@conocido así...
¡Trabajemos d-duro para acabar@con esas Brujas!
Como estamos en año nuevo, se pueden ver@muchas Chicas Mágicas vistiendo kimonos@incluso dentro del Laberinto de los Espejos...
Aunque son copias, claro...
¿¡Qué!? ¿¡C-Crees que debería quitarme@los lentes mientras uso mi kimono...!?
¡N-No! Me pongo muy nerviosa@cuando me los quito...
Oh, ahora que lo mencionas... Esa paleta@de hanetsuki que tiene Madoka...
Tiene un diseño de Kyubey, ¿no?
Cuando estaba en el hospital, nunca habría@imaginado ponerme un kimono con@mis amigas para celebrar el Año Nuevo...
Está bien simplemente disfrutarlo, ¿no?
Este kimono... ¿se ve bien?
Madoka dice que me queda bien,@pero... no sé...
Madoka me da valor con su fuerza y@positividad que yo nunca tuve.
Tan diferente a mí...@Yo solo soy una tonta, egoísta y@asustadiza gatita...
Kyoko seguramente está pasando el@Año Nuevo como cualquier otro día...
Pero que ella sea así es la razón por la@que puede hablar con tanta calma y@serenidad en cualquier situación difícil.
Pasé mucho tiempo en el hospital,@así que pasear por todos esos puestos@del santuario fue muy divertido...
Quizás porque Madoka y las demás@también estaban allí.
Quiero mantener estos recuerdos que@hice con Madoka cerca de mi corazón...
Así que no me desviaré de mi camino,@no importa cuántas veces@salte en el tiempo.
¡D-Deja de hacer eso!
¡Si quieres pellizcar las mejillas de alguien,@deberías ir a buscar a Kyubey!
