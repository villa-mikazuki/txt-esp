Mitama: ¿El creciente número de rumores@en la ciudad de Kamihama?
Iroha: Sí. Pensé que podrías saber@algo sobre ellos, Mitama-san.
Mitama: Hmm...
Mitama: ¿Rumores como que la Coordinadora@da descuentos a las chicas lindas?
Iroha: Ehh... ¿En serio?
Mitama: No... No puedo burlarme de ti cuando@te crees mis bromas de esa manera...
Iroha: ¡Cielos!@¡Estoy preguntando en serio!
Mitama: Fufu, lo siento.@No te enfades de esa manera.
Mitama: En realidad, he estado escuchando@un montón de rumores últimamente.
Mitama: ¿Pero qué te hace preguntar eso de repente?
Iroha: Um...
Iroha: Es que estoy preocupada por la@[textRed:Regla de la Ruptura de Amistades].
Mitama: Yo también he oído hablar de esa.
Iroha: [chara:100102:effect_emotion_surprise_0][se:7226_shock]¿En serio?
Iroha: Lo pregunto porque las amigas de Momoko-san...
Mitama: ¿Supongo que Rena-chan y Kaede-chan@se han vuelto a pelear?
Iroha: Ah, sí. Lo hicieron.
Iroha: Y las dos dijeron, "[textRed:Hemos terminado]".
Iroha: Le conté a Momoko sobre la@[textRed:Regla de la Ruptura de Amistades].
Iroha: Pero ella no me cree, así que...
Iroha: Así que solo espero que no pase nada malo.
Mitama: Hmm... Dudo que puedas conseguir@que Momoko te crea.
Iroha: ¿No le gustan los rumores?
Mitama: No, ella no lo creerá porque es Yachiyo-san@quien está haciendo un gran problema con eso.
Mitama: Es como tratar de mezclar aceite y agua,@o echar leña al fuego...
Mitama: Ya sabes, algo así.
Mitama: Es más la fuente que la información@lo que no cree.
Iroha: Así que ellas dos realmente no se@llevan bien.
Mitama: Escuché que solían ser buenas amigas@cuando eran más jóvenes.
Mitama: Pero Momoko ha tenido una actitud muy mala@últimamente.
Mitama: "Yachiyo-san ha estado actuando extraña.@Se ha vuelto egoísta."
Mitama: "Actúa como una de esas modelos de moda".
Mitama: "Es injusto que sea tan fuerte,@y que tenga una figura tan delgada".
Mitama: "Pelo tan brillante y sedoso,@sin mencionar que no tiene las puntas abiertas".
Mitama: "¡Piel tan suave y flexible,@como la de un bebé demonio!"
Mitama: ... Y así sigue y sigue.
Iroha: No se sabe si la está alabando@o piensa que son malas cualidades.
Mitama: Bueno, ya que solían llevarse bien,@probablemente sea lo segundo.
Iroha: Hmm...
Iroha: (Las dos llevándose bien...@... nunca me lo hubiese imaginado.)
