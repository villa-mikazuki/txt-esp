Mayu: Qu... ¡¿Qué está pasando?!
Aimi: ¡No puedo creer que las cosas se hayan desviado@tanto mientras estábamos en la tienda de la@coordinadora!
Mitama: Menos mal que salieron.
Rika: Sé que solo estuve llorando, y aún me siento@fatal, pero lo hecho, hecho está.
Rika: Quiero atesorar los recuerdos que hice al@convertirme en Chica Mágica...
Rika: Eso es lo que he decidido.
Aimi: Sí... Sentí que todo lo que me pasó fue@completamente horrible...
Aimi: Pero cuando lo pienso bien, no es así en absoluto.
Aimi: No tendría ni la mitad de mis experiencias y@recuerdos si no me hubiera convertido en una@Chica Mágica.
Aimi: ¡Y Kamihama está lleno de esos recuerdos!
Kokoro: Sí, por eso era inevitable que me convirtiera@en Chica Mágica en algún momento.
Kokoro: He decidido pensar en ello como la mejor@elección que podría haber hecho entonces.
Ren: Sí... así no se estropeará todo el tiempo que@pasamos, viviendo nuestras vidas...
Mayu: ¡Así que tenemos que ayudar!
Mayu: Um, ¿Mitama? ¿Qué debemos hacer?
Mitama: *Risita* Es reconfortante tener a tantas de@ustedes aquí para ayudar.
Mitama: Bien entonces, pueden empezar por cazar a todos@esos Familiares.
Mitama: Hay demasiados para que solo mi equipo pueda@acabar con ellos.
