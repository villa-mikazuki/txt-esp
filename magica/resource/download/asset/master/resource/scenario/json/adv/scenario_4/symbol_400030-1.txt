Nanaka: Saludos.
Nanaka: Gracias por la presentación, Kako. Soy yo...
Nanaka: ¡Nanaka Tokiwa~!
Leila: ¿Nanaka Tokiwa...?@Siento que la he conocido en alguna parte...
Mito: ¡Oh! ¡La conocimos en los apartamentos!
Seika: Sí... y, um...@tú también estabas en su equipo, ¿verdad...?
Kako: Sí. Es un placer volver a verlas.
Nanaka: ¿Puedo tener un breve momento de@su tiempo?
Mito: ¿Así que estás aquí para buscar a un enemigo@que detectaste con tu habilidad?
Nanaka: Sí, precisamente.
Seika: Pero sentiste al mismo enemigo...@¿en dos lugares a la vez?
Seika: Eso... no es posible, ¿verdad?
Kako: A mí también me costó creerlo al principio.
Seika: ¿Algo te convenció de lo contrario?
Kako: Sí.
Leila: Nosotras no podemos dividirnos en dos@precisamente...
Nanaka: Pero seguro que todas estamos familiarizadas@con algo que puede crear copias de sí mismo.
Leila: ¿Qué es?
Mito: ¡Oh, ya sé...!
Seika: ¡Dinos, Mito!
Mito: Una Bruja, ¿verdad?
Nanaka: ¡Correcto! Muy bien hecho.
Nanaka: Mi habilidad me indicó que había dos Brujas@con las mismas características.
Nanaka: No estoy del todo segura de lo que eso@significa...
Nanaka: Pero puedo decir una cosa con certeza.
Seika: Si está reaccionando a una Bruja que@representa una amenaza para nosotras...
Leila: ¿Entonces necesitamos derrotar a ambas?
Nanaka: Así es.
Nanaka: "Estos son los dos lugares donde sentí a la Bruja".
Nanaka: "La primera fue aquí,@en la Mansión de los Espejos".
Nanaka: "La segunda fue en la llamada@Ciudad de los Estafadores en Daito".
Nanaka: "Por desgracia, no estoy familiarizada@con la geografía de Daito.  Pero estoy segura de que ustedes tres@están familiarizadas con ese lugar, ¿no?".
Nanaka: "Así que me gustaría pasarles la antorcha.@Por favor, déjennos los Espejos Infinitos a nosotras,@y hagan lo que puedan para investigar la@Ciudad de los Estafadores..."
Seika: *Jadeo, Jadeo*...
Leila: Esta es... *Jadeo*, la dirección opuesta...@¿¡pero estás segura...!?
Mito: *Jadeo*... fuu. ¡Sí...!
Mito: Parece que la Coordinadora estaba buscando@algo en Daito...
Mito: ¡Así que puede que haya algo en su tienda!
Mito: ¡Coordinadoraaaa!
.....
Seika: No está aquí.
Leila: Sí... @está muy tranquilo.
Seika: ... No hay indicios de que haya habido@una pelea.
Leila: Todo en la tienda está donde debe estar.@[chara:301300:lipSynch_0][wait:1.0][chara:301300:cheek_0][chara:301300:face_mtn_ex_020.exp.json:motion_200][chara:301300:lipSynch_1]... Voy a echar un vistazo en la parte de atrás.
Seika: Entendido.
Mito: Aunque no resguardó muy bien el lugar.
Seika: O mejor dicho... es como si se hubiera@ido un día y no hubiera vuelto.
Seika: Por eso dejó todo detrás...
Mito: Como una película de terror.
Seika: ¡No digas eso, Mito...!
Leila: ¡UWAAAAH!
Seika: [chara:301400:effect_emotion_surprise_0][se:7226_shock]¡KYAH!
Leila: ¡Oigan, vengan a ver esto!
Seika: Cielos, no me asustes así...
Leila: L-Lo siento.@¡Pero lo entenderás en un segundo!
Seika: Estas son...
Seika: Estas cosas esparcidas por todas partes. ¿Son...@esas monedas que encuentras en los Espejos?
Mito: No era eso...
Seika: ¿Eh? Definitivamente son Monedas Espejo.
Mito: ¡No, esos no! ¡La Coordinadora!
Mito: Esto es solo una teoría, pero no creo que@"simplemente se haya ido" un día.
Mito: Ella salió después de hacer un montón de@preparativos aquí, pero no pudo volver.
Leila: ¿Significa que estas monedas son importantes@para lo que sea que hayan sido?
Mito: Sí, probablemente.
Leila: Entonces deberíamos llevárnoslas.
Leila: ¡Llenemos una bolsa con tantas como podamos@llevar, para cuando la encontremos!
Mito: ¡Sí! ¡Y si se enoja con nosotras,@entonces solo lidiaremos con eso~!
Seika: ¡Muy bien, ya casi llegamos a@la Ciudad de los Estafadores...!
Leila: ¡Esperen!
Mito: ¿¡Y ahora qué!? ¡Tenemos que darnos prisa!
Leila: No, quiero decir...@¿no sienten, como, alguna magia extraña?
Seika: .....
Mito: .....[wait:1.0][chara:301500:cheek_0][chara:301500:face_mtn_ex_030.exp.json:motion_300][chara:301500:lipSynch_1]@Sí, eso creo.
Leila: Creo que proviene de nosotras tres.
Seika: ... Deben ser las monedas.
