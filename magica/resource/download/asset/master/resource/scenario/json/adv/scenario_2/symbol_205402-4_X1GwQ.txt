Rena: [chara:100901:effect_emotion_joy_0][se:7222_happy]¡Woo! ¡Eso estuvo delicioso!
Kyoko: Bueno, supongo que...
Kyoko: (¡¿...?! ¡Mi Soul Gem está reaccionando!)
Kyoko: (Parece que está bastante cerca...)
Kyoko: Ah, supongo que me iré ahora.
Rena: ...
Kaede: ...
Kyoko: ¿Uh? ¿Que pasa?
Kaede: ¡¿Huh?!
Rena: Oh... A-ahh... Um...
Rena: ¡C-cierto! [chara:100901:lipSynch_0][wait:0.5][chara:100901:cheek_0][chara:100901:face_mtn_ex_020.exp.json][chara:100901:lipSynch_1]Nosotras también tenemos@un lugar al que ir...
Kaede: ¡S-sí! deberíamos ir...
Kyoko: (¿Huh? ¿Por qué de repente tienen prisa?)
Rena: ¡Oye, oye! ¡Ese no es el camino a la estación!
Kyoko: ¿Huh? ¿Y qué?
Rena: P-pero, ¿no acabas de decir que@estabas buscando a Iroha?
Rena: Deberías buscar fuera de Kamihama...
Kyoko: ¡Eso no fue lo que me dijiste antes!
Kyoko: ¿No dijiste que debía esperarla por aquí?
Kyoko: Eso fue lo que dijiste, ¿no?
Rena: ¡Ngh!
Kyoko: Solo doy un paseo, procesando lo que comí,@y descansando de mi trabajo de detective.
Kaede: ¡O-oh! ¡Un p-paseo! En ese caso...
Kaede: Hay un lugar muy bonito para pasear por allí...
Rena: ¡S-sí! ¡Sí, lo hay! ¡Allí! ¡En la dirección opuesta!
Kyoko: ¿Qué está pasando aquí...?
Kyoko: (La reacción es cada vez más fuerte.@Está muy cerca... justo delante.)
Rena: ¡Nnngh, esto es tan frustrante!
Rena: ¡¿Por qué nos sigues?!
Kyoko: ¿Huh?
Kyoko: No sé de dónde sacaste esa idea. Casualmente@estamos caminando en la misma dirección.
Rena: ¡Urghhh... maldición!@¡Pero ya nos estamos acercando!
Kaede: ¡Rena!
Kyoko: ...
Kyoko: Ohh, ahora lo entiendo. ¿En serio?@¿Así que de eso se trataba?
Kaede: ¿Huh?
Kyoko: No se preocupen.@No es que vaya a tratar de detenerlas...
Kyoko: No planeo tomarlo todo para mí.
Rena: ¿Qué? De qué estás hablando...
Kyoko: Decidamos quién se lleva la mejor parte...
Kyoko: Con una prueba de fuerza.
Kaede: ¡¡¡...!!!
Kyoko: Permítanme participar también.
Rena: ¿Huh? ¡¿Eres una Chica Mágica?!
