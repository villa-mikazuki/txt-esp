Mifuyu: Tsukuyo, Tsukasa... ¿Están listas?
Tsukasa: S-seguro no podemos usar nuestras flautas, ¿verdad?
Tsukuyo: ¡Probablemente no!
Tsukuyo: Intentemos imaginar el sonido expandiéndose@a través del vasto espacio...
Tsukasa: Nunca he hecho esto, así que no se@cómo hacerlo.
Mifuyu: ¡Vamos, ustedes dos! ¡Hagamos esto!
Tsukuyo & Tsukasa: ¡S-sí!
