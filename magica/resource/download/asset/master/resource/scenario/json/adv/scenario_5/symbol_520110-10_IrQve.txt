Takina: ... Kyubey.
Kyubey: Hola, Takina.
Kyubey: Parece que Chisato está en condición crítica.
Kyubey: Pero si lo deseas, puedes salvarla.
Takina: Conviértete en una Chica Mágica a cambio@de un deseo...
Takina: Y asumir el destino de convertirme en una Bruja@algún día, ¿correcto?
Kyubey: ...
Takina: Está bien... Haré un contrato.@Puedo salvar a Chisato, ¿verdad?
Kyubey: No te preocupes.@Tu deseo se hará realidad.
Takina: Entonces... por favor salva a Chisato.
Kyubey: "Ahora...@Abre tu mente y acéptalo.@Este es tu destino."
Takina: ... Entonces soy una Chica Mágica.
Takina: ¡Ah, Chisato...!
Chisato: Mm... *Zzz...* *Zzz...*
Takina: ... Me alegro mucho de que estés bien.
Voz de ???: ... Entonces hiciste un contrato...
Takina: [chara:406200:effect_emotion_surprise_0][se:7226_shock]*Jadeo*
Takina: ¡¿Quién está ahí?!
Sana: ... Oh, cierto...@Me puedes ver ahora...
Sana: ...Um, siento no haber podido ayudarte...
Sana: Ah, erm... me llamo Sana Futaba...@Vivo en la Villa Mikazuki...
Sana: Um... estaba tomando la retaguardia como@contingencia... En caso de que algo saliera mal...
Yachiyo: Bueno, manos a la obra.
Iroha: Sana, ¿te puedo pedir un pequeño favor?
Sana: Ah, claro... ¿qué es?
Iroha: Aunque no creo que esto sea un [textBlue:Rumor] ya que@está fuera de la ciudad, si realmente lo es...
Iroha: Impactaría a todos a su alcance.@Podríamos quedar atrapadas.
Iroha: Así que quiero que te quedes en la retaguardia,@a una buena distancia.
Sana: Quieres decir...@En caso de que algo salga mal, ¿cierto?
Iroha: Sí, ¿puedes hacer eso por nosotras?
Sana: ... Sí, lo haré.@Y borraré mi presencia por si acaso.
Sana: De hecho... he estado aquí un tiempo,@pero soy invisible...  Bueno, mejor dicho...
Sana: Solo las Chicas Mágicas pueden verme...
Sana: Erm... Si piensan en mi como un hada...@o un yokai... ¿Puede que tenga sentido...?
Felicia: ...Bueno, ah...ella es un hada, ¡algo así!@¿o un yokai? Misma cosa.
Takina: Ah... En ese entonces...@En realidad eras una Chica Mágica, ¿no?
Takina: En ese caso, me gustaría pedirte un favor.
Sana: ¿Ehh...?
