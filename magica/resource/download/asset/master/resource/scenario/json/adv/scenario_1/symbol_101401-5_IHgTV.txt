Iroha: (Es raro ver lugares que den@agua gratis...)
Bienvenida, jovencita.
Esta es el Agua de las Maravillas. Le garantizo que@saciará su sed y energizará su cuerpo cansado.
¿Quieres probar un poco?
Iroha: Sí, por favor.
???: ¡Oye, viejo! ¡Yo también quiero! Es gratis, ¿verdad?
Sí, por supuesto. Una para ti también.
???: ¿Puedo tomar un poco también?
???: Estoy bastante cansada por dar tantas vueltas.
¡Ja, ja! Vaya, vaya, ¡Tengo un montón de@clientas lindas de repente!
Un momento, por favor.
Iroha: (Hay muchos otros clientes.)
Iroha: (¿Este lugar es famoso por aquí?)
Aquí tiene, señorita.
Iroha: Gracias.
*Glup* *Glup*
Iroha: ... ¡¿?!
¡¡¡Aaah!!!
Iroha: (¡Esta agua está muuuy rica!)
???: ¡Oye, viejo! ¡¿Esto en serio es solo agua?!@¡Casi sabe a jugo!
???: ¡Viejo! ¡Dame otro vaso!
???: ¡Es fácil de beber! ¡Quiero más!
Me alegro de que te haya gustado tanto.@Sin embargo, me temo que solo puedes tomar@un vaso por día.
Después de todo, hay otros que también@quieren tomar.
???: ¡¿Eh?! ¿Pero qué...? ¡Tacaño!
Oh, no digas eso...
???: Tú también querías más, ¿verdad?
Iroha: ¿Eh? ¿Yo? U-um, sí, quería más.
???: Bueno, si es así, vamos a tener@que dejarlo ir.
Iroha: Sí, estoy de acuerdo.
Iroha: No queremos darle problemas al buen hombre.
???: Meh.
