Madoka: ¡Lo hicimos, lo logramos!
Iroha: ¡Seguro que lo hicimos!
Madoka: ¡Sí, Iroha!
Yachiyo: ¡Estaban en perfecta sintonía!
Yachiyo: Solo era una idea... Nunca esperé@que lo hicieras tan bien.
Yachiyo: Tal vez sus magias sea similares de@cierta forma.
Iroha: *Risita*
...
Yachiyo: Bueno, el Beso de la Bruja ha desaparecido.@Vamos a llevarla abajo, con calma.
Iroha: Gracias, Madoka.
Iroha: No solo ayudaste a esta chica,@también nos ayudaste a nosotrss.
Madoka: ¿Qué quieres decir?
Iroha: Bueno, queríamos preguntarle a esta chica@sobre un rumor que se está propagando por aquí.
Madoka: ¿Quieres decir... un rumor de esta ciudad?
Iroha: [chara:100101:effect_emotion_surprise_0][se:7226_shock]¿Sabes algo?
Madoka: No, la verdad es que no.
Madoka: Es solo que mi senpai vino aquí a investigar@algunos rumores, y todavía no ha vuelto...
Iroha: Entonces, ¿es a ella a quien buscas?
Madoka: Sí...
Madoka: En realidad... ella nos dijo que no viniéramos@aquí solas.
Madoka: Pero no podía aguantar la espera.@Así que mi amiga y yo vinimos juntas.
Madoka: [chara:200101:effect_emotion_sad_0][se:7224_fall_into]Pero entonces nos separamos...
Yachiyo: Así que eso es la situación.
Yachiyo: ¿Quieres venir con nosotras, entonces?
Madoka: ¿S-se... puede?
Yachiyo: Sí, por supuesto.
Madoka: ¿De verdad? Gracias.
Madoka: La verdad es que estaba bastante nerviosa@por estar sola...
Yachiyo: También existe la posibilidad de que nos@encontremos con tu amiga por el camino.
Madoka: ¡Sí!
Ugh...
Iroha: ¡Ah! ¿Estás bien?
¿Eh? ¿Qué estoy haciendo aquí?
¿La voz de [textRed:La Chica de las Ondas deRadio]?
Yachiyo: Sí. ¿Tienes idea de quién puede ser?
...Sí.
Hay un rumor que circula últimamente, de que es@una estudiante de nuestra escuela.
Esta chica que desapareció...
Yachiyo: ¿Una estudiante de tu escuela desapareció?
Sí, pero... no es nadie que yo conozca.@Al menos no creo que lo sea...
Escuché que era una especie de solitaria...
Que realmente no encajaba...@Nadie se fijaba en ella.
Así que pensé, ¿hice algo para lastimarla sin@siquiera saberlo?
La posibilidad de que le ocurriera algo terrible...@Me hizo sentir muy mal.
Por eso vine aquí ayer... Para intentar@y escuchar su voz por una vez...
Iroha: (Una solitaria... Alguien que no encaja...)
Genial, tenemos el premio de la feria de la escuela!@¡Vamos, qué todo el mundo venga para una foto!
Iroha: Um, donde debo ponerme...
¿En qué grupo estás para el viaje escolar?
Iroha: Espera, ¿ya lo han decidido todos?
Así que... Todos pensamos que Iroha sería una buena@presidenta de clase, ¿verdad?
Iroha: ¿Eh? ¿Yo? Lo haré, pero... ¿están seguros?
Iroha: ¿Por qué siento que siempre estoy a la deriva@en la clase?
Iroha: ¿Es porque me gustan otras cosas?
Iroha: ¿O es por mi personalidad?
Iroha: ¿Por qué siempre tengo la sensación de que@la gente me está haciendo a un lado?
Madoka: Iroha, ¿estás bien?
Iroha: ¿Eh?
Madoka: Estás muy pálida de repente...
Iroha: No es nada. No te preocupes por ello.
Iroha: Um...
Sí...
Iroha: Tu uniforme... Es de la Academia de Chicas@de Mizuna, ¿verdad?
Iroha: Um... Eso significa que la chica desaparecida@también va allí, ¿verdad?
Eso es lo que he oído, al menos...
*Buzz* *Buzz*...
"Por favor, responda."
"Solo una Chica Mágica puede ayudarme".