Yachiyo: ¿Reemplazar a Kyubey?
Yachiyo: Dices que intentan robarle sus deberes...
Yachiyo: ¿Y crear un mundo donde no ocurra@la transformación en Brujas?
Yachiyo: Transformarse en Doppels crearía energía@para mantener el universo.
Yachiyo: Asi no nos transformaríamos en Brujas.
Nemu: Y cuando todas las Brujas desaparezcan@eventualmente, podremos vivir en paz.
Nemu: Para ese propósito,@necesitamos transformar a Eve en una Bruja.
Nemu: Usaremos esa energía de transición para@expandir el campo de fuerza de Kamihama, asegurar@la Bruja Eve al nuevo campo, y formar una puerta@para regresar energía al universo.
Nemu: Y todo lo que faltaría es tener a Eve,@que tiene el poder de recolectar energía@y regresarla al universo,@haciendo eso automáticamente por nosotros.
Nemu: Kyubey no tendrá razones para interferir@si el universo continúa existiendo. Incluso@si quisiera, tendriamos nuestro campo de fuerza.
Nemu: Tendremos nuestro paraíso propio, donde@las Chicas Mágicas y las Brujas no sean creadas.
Kanagi: Hm... Kyubey desapareció de esta ciudad.
Kanagi: Supongo que eso significa que estuvieron@detrás de eso también.
Nemu: Nuestro campo de fuerza que mantiene a Kyubey@fuera es nuestra garantía.
Nemu: Touka se simplificó cuando dijo que Kyubey@apareció un día.
Nemu: Intervino en la red de su raza.
Nemu: Intentó robarles tanta información como pudo,@así que ellos desconfían de ella.
Nemu: Pero, el mismo Kyubey nos convirtió en@Chicas Mágicas.
Nemu: Pensamos que incluso si el supiera la@mitad de nuestro plan, si cada unidad es prácticamente lo mismo...
Nemu: Podrían estar preocupados sobre lo que pasa@en Kamihama, pero no interferirían.
Nemu: Dicho eso, energía emocional negativa ocurre@cuando las Doppels son creadas.
Nemu: Y muchas personas también fueron atrapadas@por un Uwasa, creando más energía emocional.
Nemu: No queríamos que un Kyubey recolectara esa@energía para sí mismos.
Nemu: De todas formas, su trabajo casi termina.
Nemu: Proto estará aquí Walpurgisnacht.
Yachiyo: Están desestimando a Walpurgisnacht.
Yachiyo: Pueden haber triunfado en sus planes@hasta ahora.
Yachiyo: Pero solo esperen hasta que vean esa@Bruja.
Yachiyo: Al final, sus planes podrían ser completamente@arruinados.
Alina: Si eso pasa, solo necesitamos liberar a Eve.
Alina: Nunca dijimos que eliminaríamos a@Walpurgisnacht.
Alina: Eso nunca fue parte del plan.
Yachiyo: ¿De qué estás hablando?
Touka: Es más, estamos recibiendo a Walpurgisnacht@con los brazos abiertos.
Nemu: Entre más se destruya la ciudad, más energía@emocional fluirá de todo esto.
Touka: ¡Eve absorberá esa energía y se hará más y más@fuerte!
Alina: Y cuando Mifuyu elimine ese [textBlue:Rumor] y libere@a Eve...
Alina: Eve devorará a Walpurgisnacht@y será destruida.
