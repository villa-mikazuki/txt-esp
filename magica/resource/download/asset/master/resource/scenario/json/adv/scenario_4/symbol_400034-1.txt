Meiyui: ¿¡Q-Quién está ahí!?
Nanaka: ¿¡Más Familiares!?@[chara:300500:lipSynch_0][wait:0.8][chara:300500:cheek_-1][chara:300500:face_mtn_ex_050.exp.json:motion_0][chara:300500:lipSynch_1]... No, no lo parecen.
Yachiyo: ¡Tokiwa...!@¿Cómo se metieron ustedes en este Laberinto?
Yachiyo: ¡E-Esperen! 
Mito: ¡Esto es extraño!
Mito: Verán, nosotras tres...
Nanaka: "Estos son los dos lugares donde sentí a la Bruja".
Nanaka: "La primera fue aquí,@en la Mansión de los Espejos".
Nanaka: "La segunda fue en la llamada@Ciudad de los Estafadores en Daito".
Nanaka: "Por desgracia, no estoy familiarizada@con la geografía de Daito. Pero estoy segura de que ustedes tres@están familiarizadas con ese lugar, ¿no?".
Nanaka: "Así que me gustaría pasarles la antorcha.@Por favor, déjennos los Espejos Infinitos a nosotras,@y hagan lo que puedan para investigar la@Ciudad de los Estafadores..."
Yachiyo: ¿Así que todas ustedes vinieron a este@Laberinto desde la Mansión de los Espejos?
Ayame: ¡Así es!
Ayame: Algo nos estaba mostrando estos@horribles sueños...
Ayame: ¡Fue de lo PEOR!
Konoha: Hazuki y yo también estuvimos involucradas.
Konoha: Si no hubiéramos oído la voz de Nanaka,@habríamos estado en serios problemas...
Seika: ¿Su voz...?
Nanaka: Sí. A nosotras dos también nos mostraron@una ilusión similar.
Meiyui: Pero cuando alcanzamos la voz que oíamos,@se desvaneció.
Meiyui: Después de eso,@nada ha tenido mucho sentido.
Meiyui: La puerta que encontramos dentro del@Laberinto resultó ser un callejón sin salida...
Meiyui: Y mientras todas buscábamos otra ruta...
Meiyui: El camino se abrió de repente y@nos encontramos con todas ustedes.
Mitama: Justo cuando mi barricada de Monedas Espejo@se rompió...
Mitama: Lo más probable es que la Bruja original se@diera cuenta y empezara a moverse de nuevo.
Himika: Así que los Laberintos se conectaron de@repente, y ustedes aparecieron aquí en Daito...
Himika: ¿¡Eso significa que básicamente se@teletransportaron!?
Seika: ¿E-Es siquiera posible algo así...?
Leila: Bueno, llegaron aquí desde@los Espejos Infinitos.
Hazuki: Quizá fuera solo una ilusión, pero por primera@vez vimos el exterior dentro de los Espejos...
Hazuki: Si los Familiares Espejo no están copiando@solo nuestras apariencias y habilidades...
Hazuki: sino también nuestras memorias...
Hazuki: Entonces podrían obtener casi cualquier@información que exista.
Nanaka: Es una posibilidad muy probable.
Hazuki: Esto es solo una teoría.
Hazuki: Y si unieron dos zonas del exterior, usando ese@poder que muestra mentiras como ilusiones...
Hazuki: ¿Y eso se vio reflejado en la realidad...?
Nanaka: ¿¡Quieres decir que las dos regiones estarían@REALMENTE conectadas!?
Yachiyo: ¡Eso es ridículo...!
Meiyui: Di una mentira las suficientes veces y@puede convertirse en verdad.
Ayame: ¿Eh...?
Ayame: ¿Quieres decir que incluso las mentiras@pueden hacerse realidad...?
Yachiyo: Es un salto demasiado grande. No hay evidencia@que pueda respaldar esa teoría.
Yachiyo: Lo único seguro es que los Espejos pueden@mostrarnos ilusiones ahora...
Yachiyo: ... Y la posibilidad de que nuestras memorias@estén siendo copiadas también.
Yachiyo: Y de cómo ahora, en la realidad, los Laberintos@están conectados a pesar de la distancia física.
Yachiyo: Los Laberintos ya son espacios separados que@existen paralelamente a nuestro mundo.
Yachiyo: No es de extrañar que dos de ellos se@conectaran entre sí.
Yachiyo: En serio, lo que da miedo ahora mismo es su@"capacidad para interferir en otros espacios".
Yachiyo: La Bruja de los Espejos podría seguir huyendo@y huyendo, sin que la atrapáramos nunca...
Nanaka: Aunque no ataque a la gente, ciertamente@no podemos dejar las cosas en este estado.
Seika: Cierto...
Seika: ¡Si sigue madurando, eventualmente será@demasiado tarde...!
Mito: ¡Tenemos que derribarla juntas!
Leila: Sí, al menos esta ex-Familiar Bruja de@los Espejos, no hay de otra.
Mitama: .......
Yachiyo: ¿Mitama?
Mitama: Si la Bruja de los Espejos se dirige a@la Ciudad de los Estafadores...
Mitama: Entonces creo que habrá al menos un civil@que morirá como resultado.
