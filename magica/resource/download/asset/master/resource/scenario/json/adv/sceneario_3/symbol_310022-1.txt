Unos días después de la reunión...
La chica que había causado alboroto@en Kamihama apareció ante Yachiyo.
Yachiyo: Gracias por llamarme.
Momoko: No hay problema.@Rena se quejó un montón.
Rena: ¡Solo dije que podía manejarla!
Kaede: ¡Vamos, Rena, de verdad teníamos que@pedir ayuda a Yachiyo!
Kaede: Quiero decir, mira a esta chica...@Se ve fuerte.
???: ¡Basta de cháchara!@[chara:100300:lipSynch_0][freeze:0.5][chara:100300:effect_emotion_angry_1][se:7218_anger][se:7218_anger][chara:100300:motion_200][chara:100300:mouthOpen_1][chara:100300:lipSynch_1]¿Cuánto más tiempo van a estar paradas?
Kaede: ¡Eep!
Yachiyo: ... ¿Cómo te llamas?
???: ¡Ugh, por fin alguien preguntó!@¡TE TOMÓ MUCHO TIEMPO!
???: Ejem...@Permítanme presentarme...
Tsuruno: ¡Me llamo Tsuruno Yui! ¡Estoy aquí para@restaurar el nombre de mi familia!
Tsuruno: Nadie se interpondrá en mi camino...@¡Te derrotaré aquí y AHORA!
Momoko: ¿El nombre de tu familia?
Rena: ¿Qué demonios?@En serio, explícate.
Tsuruno: ¡No quiero!
Rena: ¿¡Qué clase de respuesta es esa!?
Tsuruno: ¡No estoy aquí para hablar!@¡Estoy aquí para LUCHAR!
Yachiyo: ... ¡No quiero!
Tsuruno: ¡O-Oye! @[chara:100300:effect_emotion_surprise_0][se:7226_shock]¡No me copies!
Yachiyo: No estoy interesada en una pelea sin sentido.
Tsuruno: ¡Bueno, MALA SUERTE!@¡Ya te dije que no estoy aquí para hablar!
Yachiyo: Entonces, resúmelo para nosotras.
Tsuruno: Er, Yo... ¡Argh! [chara:100300:lipSynch_0][freeze:1.0][chara:100300:motion_101][chara:100300:cheek_0][chara:100300:face_mtn_ex_020.exp.json][chara:100300:lipSynch_1]¡Bueno...!@¡Quiero ser la más poderosa!
Kaede: ¿La más poderosa?
Tsuruno: La Chica Mágica más Poderosa...@¡No hay mayor honor!
Tsuruno: Eso es lo que yo creo,@¡y voy a hacerlo realidad!
Rena: ... ¡Aún no tengo idea de lo que está hablando!
Yachiyo: (Tal vez no sea tan peligrosa@como pensaba...)
Yachiyo: ... De acuerdo entonces.@Yo seré tu rival hoy.
Tsuruno: ¡De eso estoy hablando!@¡En serio, NO MÁS CHARLA!
