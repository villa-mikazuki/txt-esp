Iroha: Últimamente he estado teniendo la misma@serie de sueños, una y otra vez.
Iroha: Sueños sobre una chica desconocida en un hospital.@Sueños en los que todo lo que hago es@verla leer un libro o comer en cama.
Iroha: A veces ella me@sonríe y dice algo.@Pero nunca puedo oír su voz.
Iroha: Debe ser porque no estoy @en el cuarto de hospital con ella. 
Iroha: Estoy afuera, en otro@mundo observando el de ella.
Iroha: Me pregunto por qué...@Es el único sueño que tengo.
—Ciudad de Kamihama—
—Distrito de Shinsei—
Iroha: ...
Iroha: (Desde que llegué a esta ciudad el otro día...)
Iroha: (He estado teniendo ese sueño una y otra vez.)
Iroha: (Cada vez que lo tengo,@siento mariposas...)
Iroha: (Como si hubiera algo muy@dentro de mí que sigue vivo...)
Iroha: ... Quiero llegar al fondo de esto.
Iroha: Llegar al fondo de estos sueños@y por qué me hacen sentir así.
Iroha: Por eso estoy aquí.
Iroha: Tengo que descubrirlo.
Iroha: Tengo que descubrir quién es esa chica.
Iroha: ...
Iroha: (Pensé que lo descubriría@si rastreaba una Bruja, pero...)
Iroha: (Hay demasiadas Brujas y Familiares como para@rastrear una señal de magia específica...)
Iroha: De cualquier modo...
Iroha: (Probablemente haya un Bruja por aquí…)
Iroha: Debería intentar buscar de nuevo...
Iroha: ...
Iroha: Como lo pensé, está cerca...
Iroha: ... ¡¿?!@¡No, está aquí!
Iroha: (Si llego tarde a casa,@Mamá se enojará...)
Iroha: (¡Me apresuraré!)
Iroha: ¡Aquí está el laberinto de la Bruja!
Iroha: ... ¿Eh?@¿Ya hay alguien aquí...?
Iroha: (¡¿Tiene problemas...?!)
Iroha: ¡Ay no, debo ayudarla!
