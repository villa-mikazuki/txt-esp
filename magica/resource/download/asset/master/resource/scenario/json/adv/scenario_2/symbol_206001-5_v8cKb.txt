Mami: Aquí, quiero que vean esto.
Homura: Esto es... ¿un mapa de Mitakihara y sus@alrededores?
Madoka: Hay algunos lugares marcados con un círculo.
Sayaka: Oh, sí. Incluso están divididos por colores...
Kyoko: Huh... ¿Así que esto es lo que has investigado?
Mami: Sí...
Mami: He marcado las desapariciones y los lugares@donde la gente se perdió cuando no debía.
Sayaka: Ohh, ¡eso es lo que hiciste en internet!
Mami: Exacto. A través de páginas de noticias y@redes sociales.
Mami: Las marcas están donde hubo un incidente.
Mami: Y utilicé un color diferente para cada día que@ocurría algo.
Mami: ¿Ven algún tipo de patrón?
Madoka: Hmm... Umm...
Madoka: Parece que la distancia que recorre la Bruja@en un día no es muy grande.
Homura: Y los movimientos son irregulares.
Homura: Tampoco parece que se dirija a zonas muy@concurridas.
Sayaka: ¿Qué está tramando? Incluso está volviendo a@lugares donde estuvo antes.
Madoka: ¿Creen que está buscando algo?
Sayaka: Eso... ¿o no sabe a dónde está yendo?
Sayaka: ¡Oh! ¡Está perdida!
Homura: (Perdida...)
Madoka: ¡Puede que tengas razón!
Madoka: ¿O tal vez ella no sabe a dónde quiere ir...@o algo así?
Homura: ...
Homura: ("Ella no sabe a dónde quiere ir.")
Homura: (... Como yo.)
Kyoko: Así que querías averiguar a dónde irá la Bruja@después, ¿verdad?
Kyoko: ¿Por qué no buscar su magia?@Apuesto a que eso sería más rápido.
Mami: ... Pero eso no ha funcionado bien para ti,@¿correcto?
Kyoko: ...
Kyoko: Bueno... sí.
Mami: Creo que tienen razón. Debe estar perdida.
Mami: Ni siquiera sabe a dónde quiere ir.
Kyoko: Así que dices que no hay manera de saber@a dónde irá la bruja después.
Mami: Pues sí, pero...
Kyoko: Si esa es toda la información que tienes,@me largo de aquí.
Mami: ¡Oye, espera!
Sayaka: ¿Por qué te enojas? Déjala terminar.
Kyoko: ¿Eh? ¡No me estoy enojando!
Kyoko: ...
Kyoko: Perdón. No debería haber interrumpido.
Kyoko: ¿Qué más?
Mami: Oh... Um...
Mami: No podemos predecir a dónde irá.
Mami: Pero creo que podemos limitar el área@general en la que podría moverse.
Madoka: ¿Así que como un área en la que podría estar?
Mami: Sí, miren.
Mami: El primer incidente reportado fue aquí.@Ese día, las víctimas aparecieron en esta zona.
Mami: Las víctimas del día siguiente estaban aquí.@El área es más o menos así de grande.
Sayaka: Entonces, al día siguiente...@Ah, ¿estaba en esta área?
Mami: Y ayer estuvo aquí.
Kyoko: Bien, bien. Así que estás reduciendo la@distancia a la que se puede mover.
Mami: Correcto, como mucho el área en la que se@mueve en un día es de este tamaño.
Kyoko: Hoy sentí su magia alrededor de este lugar.
Mami: ...
Kyoko: ... ¿Qué?
Mami: Er, nada. Gracias.
Kyoko: ...
Sayaka: Eso es... ¿más o menos en la frontera entre@Mitakihara y Kazamino?
Mami: Sí. Y considerando su rango de movimiento...
Mami: Homura, ¿podrías pasarme ese compás de ahí?
Homura: Oh, claro.
Mami: Gracias.
*Rayar* *Rayar*
Mami: Debería estar dentro de esta circunferencia.
Madoka y Sayaka: ¡Ohhh!
Mami: Ahora solo hará falta un poco de@trabajo de campo.
Madoka: ¡Deberíamos ser capaces de cubrir un área de@este tamaño!
Sayaka: ¡Muy bien!@¡Es hora de encontrar a nuestra Bruja perdida!
Madoka: ¡Sí! ¡Hagámoslo!
Homura: ...
Madoka: ¿Homura? ¿Estás bien?
Homura: ... Sí.
Homura: (¿Qué... qué es lo que quiero hacer?)
