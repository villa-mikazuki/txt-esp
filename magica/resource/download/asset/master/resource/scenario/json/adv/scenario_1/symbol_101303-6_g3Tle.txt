Yachiyo: Haa...@Eso fue agotador.
Yachiyo: Tamaki-san, deberías apresurarte a volver a casa.
Iroha: U-umm, ¡espere un segundo!
Yachiyo: Por cierto, ¿qué has estado escribiendo?
Yachiyo: ¿No es tu tarea?
Iroha: ¡[chara:100102:effect_emotion_surprise_0][se:7226_shock]Ah!@¡N-no, por favor, no mire!
Yachiyo: ¿Qué haces tapándote la cara?@Puedo ver todo lo que has escrito.
Iroha: [chara:100102:effect_emotion_surprise_0][se:7226_shock]¡¿Eh?!
Yachiyo: Kamihama... ¿Libro de Misterios?@Espera, estás...
Iroha: Umm...
Iroha: Sí...@Le he copiado un poco.
Yachiyo: Así que no fue tu tarea lo que se te cayó antes....
Iroha: Lo siento...
Yachiyo: Je Je... Está bien.@No me importa que me copies.
Yachiyo: Solo no te sobreesfuerces, ¿de acuerdo?
Iroha: Gr-gracias.
Yachiyo: Bueno, entonces, si me disculpas, debería irme.
Iroha: Oh, sí, por supuesto.@¡Gracias por lo de hoy!
Yachiyo: Gracias, también.
Yachiyo: Gracias a ti conseguí este@amuleto de emparejamiento...
Yachiyo: Espero que la encuentres...@a tu hermana quiero decir.
Iroha: [chara:100102:effect_emotion_surprise_0][se:7226_shock]¿Eh?
Yachiyo: No, no es nada.
Iroha: ...
Iroha: (Realmente no entiendo a la Yachiyo-san...)
Iroha: (Tratando de ahuyentarme...@De ir tras userName...)
Iroha: (Luego ayudándonos cuando estábamos lidiando@con La Regla de la Ruptura de Amistades...)
Yachiyo: Solo no quiero que más gente muera en vano.@Eso es todo...
Yachiyo: Ten cuidado con los [textBlue:Rumores].@Estás advertida.
Yachiyo: No metas las narices en cosas@que no te tomarás en serio.
Iroha: (Puede ser fría, y a veces da la sensación@de que intenta meterse en medio, pero...)
Iroha: (Creo que puede ser su forma de ser amable...)
Iroha: (Como cuando derrotamos a esa Bruja antes...)
Iroha: (Solo desearía que ella me apoyara más@en la búsqueda de Rumores...)
