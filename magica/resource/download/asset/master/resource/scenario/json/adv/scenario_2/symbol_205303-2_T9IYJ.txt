Mami: Ante mis ojos, una bruja@se transformó en una@perfecta forma humana.
Mami: La persona a la que intentaba salvar@hace un momento, que percibía como una aliada,@¡¿era en realidad mi enemiga?!
Mami: ¿Cómo puedo seguir luchando@cuando ya no sé en qué creer?
Mami: Aun así...@El único camino posible es derrotar@al enemigo que tengo delante.
Yachiyo: De todas formas...@deberíamos ir a ver a la Coordinadora.
Mami: Lo lamento, pero no puedo dejarles hacer eso.
Iroha: ¡¿Eh?!
Iroha: Um... ¿Quién eres tú?
Mami: Mi nombre es [textRed:Mami Tomoe].@Soy una Chica Mágica de Mitakihara.
Mami: Me sorprende que estén tan@calmadas con una bruja justo a su lado.
Mami: Encuentro un Laberinto inusual, ¡Y qué@encuentro sino una recompensa inesperada!
Yachiyo: Eres terriblemente hostíl para alguien con@tan grande sonrisa en su rostro.
Mami: Supongo que estás en lo correcto.
Mami: Pero mi hostilidad es dirigida@solo a aquella bruja con quien estan.
Iroha: ...
Iroha: ¡¿Qu-quién... Yo?!
Mami: No eres buena mentirosa.
Mami: Nunca creí que vería@una Bruja disfrazada como humano.
Yachiyo: Cometes un error.
Yachiyo: Todas aquí somos Chicas Mágicas,@igual que tu.
Mami: (Ellas vieron lo que era, pero aún así...)
Mami: (Todavía tienen fe en ella y@la tratan como humana...)
Kaede: Y cuando Rena vino a ayudarme,@algo nos lavó el cerebro.
Mami: (Oh, por supuesto. Así que la especialidad de@esta Bruja es controlar la mente de la gente...)
Mami: No, ella no...
Mami: La ví con mis propios ojos.
Mami: Ví su verdadera forma.
Tsuruno: ¡¿Qué?! ¡¿Cómo te atreves?!
Tsuruno: ¡Iroha no ha hecho NADA malo!
Tsuruno: ¡Acabas de poner a Tsuruno Yui, la más Poderosa@y Agradable Chica Mágica, muy... MOLESTA!
Tsuruno: Ugh...@Pero usé mucha magia.
Yachiyo: Salir de la sarten y caer en el fuego...
Yachiyo: No queremos gastar más magia@en una Chica Magica como nosotros.
Iroha: Yachiyo, pelearé...
Yachiyo: No, Iroha.
Yachiyo: Ninguna persona ordinaria estaría tan calmada@enfrentando a tres Chicas Mágicas.
Iroha: Pero soy la unica a quién busca.
Iroha: De todas formas, me queda más magia@que a ustedes dos.
Mami: ¡Incluso dices cosas tan consideradas!
Mami: Te daré crédito por mantener a otros@fuera de esto.
Mami: ¡Pero es demasiado tarde para arrepentirse!
Mami: (Engañar a estas chicas solo@para que te protejan...)
Mami: (Realmente no hay límite para lo bajo@que puede caer una Bruja.)
Mami: (¡Les mostraré @[chara:200500:motion_200]lo falsa que eres en realidad!)
