Iroha: No parece que la Uwasa quiera@atacarnos...
Kyoko: No es como que lo necesite. Si no podemos@destruirla, gana por defecto.
Kyoko: ¿Y qué hacemos ahora? Estoy completamente@sin ideas...
Iroha: ¿Cómo podemos llegar hasta allá...?
(Player Choice): ¡Mokkyuu!@(¡Corre más rápido de lo que las rocas caen!)
(Player Choice): Mokyu, mokyu.@(¡Dispárale una ráfaga de flechas!)
userName: ¡Mokkyuu!
Iroha: Probablemente tropezaríamos aunque corriéramos...
userName: Mokyu, mokyu.
Iroha: Podría dispararle un montón,@pero no parece que tendrían algún efecto.
Iroha: Me gustaría que hubiera alguna forma@de que pudiera teletransportarme ahí...
Kyoko: Nunca es tan fácil...
Felicia: ¡Eso es! ¡Tengo una idea!
Iroha: ¡¿Cual es?!
Felicia: ¡Solo tenemos que llegar hasta allá antes@de que caigan las rocas!
Iroha: Bueno, sí, ¿pero cómo?
Felicia: ¡Si en algo creo, es en mi fuerza!
Felicia: ¡Puedo hacerme KABLAM desde una roca,@y entonces hacer BABOOM!
Kyoko: ¿Huh? ¿Y qué significa ESO?
Iroha: Creo que está diciendo que puede saltar desde@una de las rocas que caen, y saltar al otro lado?
Felicia: ¡Sí, eso!
Iroha: ¿Realmente puedes hacer eso...?
Kyoko: No veo por qué no. ¡Merece un intento, al menos!
Felicia: ¡Sí, claro que sí! ¡Al menos deberíamos intentarlo!
