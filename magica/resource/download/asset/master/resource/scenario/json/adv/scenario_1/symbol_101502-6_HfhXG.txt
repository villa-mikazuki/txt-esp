¡ｧ螳ｴб壹！
Iroha: [flashEffect:flashWhite2][se:4001_equip_arms_bow_vh]¡Ahora!
[flashEffect:flashRed2][bgEffect:shakeLarge][se:6001_hit_shooting_01_vh][name:sprite_0:effect_shake]¡¿жxヮ亦試?!
[flashEffect:flashWhite2]縺薙ｌ¡¡wg!!
Yachiyo: [flashEffect:flashWhite2][se:4050_landing_jump]¡Demasiado lento!
[flashEffect:flashRed2][bgEffect:shakeLarge][se:6256_hit_spike_02_v][name:sprite_0:effect_shake]［］¡¿¡¿倥Бф繧後?!?!
Madoka: Guau... Increíble...
Madoka: Mm, así que las dos son de aquí, ¿huh?
Yachiyo: Así es. ¿Pasa algo con eso?
Madoka: Es solo que las Chicas Mágicas en esta@ciudad son muy fuertes...
Madoka: Estaba con mi senpai cuando llegué,@así que me enseñó los alrededores.
Madoka: La cantidad de Brujas aquí me sigue@sorprendiendo, incluso ahora. 
Yachiyo: Así que no eres de Kamihama.
Yachiyo: ¿Ya has sido capaz de derrotar una Bruja@tú sola?
Iroha: ...¡¿?!
Iroha: ¡Yachiyo, no la pruebes a ella también!
Yachiyo: Ella vino con una guía. No me es necesario@probar su determinación.
Yachiyo: Tu caso era diferente, Iroha.
Iroha: Ugh...
Madoka: Normalmente puedo manejarme, pero aún así...@derrotar Brujas aquí es mucho más difícil.
Madoka: Son muy fuertes...
Yachiyo: Ser capaz de derrotar a una sola es increíble@comparado con lo que ALGUNAS personas pueden hacer.
Iroha: ¡Está bien, Yachiyo, ya entendimos!
Yachiyo: *Risita*
Yachiyo: Bien, ya casi estamos en el núcleo. ¡Las dos,@prepárense!
Iroha: ¡Hecho!
Madoka: ¡Vale!
