Momoko: Maldición. ¿Qué pasa?@¡Mis ataques no funcionan!
Yachiyo: Parece que les vendría bien un poco de ayuda.
Iroha: ¡Yachiyo-san!
Momoko: ¡¿Qué le hiciste a Kaede?!
Yachiyo: Hice que tomara una pequeña siesta.
Momoko: Ya veo. Gracias.
Yachiyo: ¡Vaya!@¿Quién diría que eres capaz de mostrar gratitud?
Momoko: Digo gracias a quien lo merece.@No eres la excepción a esa regla.
Momoko: Como sea, sobre la Bruja...@¿Cómo la derrotamos?
Yachiyo: No lo sé.
Momoko: ¡¿Q-qué?!
Momoko: ¿Los rumores no eran tu especialidad?
Yachiyo: El Archivo de Rumores de Kamihama@no contiene todo lo que hay que saber.
Yachiyo: Esto no sería exactamente un [textBlue:rumor]@si todo el asunto ya estuviera resuelto, ¿no crees?
Yachiyo: Eso significa que tenemos que encontrar@las respuestas por nuestra cuenta.
Momoko: Uggh... eso no sirve...
Yachiyo: No creas que puedo resolver todos tus problemas@solo porque te ayudé a darle sentido a esto.
Yachiyo: ¿Planeas aferrarte a mí como una niña@como lo hacías antes, pequeña Momoko-chan?
Momoko: ¡E-eso fue en el pasado!
｜¡¡ラ↑ン↓ラ↑ンラ／!!｜[flashEffect:flashWhite1][se:7213_witch_noise_01]
Rena: ... Hacerme... tu sirvienta...
Iroha: ¡No! ¡Tenemos que apurarnos!@O Rena-chan será...
