Sayaka: Fuimos a la biblioteca con la esperanza de@encontrar un libro sobre el arte de Alina,@y tal vez algunas pistas, pero...
Sayaka: Sin suerte...
Madoka: Sí...
Sayaka: Oh, pero siempre podemos ir a otra biblioteca.
Sayaka: O buscar en otro lado, algún lugar donde@se pueda encontrar información.
Homura: ¡Intentaré buscar lugares así!
Sayaka: La búsqueda nos llevó a bibliotecas, escuelas,@ayuntamientos, centros comunitarios...
Sayaka: Pero un nombre me llamó la atención.
Sayaka: ¿Museo de los Recuerdos de Kamihama?
Homura: Creo que es un museo donde se recopilan y@almacenan documentos... ¿tal vez?
Madoka: ¡Suena como un gran lugar!
Sayaka: ¡Sí! ¿Por qué no lo revisamos?
Madoka: Muy bien, ¡vamos al Museo de los Recuerdos!
Homura: ¡Apuesto a que hay todo tipo@de documentos allí!
Sayaka: Así que nos dirigimos al@Museo de los Recuerdos de Kamihama.
Sayaka: Ugh...
Homura: Ahora es solo un edificio abandonado...
Homura: [chara:200301:effect_emotion_sad_0][se:7224_fall_into]Ahora que lo mencionas, dice "Cerrado"@más abajo en la descripción...
Homura: ¡L-lo siento mucho!
Sayaka: ¡No, no te lamentes!@¡No tienes nada de qué disculparte!
Madoka: ¡Sí, es cierto!
Sayaka: Yo soy la que se dejó llevar por el nombre...
Sayaka: ¿Huh?
Homura: Eso se sintió como...
Madoka: ¡Sí!
Sayaka: ¡¿Mami?!
Homura: ¡¿Creo que viene de algún lugar@dentro del museo?!
Sayaka: ¡Sí, no hay duda!
Sayaka: ¡No puedo creer que este lugar haya estado@directamente conectado con ella!
Madoka: ¡Tu corazonada era correcta, Sayaka!
Sayaka: *Risita* Lo fue, ¿no?
¡¿Quién está ahí?!
Sayaka: ¡¿Huh?!
