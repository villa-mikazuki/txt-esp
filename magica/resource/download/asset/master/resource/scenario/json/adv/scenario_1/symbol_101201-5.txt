Iroha: (Todavía no he sabido nada@sobre Touka-chan y Nemu-chan...)
Iroha: (Y no estoy segura de qué pensar de esa@historia que me contó Yachiyo-san...)
Iroha: Los rumores se convierten en realidad...@userName, ¿qué opinas?
(Player Choice): ¡Mokyukyu! @(¡Podría ser cierto!)
(Player Choice): Kyuin... @(Podría ser solo una mentira...)
userName: ¡Mokyukyu!
Iroha: Sí...@No hay forma de saberlo con seguridad, ¿verdad?
userName: Kyuin...
Iroha: Sí...@No hay forma de saberlo con seguridad, ¿verdad?
¡Deberías haberlo conseguido!
Iroha: ¡¿Qué?![chara:100102:effect_emotion_surprise_0][se:7226_shock]
Iroha: Q-qué voz tan ruidosa...
Pero se agotó.@¡No hay nada que pudiera haber hecho!
Iroha: Viene de ese parque...
Iroha: (¿Una pelea...?)
Iroha: Eh...
Iroha: Podría ser... ¡¿Momoko-san?!
Iroha: Y esa otra chica es...@La chica que salvé en el Laberinto de la Bruja...
Momoko: Les digo que se calmen.@¿Por qué se pelean?
???: ¡No es asunto tuyo, Momoko!
???: ¡Sí, mantén la boca cerrada, Momoko-chan!
Momoko: Oye, oye. Soy la líder aquí.
???: [chara:100901:effect_emotion_angry_1][se:7218_anger]¡Cállate! Se acabó.@¡[textRed:Ya no somos amigas], Kaede!
???: [chara:101101:effect_emotion_angry_0][se:7219_strain]¡Ah, lo dijiste!
???: ¡Si eso es lo que quieres, entonces está bien!@¡[textRed:Se acabó], Rena-chan!
Momoko: No deberían decir cosas como esa@tan descuidadamente...
Momoko: ¿Y cuántas veces han [textRed:terminado su amistad]@ustedes dos?
???: ...
Momoko: Vamos, hablen conmigo.@¿Qué les molesta a las dos?
???: ¡¡...!![chara:100901:effect_emotion_angry_0][se:7219_strain]
???: ¡Deja de meter las narices en todo!@[chara:100901:effect_emotion_angry_1][se:7218_anger]¡Eres muy sobreprotectora y es molesto!
Momoko: ¡O-oye, Rena!@¡¡No grites y salgas corriendo!!
Momoko: ... ¡¿?![chara:101001:effect_emotion_surprise_0][se:7226_shock]
Iroha: Waa...
Iroha: (Lo dijeron... [textRed:"Ya no somos amigas"]...)
Momoko: ¡¡¡Iroha-chan, detenla!!!
Iroha: ¿Eh?[chara:100102:effect_emotion_surprise_0][se:7226_shock] ¡Ah... sí!
???: ¡¡¡Fuera de mi camino!!!
Iroha: ¡Eek!
