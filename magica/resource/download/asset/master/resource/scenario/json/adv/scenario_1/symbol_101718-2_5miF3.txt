Yachiyo: ¡Lleva esto, Felicia!
Felicia: Carne... ¡Oye, esto solo es sopa miso!
Yachiyo: Le agregué puerco porque no parabas de quejarte.
Felicia: ¡Carne en sopa miso no cuenta como CARNE!
Yachiyo: Entonces supongo que no vas a desayunar.
Felicia: ¡Nooo, espera! ¡Es carne! ¡Es carne!
Yachiyo: Bien. Ahora llévala.
Felicia: [chara:100501:effect_emotion_sad_0][se:7224_fall_into]Okey...
Sana: Iroha-san... Quiero ayudar yo también...
Iroha: Está bien, ¿podrías traer los palillos?
Sana: ¡Sí!
Sana: Además...
Iroha: ¿Hmm?
Sana: ¿Deberíamos dárselos ahora? ¿Antes de comer?
Iroha: Buena idea. Ya que por fin estamos todas juntas.
Iroha: Hagámoslo.
Sana: ¡Okey!
Iroha: Tengo los vegetales en escabeche, ¿eso es todo?
Tsuruno: ...
Iroha: [chara:100101:effect_emotion_surprise_0][se:7226_shock]¡Qué! ¡Y-Yachiyo-san!
Iroha: ¡Tsuruno-chan está haciendo esa cara de nuevo!
Yachiyo: ¡¿Todavía está bajo la influencia del@[textBlue:Rumor]?!
Felicia: Es más como que su alma dejó su cuerpo.
Sana: Creo que descubrir que perdió su récord de@asistencia perfecta fue muy duro.
Felicia: ¡Iroha, ahora es nuestra oportunidad de@apoyarla!
Iroha: ¡¿Qué?!
Iroha: Pero, exactamente que debería...
Iroha: [chara:100101:lipSynch_1][chara:100101:voiceFull_fullvoice/section_101718/vo_full_101718-2-28]Um...
Iroha: Tsuruno-chan.
Tsuruno: ...
Iroha: ¡Er, en mi mente, tienes asistencia perfecta!
Iroha: Porque yo sé que habrías ido a la escuela si@hubieras podido, um...
Iroha: ¡Calentamiento de Tsuruno-chan, número uno!
Tsuruno: ¡Sí!
Tsuruno: [chara:100301:effect_emotion_surprise_0][se:7226_shock]¡Whoa! ¡Fue como un reflejo!
Yachiyo: ¿Qué estás haciendo?
Yachiyo: Lo de la asistencia es algo con lo@que tendrá que lidiar por su cuenta.
Yachiyo: Ahora bien, vamos a comer.
Iroha: Oh, hay algo que queríamos hacer primero,@Yachiyo-san.
Iroha: ¿No es cierto, Tsuruno-chan? ¿Felicia-chan?
Iroha: ¿Cierto?
Tsuruno: ¿Oh?
Tsuruno: ¡Whoa, por fin llegó el momento!
Felicia: ¡Maldición! ¡Al fin!
Yachiyo: E-espera, ¿qué está pasando?
Yachiyo: ¿Todas las demás lo saben?
Iroha: Tráelas, Sana-chan.
Yachiyo: ¿Qu-Qué?
Yachiyo: ¿Trae qué?
Sana: Um... aquí tienes.
Yachiyo: ―Yachiyo―@¿Estos son portavasos?
Iroha: ―Iroha―@Si, fuimos de compras juntas@y los compramos para ti.
Sana: ―Sana―@En realidad queríamos dártelos@hace un tiempo...
Yachiyo: ―Yachiyo―@¿Flores de cerezo?
Felicia: ―Felicia―@¡Así es!@¡Nosotras cinco somos un equipo!
Tsuruno: ―Tsuruno―@¡Es prueba de que somos el Equipo Villa Mikazuki!
Yachiyo: No puedo creer que fueron a comprar estas...@Muchísimas gracias.
Yachiyo: Vamos a empezar a usarlas ahora mismo.
Yachiyo: Ya que fueron tan amables@como para comprarlas.
Iroha: ¡Sí!
Tsuruno: Bueno, esto podría ser muy directo de mi parte,@pero como la Chica Mágica más Poderosa,@quisiera decir...
Felicia: ¡Saluuuuuuud!
Tsuruno: [chara:100301:effect_emotion_surprise_0][se:7226_shock]¡¿Quéeee?!
Iroha: *Risita* ¡Salud!
Iroha: Bien entonces, usaremos las flores de cerezo.
Iroha: ¿Ehh?
Yachiyo: ¿Qué paso, Iroha?
Iroha: Oh, no es nada.
