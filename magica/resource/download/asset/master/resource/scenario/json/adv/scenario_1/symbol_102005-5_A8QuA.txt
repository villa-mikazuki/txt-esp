Mayu: No puedo creer que esté saliendo tan bien...
Mayu: Hace un rato estábamos demasiado cansadas@como para movernos. Qué sensación tan@extraña.
Sasara: Tenemos que aprovechar al máximo esta gran@fuerza con la que hemos sido bendecidas.
Sasara: Tal vez no entendamos de dónde vino, pero@ahora nuestro futuro luce mucho mejor...
Emiri: ¡Ah!
Emiri: Parece que Rikki y su grupo lo han hecho@todo a la perfección.
Mayu: ¡La hicieron cambiar de rumbo!
Emiri: ¡¡¡Oooh!!! ¡Estoy muy emocionada ahora!
Mayu: ¡Riko, preparémonos también!
Riko: ¡S-sí!
Yachiyo: ¡Tsuruno! ¡Iroha!
Tsuruno: ¡Estoy en ello!
Iroha: ¡Sí!
Yachiyo: Esas cuatro se están encargando de guiar a@Walpurgisnacht al Parque Kaihin.
Yachiyo: ¡Nuestro trabajo será mantenerla ocupada@mientras hacen eso!
Walpurgisnacht: ¡AAAH JA JA JA JA!
Yachiyo: Urgh. ¡Ya nos está tirando edificios!
Tsuruno: ¡Vamos chicas!
Tsuruno: ¡No subestimen mi suerte cuando estoy en@mi mejor forma!
Tsuruno: ¡Raaah!
Iroha: Oh... Así que si ella corre justo al frente de@donde están todas...
Yachiyo: *Risilla* El edificio colapsado cayó de una manera@tan perfecta que nadie resultó herido.
Sasara: ¡Muchas gracias! ¡Empezaremos a alejarla@ahora, mientras tengamos la oportunidad!
Sasara: Soy un Caballera Mágica...
Sasara: SALVARÉ innumerables vidas. Ni siquiera voy@a perder contra mi padre en ese sentido.
Sasara: ¡Y él ha estado salvando a la gente durante@mucho tiempo!
Sasara: [flashEffect:flashWhite2][bgEffect:shakeLarge][jingle:300405_magia]¡Así que usaré mis habilidades para atraer@el mal hacia mí, y traerte cerca!
Riko: No soy un gato asustadizo.
Riko: Me di una gran discurso motivador hace un@momento, y crucé los dedos. ¡No perderé!
Riko: ¡Utilicé mi poder para hacer feliz a la señorita K@y conseguir más clientes!
Riko: ¡A-Así que ahora voy a usar ese poder para@llamarte aquí también!
Riko: [flashEffect:flashWhite2][bgEffect:shakeLarge][jingle:3035M01]¡Bienvenidoooo!
Emiri: Ups, ¡mejor lo hago yo también!
Emiri: Usando el encanto que conseguí tratando de@acercarme a la chica que tanto admiraba...
Emiri: ¡Voy a atraerla hacia aquí!
Emiri: [flashEffect:flashWhite2][bgEffect:shakeLarge][jingle:300602_magia]Puede que hayamos fallado una vez, pero,@si seguimos así, ¡seguro que va a funcionar!
Emiri: ¡Whoooooooooa!
Emiri: ¡Por Dios, atacar a alguien justo cuando termina@de hablar va TAN en contra de las reglas!
Mayu: [flashEffect:flashWhite2][bgEffect:shakeLarge][jingle:3032M04]¡P-por favooor, detenteeee!
Mayu: ¡Mi poder para detener los ataques es justo lo@que necesitamos en un momento como este!
Emiri: Phew, que mal susto...
Sasara: Pero si tenemos esa habilidad, deberíamos ser@capaces de conducirla con cierta facilidad.
Mayu: Um, no puedo detenerla para siempre, ¡así que@apresúrense y vayan a una distancia segura!
Sasara: ¡¿Qué?! ¡¿Avísanos antes, si?!
Mayu: *Exhalar*
Mayu: Jefe... ¡El deseo que pedí para ti fue útil@después de todo!
Walpurgisnacht: ¡AAAH JA JA JA JAAA!
Mayu: ¡Señorita Walpurgisnacht!
Mayu: No creo que pueda mantenerte a raya mucho@más tiempo...
Mami: Se está moviendo de nuevo. ¡Necesitamos ayudarlas!
Madoka: ¡Entendido!
