Chisato y Takina: [flashEffect:flashWhite2][surround:4061A10]¡YAAAAAAAHHHH!
Takina: ... Éxito.
Chisato: Creo que tenemos la aptitud para esto de ser@Chicas Mágicas, ¿no?
Chica L: Maldita sea... Invoca otra Bruja...
Chica M: ¡Senpai! No... puedo...
Yachiyo: Consumiste demasiada magia al invocar esa@Bruja.
Tsuruno: No se esfuercen más o ustedes mismas se@convertirán en Brujas...
Chica L: ... ¡Entonces, en una Bruja me convertiré!
Chica L: Se acabó mi magia y no hay como salvar@este plan.
Chica L: Pero si me convierto en una Bruja y me vuelvo@loca... y destruyo todo...
Chica L: Podemos aplanar el mundo y empezar de nuevo.
Chica L: Cuando no haya nada, no habrá más maldad.
Chica M: Sí, tienes razón...
Chica M: Si es contigo, Senpai...@Siento que puedo destruir todo.
Chica M: Además, si hago eso, podré estar con mi Senpai@que se convirtió en una Bruja.
Iroha: Eso... ¡No puedes...!
Ui: ¡Alguien traiga una Grief Seed! ¡Rápido!
Tsuruno: ... Espera, ¿sus Soul Gems se dejaron de@oscurecer?
Chica M: ¿Por qué?... ¿Por qué no nos convertimos en@Brujas?
Chica L: ¡No quiero un mundo sin él...!
Chisato: ... Ustedes no caerán en desesperación.
Chisato: Quiero decir, acaban de tratar de destruir el@Universo, ¿saben?
Chisato: La gente con tanta ambición como esa nunca@podrían caer en desesperación.
Chisato: Porque cuando te veo, veo los ojos de alguien@que nunca se daría por vencido.
Chisato: ¡Nadie con una mirada así caería en@desesperación!
Takina: ... Tu desesperación fue por perder gente a@quien atesorabas.
Takina: Así que ya no caerás en desesperación.
Kurumi: "¿Estás diciendo que esta operación fue una@forma de esperanza para esas dos?"
Chica M: Eso... No...
Takina: ... ¿Por qué no conviertes ese deseo de@esperanza en algo más?
Chisato: ¡Hay cosas mucho más divertidas que podrías@hacer!
Takina: Nosotras te investigamos, como tú nos@investigaste.
Chica M: Qué...
Takina: La relación entre tú y tu Senpai, el mundo@sin maldad que querías...
Takina: Creo que dolor de perder un ser querido es algo@que tarda mucho tiempo en sanar.
Takina: ... Y creo que las dos son personas fuertes@que intentan seguir con sus vidas, aún si su@punto de vista está un poco distorsionado.
Takina: Me gusta la idea de deshacerme de la maldad.
Takina: Pero, ¿de verdad este es el futuro que tu@Senpai quería?
Quiero crear un mundo sin maldad.@Por eso me convertí en una Chica Mágica.
Quiero que todos tengan una sonrisa en su cara.@... Claro, eso también te incluye a ti.
Chica M: ¡Mientras sigas a mi lado, siempre estaré@sonriendo!
No, tienes que buscar algo más que eso.@El mundo es más grande de lo que piensas.
Chica M: ... Es verdad.
Chica M: ... Estaba equivocada. Estaba de luto@por mi Senpai.
Chica M: Y lo siento... Senpai.
Chica M: Impuse mis ideas en ti y te hice un reemplazo@para ella...
Chica L: ¿Eh...? ¿Qué...? Tú me llamaste aquí,@¿y te estás rindiendo solo así?
Chica M: ... Es hora de cancelarlo esto.@Cometimos un error.
Chica L: ... ¡No hubo ningún error!@¡Él no estaba equivocado!
Chica L: ¡Tus ideales y los míos eran distintos desde@el principio! ¡Solo te estaba usando!
Chica L: Las dos queríamos un mundo sin maldad, pero@yo quería más que eso... ¡Yo quería destrucción!
Chica L: ¡Nada como tu hermosa ambición!
Chica L: ¡Yo! ¡Quiero destruir el mundo!@ Y... ¡Y...!
Chisato: ... Lo siento.
Chica L: ¡¿Qué...?!
Chisato: Lo siento. No hay nada más que pueda decir@o hacer.
Chisato: Bueno, puedo decirte esto.
Chisato: Sé que hay lugar ahí fuera donde puedas@pertenecer.
¿Tú no piensas que perteneces a otro lugar?
No en un lugar como este@sino un lugar más brillante.
Chica L: ... ¿Quieres que me vaya?
No, solo quiero que seas feliz.
Quiero que estés en un lugar lleno de risas@y un montón de buena comida en tu pansa.
Tienes que encontrar un lugar así, porque algún@día, ya no estaré aquí.
Chica L: ... ¿Por qué estás diciendo las cosas@que dijo él...?
Chisato: No puedo decir que de verdad entiendo el@mundo o el Universo.
Chisato: Pero no creo que sepamos suficiente para@asegurar que todo debería ser destruido.
Chisato: Sabes, de hecho, nosotras trabajamos en una@tienda donde servimos café y dulces tradicionales.
Chisato: *Risilla* Sé que estás pensando que solo@son palabras vacías...
Chisato: Pero, ¿por qué no vienes a probar?
Chisato: Ten, un folleto de nuestra cafetería.
Chisato: ¡El café que nuestro jefe hace está en otro@nivel!
Chisato: Incluso después de tomar café, aún no es@demasiado tarde para conquistar el mundo.
Chica L: No se trata de conquistar el mundo.
Chica L: Como lo pones... Haces que suene como si@un día destruiremos el mundo.
Chisato: Estoy segura de que encontrarás algo más por@lo que vivir para cuando ese día llegue.
Chica L: *Suspiro* Está bien, lo entiendo.@Me rindo ante tu persistencia.
Chisato: *Risilla* ¡Estaremos esperado tu visita!
Felicia: ¡Hey! ¡Estamos en problemas! ¡Esta cosa@va a colapsar!
Ui: ... ¡Está empezando a brillar...!
Kurumi: "Aparentemente los rastros de Chisato y las@demás están desapareciendo de Kamihama.@La luz parece venir de eso."
Iroha: ¿Este es el poder del deseo de Chisato...?
Kurumi: "No sé, pero todo parece estar@desapareciendo... Es como si las cosas@estuvieran volviendo a la normalidad.@La información que tengo de Kamihama@también está desapareciendo de mi@computadora."
Kurumi: "Los controles del dron están empezando a@fallar también, así que supongo que es hora@de decir adiós. Iroha, Touka, Nemu... y todas@las demás. Les deseo lo mejor."
Iroha: Gracias... ¡Cuídate, Kurumi!
Kurumi: "Chisato, Takina... Las estaré esperando en@Café LycoReco."
Takina: Sí.
Chica L: ... Lo siento... Exploté tus sentimientos@por ella.
Chica M: Yo lo siento también... hacerte tomar el@lugar de Senpai.
Chica L: ... *Risilla* Recuerdo que me dijiste que@me parecía a tu Senpai,
Chica L: pero, para decir la verdad, creo que me@parezco más a ti.
Chica M: Sí... eso parece. Me gustaría... si podemos@vernos de nuevo algún día.
Chica L: Sí, hablemos de nuevo... con una taza de café.
Chica L: Hey. Tú.
Iroha: Eh, ah... ¿Sí?
Chica L: ... Por favor cuida de mi... Kohai.
Ui: Desapareció.
Chisato: No lo entiendo por completo, pero...@¿parece que es caso cerrado?
Yachiyo: La única forma de mantener nuestras vidas@diarias es hacer como si nada de esto hubiera@sucedido.
Felicia: Ehh... ¿Quieres decir que Chisato y Takina se@olvidarán de nosotras?
Ui: Y eso debe significar que nosotras también nos@olvidaremos de ellas...
Tsuruno: Que triste...
Takina: Aunque, se supone que nunca nos debimos@haber conocido.
Chisato: Dices eso, pero te ves como si estuvieras@a punto de llorar, Takina.
Takina: ¡No es cierto!
Chisato: *Risilla* Gracias por todo, Iroha
Chisato: Y ustedes también. Yachiyo, Tsuruno, Ui,@Felicia, y...
Sana: Ah... Sana Futaba...
Chisato: ¡Y Sana! ¡Fue muy divertido estar en@Kamihama!
Takina: Estoy de acuerdo... Fue divertido. Sigamos@esforzándonos juntas,
Takina: como aquellas de diferentes mundos que@afrontas enemigos distintos...
Iroha: Sí, como Chicas Mágicas...
Chisato: ¡Y como Lycoris!
Chisato: Muy bien... ¡Nos vemos luego!@¡Fue genial conocerlas!
Takina: ¡Hasta la próxima!
