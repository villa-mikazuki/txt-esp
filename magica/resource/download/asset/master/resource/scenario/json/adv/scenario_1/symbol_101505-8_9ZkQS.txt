Iroha: [chara:100100:effect_shake][flashEffect:flashRed2][bgEffect:shakeLarge][se:3001_chara_damage]Krrrgh... ¡Augh!
Alina: *Risilla* ¡Delightful! Me siento refrescada ahora.
Iroha: *Huff* *Puff*...
Iroha: (¡Si no libero mi Doppel, puede que@que no salgamos de aquí!)
Sana: ¿Por qué...?
Homura: Madoka, ¿estás bien?
Sana: ... Todas siguen...
Madoka: S-sí...
Madoka: Lo siento, Homura... estoy bien...
Sana: ¿Por qué todas siguen... ignorandome?
Homura: ¿Qué hacemos...?
Homura: Todavía tenemos que encontrar a Mami...
Sana: No puedo quedarme mirando...
Sana: ¡Necesito pelear también!
Sana: [flashEffect:flashWhite2][surround:1004A04]¡Jyaaaaaah!
Alina: [flashEffect:flashWhite2][surround:1008A02]No tienes qué, ¿Sabes?
Sana: [bgEffect:shakeLarge][flashEffect:flashRed2][surround:1008A03][chara:100400:effect_shake]¡Auuugh!
Alina: ¿Por qué crees que no te golpeé con@mi ataque también?
Alina: Evitaste que esa Uwasa se sintiera sola,@que se volviera loca...
Alina: Cumpliste un papel importante.
Alina: Así que me gustaría que se quedaran ahí y se@hicieran compañía entre ustedes. ¿Understand?
Sana: P-pero... Eso no significa...
Sana: ¡No puedo quedarme aquí mirando mientras las@demás se lastiman!
Alina: De cualquier forma yo, Alina, me retiro.
Alina: ¡Bye!
Iroha: ¿Qué?
Alina: Oh, ¿Querías que las rematara?
Alina: Lo siento, ese no es mi estilo.@No me gusta acabar las cosas de esa manera.
Alina: ¿Ustedes quieren sacar a esa chica de este@lugar, ¿Correct?
Alina: Pueden hacerlo ahora si quieren.
Alina: Pero después de sufrir tal daño de mi Doppel,@me pregunto... ¿Pueden vencer a un Uwasa?
Alina: Prepárense para ser aplastadas por su propio plan.
Alina: ¡Ah ja ja ja!
Alina: Ja...ja...
Alina: De solo pensalo, me excito...
Alina: Espero a volver maás tarde para ver@qué queda de ustedes.
Iroha: ¿Esa es la chica que quiere liberear a todas@las Chicas Mágicas?
Iroha: ¿Qué SON estas Magius?
Sana: ...
