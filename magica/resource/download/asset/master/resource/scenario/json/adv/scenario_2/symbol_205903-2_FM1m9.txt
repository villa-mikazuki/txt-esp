Mami: ¿Por qué me han llamado de repente?@¿Ha ocurrido algo?
Touka: ¡Es una emergencia! [chara:100701:lipSynch_0][wait:1.0][chara:100701:cheek_0][chara:100701:face_mtn_ex_010.exp.json][chara:100701:lipSynch_1]Bueno, no, no lo es.
Alina: Hay algo que nos gustaría que hicieras.
Mami: Si es por la liberación, estoy feliz de ayudar.
Mami: Pídanme lo que sea.
Nemu: Eres muy confiable.
Nemu: Cierra los ojos, Mami.
Mami: ¿Así?
Nemu: Justo así.
Nemu: ...
Nemu: Por lo que veo, la fusión con el [textBlue:Rumor] está@completa.
Nemu: Y ahora, no sientes ninguna duda o@incomodidad, ¿verdad?
Mami: No, claro que no...
Mami: Solo mi propósito celestial.
Mami: Las Magius son las únicas que pueden@liberarnos...
Mami: Haré cualquier cosa para que esa salvación@sea una realidad.
Nemu: Jeh. Parece que ahora está totalmente@modificada.
Nemu: Ahora vamos. No podemos hacer esperar a@todas.
Touka: *Risilla* [chara:100701:lipSynch_0][wait:1.0][chara:100701:cheek_0][chara:100701:face_mtn_ex_010.exp.json][chara:100701:lipSynch_1]Parece que se ha fusionado@totalmente con el [textBlue:Rumor].
Alina: ¡Aja! Magnific.
Mami: Para entonces era muy conocida entre@las Plumas como su "salvadora"...
Mami: Las Magius no se olvidaron@de utilizar eso en su beneficio.
Nemu: Muchas de ustedes probablemente ya la@conocen.
Nemu: Pero nos gustaría presentarla oficialmente.
Nemu: Me complace presentarles...
Nemu: ¡A nuestra santa y símbolo de la liberación,@Mami Tomoe!
Mami: Encantada de conocerlas. O mejor dicho...@Hola de nuevo a algunas de ustedes.
Mami: Soy Mami Tomoe.
Pluma Blanca: ¡Oh! ¡Es ella!
Pluma Negra: La conozco...
Pluma Blanca: Ella nos salvó durante esa dura batalla.
Pluma Negra: Ella fue la que compartió sus Grief Seeds con@nosotras.
Pluma Blanca: Ella es la santa...
Pluma Negra: Ella nos llevará a la liberación.
Mami: Trabajemos juntas por la liberación.
Plumas: ¡Yeeeeeeaaahhhhhh!
Mami: Y así... Las Plumas me recibieron@con los brazos abiertos.
Mami: Sin embargo, eso también significó el fin@de la voz en mi cabeza que había estado@reprimiendo durante tanto tiempo.@Ya no podía escucharla.
Mami: Empecé a olvidar las cosas que habían sido@tan importantes para mí cuando solo@era Mami Tomoe, la Chica Mágica.
Mami: Empecé a dar prioridad a todo aquello que@fuera por Magius, y por la liberación,@por encima de cualquier cosa.
Mami: Fue entonces cuando ocurrió...
