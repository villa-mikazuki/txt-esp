Iroha: ...
Iroha: Eres la misma Pluma Negra de antes,@¿no es cierto?
Pluma Negra: ¿Y eso qué?
Pluma Negra: Puedes intentar convencerme todo lo que quieras,@pero no voy a ceder.
Pluma Negra: La Plumas de aquí an jurado su lealtad@a la liberación más que a Magius.
Pluma Negra: Y de la misma manera...
Pluma Negra: Tenemos fé en lo que ella cree...@Ella nos protegió.
Iroha: Pero no hay garantía de que su plan para@liberar a las Chicas Mágicas sea exitoso.
Iroha: Están usando Brujas para hacerlo, así que algo@aún pero podría suceder.
Pluma Negra: Nos preocuparemos de eso cuando suceda.
Pluma Negra: Nuestros destinos nos alcanzarán tarde o@temprano.
Pluma Negra: Y si tenemos que afrontar nuestro destino de culquier@manera, me aferraré a esta pequeña posibilidad.
Pluma Negra: No me importa que las Magius nos puedan@estar utilizando.
Iroha: Me parece algo ingenuo.
Pluma Negra: No espero que entiendas.
Pluma Negra: Tú eres alguien a quien no le da miedo afrontar@su destino como una Chica Mágica.
Iroha: No, también tengo miedo. Estoy aterrorizada de volverme@una Bruja.
Pluma Negra: Entenderías si hubieras visto a alguien cercana@tí convertirse en una delante de tus ojos.
Iroha: ¿Alguien cercana?
Pluma Negra: Mi hermana menor se convirtió en una Bruja.
Iroha: ¡¿...?!
Iroha: Tu hermana menor...
Pluma Negra: Sí.
Pluma Negra: Desde que la vi convertirse en una de esa@cosas, no he podido aceptar mi destino.
Pluma Negra: Pero si logramos liberar a todas las Chicas Mágicas,@nos salvaremos de eso.
Pluma Negra: Y sería una manera perfecta de honrar a mi@hermana.
Iroha: ...
Iroha: Y-yo también tengo una hermana menor.
Pluma Negra: ¿Ah, sí?.
Iroha: No sé si está muerta o viva. Ella solo@existe en mis recuerdos.
Iroha: Pero todavía estoy determinda a encontrarla algún día.
Pluma Negra: Lo que pasó con tu hermana no tiene@nada que ver con la mía en lo absoluto.
Iroha: No creo que mi hermana querría esto.
Iroha: Esta salvación que viene de@sacrificar tantas vidas.
Pluma Negra: Ustedes dos beden ser muy fuertes, entonces,@como para decir eso con tanta confianza.
Pluma Negra: Mi hermana y yo nos hicimos añicos del@sufrimiento y deseamos escapar de nuetro destino.
Pluma Negra: No toleraré que nos hagas menos por eso.
Pluma Negra: No actúes como si tú y yo fuéramos iguales.
Pluma Negra: Estoy determinada a hacer cualquier sacrificio.
Pluma Negra: Haré mi propio deseo egoísta realidad.
