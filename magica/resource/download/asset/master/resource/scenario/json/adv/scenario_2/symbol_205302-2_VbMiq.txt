Kaede: ¡Um!@¡P-pero!
Mami: Oh, vamos...
Kaede: Yo... realmente...@[chara:101100:motion_100:cheek_2:face_mtn_ex_060.exp.json]¡¡¡realmente, lo siento mucho!!!
Mami: ¡Aquí vamos!@[se:5002_shooting_02_s]¡¡¡Hahhh!!!
¡¿?!
Kaede: ¡Ugh!
Mami: ¡Concéntrate!@[se:5002_shooting_02_s]¡¡¡Sigue moviendote!!!
¡¡¡!!!
Kaede: ¡Oof!
...
Kaede: ¡¿H-huh?!@[chara:101100:cheek_2:face_mtn_ex_031.exp.json]¡¿Viene hacia mí?!
¡!
Kaede: ¡Ahh!
Mami: ¡No te dejaré!@[se:5002_shooting_02_s]¡Haaaaah!
¡¿?!
Kaede: Um, yo...@No pude... hacer nada...
Kaede: Yo... realmente...@[chara:101100:motion_200:cheek_2:face_mtn_ex_060.exp.json]¡¡Lo siento mucho!!
Mami: No pasa nada.@No te preocupes.
Mami: Me dejaste tomar la Grief Seed,@después de todo.
Kaede: ¡Oh!@Eso es, quiero decir...
Kaede: No usé mucha magia,@así que no la necesito...
Mami: Aún así.
Mami: Parece que esta ciudad está plagada@de Brujas. Y poderosas, además...
Mami: ¿Tú... vas a estar bien por tu cuenta ahora?
Kaede: ¡Ah!@B-bueno... ¡Sí!
Kaede: Normalmente estoy con mi equipo.
Kaede: Así que antes, si me metía en algún@serio problema...
Kaede: Iba a llamar a mi equipo.
Mami: (Eso es exactamente lo que me dijo@la chica que conocí antes...)
Rena: No soy tonta. No suelo luchar sola@contra las Brujas de esa manera.
Mami: (Ahora veo...@Ya que las brujas aquí son tan fuertes...)
Mami: (Es habitual enfrentarse a ellas en equipo.)
Mami: Ya veo... Así que si estás con tu equipo,@estarás bien.
Kaede: S-sí[chara:101100:lipSynch_0]...
Mami: ¡A ver qué te parece esto!
Mami: ¡¡¡Tiro Finale!!!!
Kaede: (Ella es tan fuerte...@y tan genial...)
Kaede: (... Pero...@Ella también es muy amable...)
Kaede: (Me pregunto si algún día...)
Kaede: (¿Podré ser... una Chica Mágica como ella?)
Mami: Así que... eres [chara:200500:motion_100:cheek_0:face_mtn_ex_010.exp.json]una de las Chicas Mágicas@de Kamihama, ¿verdad?
Kaede: ¡Hwah!@¡Quiero decir, sí!
Mami: Me estaba preguntando...@[chara:200500:motion_200]¿Podríamos charlar un poco más?
Kaede: Por supuesto...
