Iroha.: Me pregunto cómo estará el papá@de Tsuruno-chan...
Yachiyo: Probablemente no muy bien.
Yachiyo: Tanto su esposa como su madre no han@vuelto a casa hace tiempo.
Yachiyo: Tsuruno era la única en la que él@podía apoyarse.
Iroha.: Sí...
Yachiyo: Él maneja un restaurante, así que lo más@probable es que haga todo lo posible para@mantenerlo abierto.
Yachiyo: Pero no creo que sea una persona tan fuerte.
Yachiyo: Existe la posibilidad de que cierre el@restaurante por un tiempo.
Iroha: Tsuruno-chan y Felicia-chan no están para@ayudar así que podría no tener otra opción.
Yachiyo: No me sorprendería si estuviese poniendo@volantes para tratar de encontrar a Tsuruno.
Yachiyo: Es su amada hija, después de todo...
