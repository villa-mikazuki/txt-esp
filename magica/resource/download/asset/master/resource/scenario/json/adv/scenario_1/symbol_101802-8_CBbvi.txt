Yachiyo: ¡Hah!
Yachiyo: ¡Yaaaaaah!
Pluma Blanca: [flashEffect:flashWhite2][surround:7151A01][bgEffect:shakeLarge]¡Vva...aaah!
Yachiyo: [flashEffect:flashWhite2][se:4050_landing_jump][bgEffect:shakeLarge][chara:100200:effect_shake]¡Ugh!
Kanagi: ¡Yachiyo!
Yachiyo: (No podemos pelear en medio de la ciudad@de esta manera.)
¡¿Qué fue ese ruido?!
Yachiyo: ¡¿...?!
Yachiyo: (No. ¡Alguien viene!)
Pluma Blanca: ...
Kanagi: Al menos parece que no harán daño al público@en general...
Yachiyo: Sí...
¿Qué demonios? ¿Qué pasó aquí?
Kanagi: No hay nada extraño aquí.
Yachiyo: No te preocupes por nosotras.@Creo que solo estabas oyendo cosas.
No, definitivamente escuché algo...
[chara:801100:effect_emotion_surprise_0][se:7226_shock]¡Ey, vaya, es Yachiyo Nanami!@¡Te he visto en revistas!
Yachiyo: (Ahh... No tengo tiempo para esto...)
[bgEffect:shakeLarge]¡Ahí estás, Take! ¡¿Dónde estabas?!
[bgEffect:shakeLarge]¡Deja de hacer el tonto y vuelve al trabajo!
Take: ¡¿A quién le importa el trabajo ahora mismo?!
Take: ¡Mira, es Yachiyo Nanami!
[bgEffect:shakeLarge]¡No me importa!
Padre de Tsukasa: Tsukasa también se fue a otro sitio@sin hacer sus tareas.
Padre de Tsukasa: Estoy rodeado de holgazanes...
Take: Probablemente se escapó porque le das@demasiado trabajo.
Yachiyo: Tú también oíste eso, ¿no?
Kanagi: Definitivamente dijo Tsukasa. Y el cartel confirma@que estamos en el lugar correcto.
Yachiyo: Amane... Estudio de Bambú...
Take: ¡Ah, sí, señorita Nanami! ¿Podría firmar esto?
Padre de Tsukasa: ¡Cállate y pule el suelo!
Padre de Tsukasa: ¡Te lo mereces por holgazanear!@¡Púlelo hasta que brille!
Take: ¡Está pidiendo demasiado, señor!
Yachiyo: Um, discúlpeme. ¿Tienes un momento?
Padre de Tsukasa: ¿Hm? Sí, ¿qué pasa?
Yachiyo: ¿No está Tsukasa aquí?
Padre de Tsukasa: ¿Eres amigo de ella?
Padre de Tsukasa: Es que no ha venido a casa desde hace unos días.
Padre de Tsukasa: Si la ves, ¿puedes decirle algo de mi parte?
Padre de Tsukasa: Dile que no estoy enfadado y que vuelva@pronto a casa.
Yachiyo: (No ha venido a casa...)
Yachiyo: (Lo mismo pasó con Tsuruno.)
Yachiyo: Gracias.
Yachiyo: Se lo diré cuando la vea.
Kanagi: ¿Más razones para sospechar del [textBlue:Rumor]?
Yachiyo: Sí...
Kanagi: No entiendo...
Kanagi: ¿Por qué quieren aniquilar tanto a las@Chicas Mágicas de Kamihama?
Yachiyo: Tendría sentido si estuvieran tratando de@obtener energía.
Yachiyo: Pero no están tratando de sacar ningún Doppel.
Yachiyo: Está claro que simplemente están atacando e@intentando matarnos.
Yachiyo: Entiendo que estamos en su camino, pero no@veo ningún beneficio en este enfoque...
Kanagi: Hm...
Kanagi: No tenemos forma de saberlo ahora mismo.
Kanagi: Su objetivo no está del todo claro.
Kanagi: Nuestro único recurso es intentar detenerlas.
Yachiyo: Sí...
Yachiyo: Para ello tendremos que deshacernos de ese@[textBlue:Rumor].
Kanagi: Podríamos intentar leer la mente de esas Plumas.
Kanagi: Es la forma más rápida de averiguarlo.
Yachiyo: Muy bien, vayamos a otro sitio.
Yachiyo: Encontraremos más Plumas reunidas en zonas@donde hay Chicas Mágicas.
Yachiyo: ...
Kanagi: ¿Qué pasa?
Yachiyo: Sigo sin tener señal...
