Felicia: ¡Como ya dije!@¡Simplemente no me gusta mucho la Navidad!
Felicia: Así que no quiero ir a la fiesta.@Eso es todo, ¿¡okey!?
Iroha: Felicia...
Sana: ¡Pero...!
Momoko: ¿Eh? ¿¡No vas a ir!?
Rena: Hmmm...
Kaede: ¿¡Qué!? ¿Por qué?
Iroha: ¿Eh?
Iroha: ¡Momoko! ¿¡Cuándo llegaste!?
Yachiyo: ¿¡De dónde han salido todas ustedes!?
Rena: Dejaste la puerta abierta.
Kaede: Así que Rena entró sin más...
Momoko: Veníamos a decirte que ya@nos encargamos del pastel...
Momoko: ¿De qué estaban hablando?
Felicia: Bueno, ya nos escuchaste...
Felicia: ¡He dicho que NO quiero ir@a ninguna fiesta de Navidad!
Kaede: Aww, pero será muy divertido@tener una fiesta con todas...
Rena: ¿No vas a ir? ¿A pesar de que yo sí?
Yachiyo: (Momoko, tan inoportuna como siempre...)
Yachiyo: (No me gusta adónde va esto.)
Momoko: Bueno, no podemos obligarla a participar.
Kaede: Sí, aunque es una pena...
Kaede: ¿No es así, Rena?
Rena: ...
Rena: ¡Bleh!
Kaede: ¿Eh? ¿Te encuentras bien?
Rena: Es solo que... ¡ugh! Muy bien, escúchame, ¡tú!
Rena: Tú crees que la Navidad es muy aburrida y@nada divertida, ¿¡no es así!?
Felicia: ¡Sí, así es!
Rena: Peeeero...
Rena: ¿¡No te PARECE divertido!? ¡Es NAVIDAD!
Kaede: (¿"parece"?)
Rena: Puedes, ya sabes... ¡darlo todo!@Todos se lo pasan bien.
Felicia: ¡No, es solo una excusa para ser tonto y@hacer estupideces!
Felicia: ¡Eso es... solo Navidad! ¡Eso es todo lo que es!
Rena: ¿¡Qué demonios!?@¡De ninguna manera es "solo" Navidad!
Felicia: ¿¡Entonces qué tiene de divertido!?
Rena: ¿Eh? Bueno... verás...
Rena: ¡...!
Rena: ¡Santa!
Momoko: ¿Santa?
Rena: ¡Santa te trae regalos!
Rena: ¡Esperar a que venga a traerlos@es SUPER divertido!
Rena: (¡Jaja! ¡Toma esa!)
Kaede: Rena... um...
Felicia: ¡Santa no es real! ¡Él no es real! ¡NO ES REAL!
Rena: ¿¡Qué...!? ¡Cómo te atreves!
Rena: ¡Él es real! ¡ES REAL!
Felicia: ¡NO, NO LO ES!
Iroha: Y-Yachiyo... Esto está empeorando rápidamente...
Yachiyo: Ya se están gritando la una a la otra...
Yachiyo: ¡Oigan! ¡Basta, las dos!
Rena: ¡Pero él ES real!
Momoko: ¡Rena!
Felicia: ¡NO lo es!
Iroha: ¡Felicia!
Rena: *Jadeo* *Jadeo*
Felicia: *Jadeo* *Jadeo*
Felicia: ¡Me harté! ¡Ya lo decidí!
Felicia: ¡NO voy a ir a la fiesta de Navidad,@de ninguna manera!
