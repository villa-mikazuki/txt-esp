Madoka: "Bueno, será mejor que vaya..."
Homura: 'Pero... ¡Mami! ¡Esa cosa la mató!'
Madoka: "Lo sé..."
Madoka: "Pero alguien tiene que detener@a [textRed:Walpurgisnacht], y yo soy la única@que queda que puede hacerlo."
Homura: (Si ella supiera que [textRed:Walpurgisnacht] está en@camino...)
Homura: (Madoka definitivamente se pondría en@la línea de fuego para proteger a todos.)
Homura: (Pero no hay manera de que podamos ganar@contra ella tal y como estamos ahora.)
Homura: (Alguien terminará sacrificándose...)
Homura: (¡Tengo que evitar eso a toda costa!)
Homura: (Incluso si eso significa abandonar Mitakihara.)
Homura: (Si este Laberinto es lo suficientemente@fuerte...)
Homura: (Podríamos pasar [textRed:la noche de Walpurgis]@aquí adentro.)
Homura: (Pero fue hecho por Alina...@No hay garantía de que sea seguro.)
Homura: (Creo que... realmente sería mejor para@nosotras evacuar a Kamihama.)
Homura: (Mami también está allí.)
Homura: (Así que no creo que Madoka y Sayaka se@nieguen a ir...)
Homura: ...
Madoka: Homura, va a estar bien.
Sayaka: ¡Sí, estamos aquí contigo!@No pongas esa cara, ¿está bien?
Madoka: Podemos descansar un poco, y luego las tres@saldremos de aquí.
Madoka: Y esta vez definitivamente traeremos a Mami@con nosotras.
Sayaka: ¡Sí, lo haremos! Así que no te preocupes.
Homura: Madoka... Sayaka...
Homura: (No quiero perderlas.)
Homura: Gracias...
Sayaka: Listo, ¿tal vez eso alivió la tensión?
Madoka: ¡Parece que lo hizo!
Homura: (Esta vez, voy a salvarlas a todas.)
Homura: ¡¿...?!
Sayaka: ¡¿Q-qué?! ¡Todo el Laberinto está temblando!
Madoka: ¡Manténganse alertas!
