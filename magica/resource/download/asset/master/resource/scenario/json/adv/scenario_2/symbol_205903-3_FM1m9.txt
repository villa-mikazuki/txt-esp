Mami: Mami: Empecé a dar prioridad a todo aquello que@fuera por Magius, y por la liberación,@por encima de cualquier cosa.
Mami: Fue entonces cuando ocurrió...
Sayaka: ¿Qué te ha pasado, Mami?
Sayaka: No eres el tipo de chica que hace cosas así.
Mami: Esto es lo que soy ahora...
Mami: Sayaka, aléjate de ellas...
Mami: Una vez que las elimine, escucharé lo que@tengas que decir.
Mami: Sayaka trató de detenerme, y aunque@yo era la que estaba equivocada...
Sayaka: Augh... [chara:200400:lipSynch_0][wait:1.3][chara:200400:cheek_0][chara:200400:face_mtn_ex_060.exp.json][chara:200400:motion_400][chara:200400:lipSynch_1][flashEffect:flashRed2][surround:2500A03][bgEffect:shakeLarge][chara:200400:effect_shake]¡ghh!
Mami: La lastimé
Mami: Eso no fue todo.
Mami: Yo soy responsable.
Mami: Soy responsable de que te hayas metido en@el mundo de las Chicas Mágicas.
Mami: ... Es por eso que no puedo permitir que se@involucren más de lo que ya están.
Sayaka: ¡Esa no es razón para liberar Brujas en@Mitakihara!
Sayaka: ¡Te has vuelto loca, Mami!
Mami: Había traído Brujas a Mitakihara,@y trataron de detenerme.
Homura: Pero... luchar en Mitakihara supone el riesgo@de convertirse en Bruja.
Homura: ¿Las trajiste aquí, sabiendo muy bien que eso@podía pasar?
Homura: ¡¿No te importa si nos convertimos en Brujas?!
Mami: Estaba haciendo cosas imperdonables.@Sin embargo, no se dieron por vencidas conmigo.
Madoka: Te están haciendo hacer cosas horribles.@¡No es posible que sean nuestra salvación!
Madoka: ¡Por favor, detén esto!
Madoka: ¡Te aceptamos por lo que eres!
Madoka: ¡Por favor, vuelve con nosotras, Mami!
Mami: Me dijeron lo que más quería escuchar.
Mami: Y sin embargo, no me atreví a@escuchar nada de eso.
Mami: A pesar de todo eso, igual vinieron.
Mami: Deben haber escuchado de Kyoko que@Walpurgisnacht venía a Kamihama.
Mami: Aun así, vinieron corriendo@desde Mitakihara para salvarme.
Mami: Estaban muy alegres de@que estuviera a salvo.
Mami: Me hizo muy feliz. Tan feliz que@no puedo expresarlo con palabras.
Madoka: ¡Te aceptamos por lo que eres!
Mami: Así que yo también quiero estar ahí@para todas ellas.
Mami: Madoka...
Mami: Sayaka...
Mami: Homura...
Mami: Quiero ser alguien que pueda aceptarlas@cuando estén pasando por momentos difíciles.
Mami: No quiero actuar como si fuera alguien@importante y poderosa. Quiero estar a su lado@y compartir los mismos sentimientos desde@el mismo punto de vista.
Mami: Para ello, tengo que enfrentarme a@los crímenes que he cometido.
Mami: Puede que nunca pueda compensarlas,@pero después de que haga lo@que tengo que hacer...
Mami: Tal vez, solo tal vez...@pueda mostrarles quién soy@realmente, con mi debilidad y todo.@Creo que puedo hacerlo.
Mami: Entonces... podría ser capaz de@hablar de nuevo con Kyoko.
Mami: (Es por eso...)
Mami: (Tengo que responder por lo que he hecho.)
