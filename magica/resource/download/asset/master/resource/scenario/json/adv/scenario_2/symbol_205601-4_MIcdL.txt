Homura: ¿Tu deseo?
Madoka: ¿No fue el de curar el brazo de Kyosuke?
Sayaka: Sí, lo fue.
Sayaka: Pero eso no fue todo.
Sayaka: Quería el poder de proteger a todas las@personas que son importantes para mí...
Sayaka: ¡Ese también fue mi deseo!
Sayaka: ¡Quiero protegerlas, Madoka, Homura...@y a Mami también!
Madoka: Sayaka...
Sayaka: Heh, mírame, una novata, hablando en grande@sobre salvar a todo el mundo.
Sayaka: En fin, así es como me siento con todo esto.
Sayaka: ¡Así que, por favor!
Sayaka: ¡Déjenme al menos ver lo que está pasando@en Kamihama por mí misma!
Sayaka: ¡No puedo soportar estar aquí sentada@sin hacer nada!
Madoka: ...
Madoka: ¡De acuerdo!
Homura: Madoka...
Sayaka: [chara:200401:effect_emotion_surprise_0][se:7226_shock]¡¿En serio?!
Madoka: ¡Sí! No podemos simplemente quedarnos@sin hacer nada.
Madoka: Si algo le ocurriera a Mami mientras estamos@esperando...
Madoka: ¡Me arrepentiría para siempre!
Madoka: Si vamos e investigamos nosotras mismas,@¡quizás podamos encontrar algunas pistas!
Sayaka: ¡Madoka!
Homura: ¡Sí, vamos!
Madoka: Homura...
Sayaka: ¡Homura!
Homura: (Sayaka tiene razón, no podemos quedarnos@esperando.)
Homura: (Además, ¡necesito llegar al fondo de lo que@está pasando allí tan pronto como pueda!)
