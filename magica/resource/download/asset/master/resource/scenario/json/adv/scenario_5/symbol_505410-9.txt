Sakura Eterna: | Un tiempo de paz volvió a las Chicas Mágicas@tras su largo y arduo conflicto. |
Sakura Eterna: | Yo también había completado@mis deberes como Uwasa una vez que@mis flores florecieron por completo. |
Sakura Eterna: | Mi [textBlue:Rumor] fue reescrito, y fui@conectada al servidor de Touka,@renaciendo como una Uwasa@con amplios conocimientos. |
Sakura Eterna: | Y se me dio un solo deber... |
Sakura Eterna: | Juzgar a las que me crearon. |
Sakura Eterna: | El juicio comenzará una vez que@las Chicas Mágicas se hayan reunido@para escuchar a las Magius explicar@todo lo sucedido. |
Yachiyo: *Suspiro*
Tsuruno: ¿Eh?
Tsuruno: ¿Por qué estás mirando tu celular?
Felicia: ¿¡Estás jugando un juego!?
Yachiyo: No. Es solo que tengo mucho en que pensar.
Iroha: ¿Tú también recibiste el mensaje, Yachiyo?
Yachiyo: ¿Tú también lo recibiste?
Sana: Justo estábamos hablando de eso...
Tsuruno: Um, ¿hola?@¡No tengo idea de lo que están hablando!
Tsuruno: ¿¡Están seguras de que quieren dejar a la Chica@Mágica Más Poderosa fuera de su conversación!?
Tsuruno: ¿Hmmm?
Yachiyo: En otras palabras, ¿te sientes excluida?
Tsuruno: ¡Sí!
Felicia: ¡Yo también!
Yachiyo: El asunto no merece una respuesta@tan entusiasta, ¿saben?
Tsuruno: ¡Lo entiendo, es un asunto serio!
Felicia: Entonces, ¿qué pasa?
Iroha: Básicamente, nos han preguntado sobre@qué deberíamos hacer con las Magius.
Yachiyo: Aún tenemos a las cabecillas en nuestras manos.@Estamos discutiendo qué hacer con ellas.
Tsuruno: Las cabecillas... Según mis cuentas,@son seis personas, ¿verdad...?
Iroha: Así es.
Iroha: Touka, Nemu, Mifuyu, Mitama...
Iroha: Y también Tsukuyo y Tsukasa.
Yachiyo: Alina sigue desaparecida hasta el momento...
Felicia: ¿Cuál es el problema? ¿No podemos simplemente@tomar una decisión y seguir adelante?
Tsuruno: No, no, no.
Tsuruno: Los juicios no deberían hacerse tan a la ligera.
Felicia: ¿Por qué no?
Tsuruno: Incluso si quisiéramos, no creo que podamos@tomar una decisión imparcial.
Tsuruno: Iroha y Ui son nuestros amigas.
Tsuruno: Así que nos gustaría ayudar a las seis.
Sana: Exactamente...
Sana: Pero dudo que todas estén contentas@con ese resultado...
Felicia: ¡Entonces vamos a hablarlo entre todas!
Iroha: No creo que eso funcione.
Iroha: Imagino que hay personas que se toman en serio@este juicio y otras que no.
Iroha: Si organizáramos una reunión, creo que solo@se presentarían quienes quieran castigarlas.
Felicia: ¿Entonces qué hacemos?
Iroha: Ojalá tuviéramos más tiempo para escuchar@las opiniones de todas al respecto...
Sakura Eterna: | Parece que yo podría ser de ayuda. |
Iroha: [chara:100101:effect_emotion_surprise_0][se:7226_shock]¡Eek!
Ui: ¡Hola!
Touka: Disculpen la intromisión.
Nemu: Hola.
Iroha: Bienvenidas.@Ustedes tres llegan bastante tarde hoy.
Ui: Estuvimos charlando mientras caminábamos@y perdimos la noción del tiempo.
Iroha: ¿De qué estaban hablando?
Touka: A decir verdad, estábamos discutiendo lo mismo@que ustedes.
Iroha: ¿Te refieres a lo que deberíamos@hacer con ustedes?
Nemu: Sí. No queremos dejar las cosas sin resolver.
Nemu: Así que tengo una propuesta.@¿Por qué no hacemos un juicio?
Yachiyo: Ninguna de nosotras está cualificada@para emitir un juicio apropiado.
Nemu: Así que pensé en pedirle ayuda@a la Sakura Eterna.
Sakura Eterna: | Sí, ahí es donde entro yo. |
Felicia: ¿Eh...? No lo entiendo.
Nemu: La Sakura Eterna ha sido reescrita para@que pueda participar en nuestro mundo.
Nemu: Reescribí su Rumor.
Nemu: Ahora está conectada al servidor de Touka,@así que tiene un conocimiento general de todo.
Tsuruno: ¡Oh, ya veo!
Tsuruno: ¿Así que puedes ajustar su historia@para que sea una jueza imparcial?
Nemu: Sí, precisamente.
Touka: Las Uwasa deben actuar de acuerdo a su@contenido, para no tener alguna preferencia.
Touka: ¡Así podrá tomar una decisión sistemática!
Ui: Creo que es una buena idea.@¿Qué opinas, hermana...?
Iroha: Hmm... Hay algo que no me cuadra...
Felicia: Si pueden reescribir todo eso,@¿no pueden hacer que ustedes ganen?
Iroha: Oh, podría ser eso.
Tsuruno: Si le das los datos correctos a la Sakura Eterna,@¡podrías salir fácilmente impune!
Tsuruno: ¡Bien pensado, Felicia!
Felicia: ¿¡En serio!? [chara:100501:lipSynch_0][wait:0.8][chara:100501:cheek_0][chara:100501:face_mtn_ex_011.exp.json:motion_200][chara:100501:lipSynch_1][chara:100501:effect_emotion_joy_0][se:7222_happy]¡Yey!
Iroha: También tiene que ser aceptable para las demás.@Quizás debería haber más de un juicio.
Sana: Oh, creo que eso es lo que hacen para@los juicios regulares.
Yachiyo: Ajustémosla un poco,@pero respetando la idea original.
Sakura Eterna: | Así fue como me convertí en jueza y@como se decidió el procedimiento del juicio. |
Sakura Eterna: | Para el primer juicio... |
Sakura Eterna: | Aprenderé todo sobre leyes y otros@conceptos, luego analizaré los delitos de las@acusadas y calcularé qué medidas se pueden@tomar para mitigarlos, y entonces emitiré un@veredicto basado en eso. |
Sakura Eterna: | Luego, para el segundo juicio... |
Sakura Eterna: | Enviaré encuestas explicando@los resultados del primer juicio,@pidiendo a las Chicas Mágicas sus@opiniones para ayudar a emitir@mi segundo veredicto. |
Sakura Eterna: | Si la respuesta es escasa o nula,@se dará por concluido con el primer juicio@por falta de interés en el caso... |
Sakura Eterna: | Y finalmente, para el tercer juicio... |
Sakura Eterna: | Enviaré los resultados del segundo juicio@junto con una pregunta de aprobación@o desaprobación, y si el número de@disconformes supera la mayoría,@tres representantes emitirán el veredicto@final. |
Sakura Eterna: | Y los castigos se determinaron@de las siguientes maneras: |
Sakura Eterna: | Pena máxima:@Destrucción de la Soul Gem. |
Sakura Eterna: | Pena incapacitante:@Modificación de los anillos para que@se rompan si se transforman. |
Sakura Eterna: | Pena restrictiva:@Modificación de los anillos para que@la transformación cause dolor. |
Sakura Eterna: | Supervisión:@Equivalente a la libertad condicional. |
Sakura Eterna: | Yo seré la encargada de la ejecución. |
Sakura Eterna: | De este modo se planificaron los juicios,@se concretó el contenido y se avisó a las@Chicas Mágicas para evitar confusiones. |
Ui: ¿Estás segura de que las penas@deberían ser tan peligrosas?
Touka: Hemos aceptado los términos,@así que no hay problema.
Yachiyo: ¿Así que quieres que las otras Chicas Mágicas vean@que ustedes se están tomando esto en serio?
Nemu: Exactamente.
Yachiyo: Estarán bien...@Ambas son jóvenes después de todo...
Sakura Eterna: | Por desgracia, no era tan sencillo. |
