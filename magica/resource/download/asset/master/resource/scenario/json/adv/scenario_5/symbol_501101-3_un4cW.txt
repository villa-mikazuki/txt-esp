Tsuruno: ¡Dormimos hasta el amanecer! ♪@¡Y ya es Año Nuevo otra vez! ♪
Tsuruno: ¡Hurra! ¡Feliz Año Nuevo!
Yachiyo: Todos los días son felices para ti...@Pero Feliz Año Nuevo.
Iroha: ¡Feliz Año Nuevo, Tsuruno!
Sana: ¡Feliz Año Nuevo!
Felicia: ¡Feliz Año!
Tsuruno: ¡Traje lo que todas estaban esperando!@¡La comida de Año Nuevo especial de Banbanzai!
Felicia: ¡Woooa!
Felicia: (Oye, espera un segundo.)
Iroha: ¡Está en un bento gigante de 3 pisos...!
Iroha: (Pero es la cocina de Tsuruno, así que...)
Tsuruno: ¡Mmhm! ¡Miren esto!
Tsuruno: Y para el primer nivel tenemos... [chara:100301:lipSynch_0][wait:1.0][chara:100301:cheek_1][chara:100301:face_mtn_ex_011.exp.json][chara:100301:lipSynch_1]¡ta-da!
Iroha: ... Arroz frito.
Yachiyo: Ugh, por supuesto.
Tsuruno: El siguiente nivel es... [chara:100301:lipSynch_0][wait:1.0][chara:101001:motion_100][chara:100301:lipSynch_1]¡ta-da!
Sana: ¡Arroz frito!
Tsuruno: ¡No, no, no! Esto es...
Tsuruno: ... ¡arroz frito con camarones!
Yachiyo: Es básicamente lo mismo.
Tsuruno: ¡Y el tercer nivel es...!
Iroha: ¡Ah! Este tiene salsa...
Iroha: ... sobre arroz frito.
Tsuruno: ¡Eso es todo! [chara:100301:lipSynch_0][wait:1.0][chara:100301:cheek_1][chara:100301:face_mtn_ex_011.exp.json][chara:100301:lipSynch_1]¡Adelante, a comer!
Yachiyo: Es puro arroz frito...
Felicia: Bueno, ¿y qué esperaban?
Felicia: ¡Arroz frito es todo lo que necesitas! [chara:100501:lipSynch_0][wait:1.0][chara:100501:motion_200][chara:100501:cheek_0][chara:100501:face_mtn_ex_011.exp.json][chara:100501:lipSynch_1]¡A comer!
Iroha: *Risilla* ¡Yo también!
Sana: Yo... ¡Yo también quiero!
Tsuruno: ¡Por favor, sírvanse!
Tsuruno: Y después de comer... [chara:100301:lipSynch_0][wait:1.0][chara:101001:motion_101][chara:100301:cheek_1][chara:100301:face_mtn_ex_011.exp.json][chara:100301:lipSynch_1]¡Miren aquí!
Iroha: ¿Hmmm? ¿Esas son cartas para jugar?
Tsuruno: ¡Así es! Pero no solo eso...
Tsuruno: ¡En realidad se trata de mi propio@juego de cartas original de Tsuruno!
Tsuruno: También conocido como...
Tsuruno: ¡Cartas Yui!
