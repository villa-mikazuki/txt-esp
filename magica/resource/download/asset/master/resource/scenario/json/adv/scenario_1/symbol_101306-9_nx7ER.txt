[se:7213_witch_noise_01][flashEffect:flashWhite2]｜¡¡蜍輔縺碁縺縲縺謌代譚縺!!｜
Iroha: ¡Agh![se:3001_chara_damage]
Yachiyo: ¡Tamaki-san!
Iroha: Es solo un rasguño.@Estoy bien, no se preocupe.
Yachiyo: Pero es obvio que te estás@moviendo más lento de lo habitual.
Yachiyo: Tsuruno, ¿aún puedes pelear?
Tsuruno: ¡Mi magia se acabará si esto dura más tiempo!
Yachiyo: Incluso la Más Poderosa es impotente@si sus poderes no funcionan.
Yachiyo: No hay nada que podamos hacer...
Iroha: Yachiyo-san... sé que es frustrante,@pero, realmente deberíamos huir.
Yachiyo: Sí, creo que tienes razón...
Yachiyo: Tsuruno, ¡hora de retirarse!
Tsuruno: ¡Sí Sí, maestra!@¡Que comience la Operación: Retirada!
Tsuruno: Listas... ¡VAMOS!
Yachiyo: ¡Oye, espera!@¡Piensa en tus aliadas por un segundo!
Yachiyo: Cielos.
Iroha: Vamos, Yachiyo-san—
Iroha: ¿Eh...?
Yachiyo: ¡¿Tamaki-san?!
Iroha: Lo-lo siento.@De repente, me siento débil—
Iroha: Ugh... ngg...
Yachiyo: ¡¿...?!@Tu Soul Gem...
Iroha: ¿Mi Soul Gem...?
Iroha: Ah, está realmente oscurecida...
Iroha: Pensé que aguantaría un poco más...
Iroha: No poder ver a Ui...@Supongo que fue un poco impactante...
Yachiyo: En un estado como ese...@¡¿Por qué usaste esa Grief Seed en mí?!
Iroha: No se preocupe, Yachiyo-san...@Simplemente no puedo moverme mucho—
Yachiyo: ¡Deja de intentar moverte!@Te llevaré en mi espalda.
Iroha: Lo siento por ser una carga...
Yachiyo: Me dijiste que no morirías delante de mí.
Yachiyo: No lo olvides.
[se: 7213_witch_noise_01] ｜ ¡¡ 荳 縺 吶 縺 代 譚 膂 繊 !! ｜
Yachiyo: Qué inoportuno...
Yachiyo: Lo siento, Tamaki-san...@Esto se va a poner un poco difícil.
Iroha: Sí...
[se: 7213_witch_noise_01] ｜ ¡¡¡¡ 繧 縺 縺 滂 !!!! ｜
Yachiyo: ¡No!@¡No puedo evitar este!
Tsuruno: ¡¡Vals de Llamas!!
[se: 8002_flame_01_h] ｜ ¿¡¿¡縺 縺!?!? 荳 菴 薙 縺 薙... ｜
Tsuruno: ¡Lo siento! Me adelanté a mí misma,@quiero decir, ¡a ustedes!
Tsuruno: ¡Escapémonos antes de que@la Uwasa se recupere!
Yachiyo: Nos salvaste, Tsuruno...@Realmente eres la Más Poderosa.
Tsuruno: Jejeje.
Yachiyo: ¿Estás bien después de usar@una magia tan fuerte...?
Tsuruno: ¿Qué?
Tsuruno: ¡Bueno, ya no puedo usar@ataques fuertes otra vez!
Yachiyo: Y lo dices tan alegremente...
Iroha: Yachiyo-san...
Yachiyo: Por favor... aguanta un poco más...
