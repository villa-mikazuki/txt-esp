Mami: *Llanto* *Llanto*
Mifuyu: ¿Está inconsciente? ¡¿Qué le has hecho?!
Alina: Me sorprende un poco el tono acusador de@tu voz en este momento, pero en fin...
Alina: Simplemente se desmayó después de usar@su Doppel.
Alina: ...
Alina: Más importante, yo, Alina,@tengo una excellent idea.
Mami: Apenas estaba consciente,@pero la conversación de Mifuyu y Alina@se filtraba tenuemente en mi conciencia.
Mifuyu: ...
Mifuyu: Tengo un mal presentimiento sobre esto.
Touka: *Risilla* [chara:100701:lipSynch_0][wait:1][chara:100701:cheek_0][chara:100701:face_mtn_ex_010.exp.json][chara:100701:lipSynch_1]Oh, no seas así. Vamos a escucharla.
Nemu: Es cierto. No hay nada malo en prestarle@atención.
Alina: Llegan tarde.
Touka: ¡Tenemos nuestros propios asuntos, ¿sabes?!
Touka: ¿Hmmm?
Touka: ¿No es esa la famosa veterana de Mitakihara@durmiendo por ahí?
Nemu: Al parecer, los comentarios sobre sus increíbles@habilidades fueron bastante exagerados.
Mifuyu: No... Ella es muy fuerte.
Mifuyu: Habría estado en serios problemas si Alina no@hubiera aparecido.
Alina: Y luego, después de eso, casi mata a mi@adorable Bruja...
Alina: ¡Aaauuuggghhh! ¡¡¡Estoy VERY angry por eso!!!
Nemu: ¿Así que al final no fue rival para la@Bruja mascota de Alina?
Mifuyu: No, solo es la consecuencia de haber liberado@a su Doppel por primera vez...
Mifuyu: Parece que no puede moverse.
Nemu: Ya veo. Podría ser útil como soldado para@nuestra causa.
Nemu: El único problema es que...
Touka: Ella no simpatizaba con nuestros objetivos.
Touka: ¿No es así?
Mifuyu: No. El problema es aún más grave.
Mifuyu: No pudo soportar la verdad sobre cómo todas@nos convertiremos en Brujas...
Mifuyu: Tuvo una reacción mucho más fuerte que@cualquiera de las otras Chicas Mágicas.
Alina: En otras palabras, no pudo soportarlo y tuvo@una pequeña crisis nerviosa.
Touka: Es una pena, teniendo en cuenta lo poderosa@que parece ser.
Nemu: Entonces, ¿Alina?
Nemu: ¿No es hora de que tu "excellent idea" haga@su aparición?
Alina: ¡Aja!
Alina: Si el problema es su mente, podemos@simplemente usar sus poderes.
Mifuyu: Pero el lavado de cerebro no funcionó...
Alina: Esa es una técnica demasiado suave.
Alina: Reescribiremos a "Mami Tomoe" por completo.
Mifuyu: ¿Qué estas sugeriendo?
Alina: Es una pena no haber podido alimentar a mi@Bruja, pero al menos se la daré a una [textBlue:Uwasa].
Nemu: ¿Estás hablando de fusionar una persona con@una [textBlue:Uwasa]?
Alina: Creo que es una very good idea.
Alina: Tú PUEDES hacerlo, ¿verdad?
Nemu: Jeh. Es una idea realmente fascinante.
Nemu: Sería la primera vez que se intenta, pero estoy@de acuerdo. No es imposible.
Nemu: ... Sí. Ya pensé en el Rumor perfecto para ella.
Touka: *Risilla* ¡Yo también me apunto!
Touka: Sería un desperdicio deshacerse de ella ahora.
Touka: ¡Aprovechemos nuestros recursos y@hagamos un buen uso de ella!
Mifuyu: ¡Esperen!
Mifuyu: ¿No creen que es algo inhumano hacerle eso?
Touka: Pero si la dejamos ir, se interpondrá en@nuestro camino.
Touka: ¡Quién sabe, podría atacar a otras@Chicas Mágicas!
Mifuyu: Eso es... osea...
Mifuyu: ...
Nemu: Entonces, empecemos.
Nemu: Transformaremos a Mami Tomoe en el [textBlue:Rumor]@de una santa.
Mami: Entonces me fusionaron con un [textBlue:Rumor].
Mami: Pero al principio, todavía tenía@mi conciencia humana...
