Iroha: El [textBlue:Rumor] está por aquí...
Iroha: Puedo sentirlo...
Yachiyo: Yo también.
Yachiyo: Pero eso no es todo...
Iroha: Touka-chan y las demás están aquí.
Yachiyo: Estarán justo frente a nosotras en cualquier@momento.
???: [flashEffect:flashWhite2][bgEffect:shakeLarge][surround:1008A02]¿Justo enfrente? ¡Qué tal justo encima!
Yachiyo: [flashEffect:flashWhite2][bgEffect:shakeLarge][surround:1008A02]Ugh, conozco este ataque.
Felicia: [flashEffect:flashWhite2][bgEffect:shakeLarge][surround:1008A02]¡Es la chica loca!
Sana: Alina...
Alina: Así que la Rubia molesta volvió.@Y tú también, Chica Invisible.
Alina: Incluso a pesar de que destruyeron mi trabajo,@fui capaz de perdonarlas cuando les lavé el cerebro.
Alina: Pero entonces escaparon. Yo, Alina,@no puedo perdonar eso. ¡Horrible!
Touka: Y yo que pensaba que todo terminaría@tranquilamente por una vez.
Touka: El trabajo de la Más Poderosa está casi terminado.
Mifuyu: Todo es mi culpa. Lo lamento.
Felicia: Es la señorita de blanco.
Mifuyu: (¡Shhh!)
Felicia: ¡¿...?!
Touka: No estoy segura de cómo escaparon.
Touka: Pero tú estabas a cargo cuando paso,@asi que piensa bien en lo que hiciste.
Mifuyu: Sí...
Alina: Bueno, a diferencia de esas Hermanas Flautistas,@esta fue tu primera ofensa.
Alina: Sería un poco duro condenarte como culpable.
Touka: Sí, tienes razón. Se merece una segunda@oportunidad.
Touka: Me alegro de que hayamos considerado la@posibilidad extremadamente baja de que nos@encontraran aquí.
Touka: Debido a eso, nuestro plan para usar a la@Más Poderosa no será un desperdicio.
Touka: *Risita*
Momoko: Así que eso es...
Momoko: Pensaste que no podríamos luchar@si la usabas contra nosotras.
Touka: ¿Quiiiiizás?
Iroha: Detén esto, Touka-chan.
Iroha: No hagas que Tsuruno-chan haga cosas horribles...@Por favor.
Touka: Hmm.
Touka: ¡Nnnnnno! ¡Lastima!
Iroha: Touka-chan.
Touka: Necesitamos que todos se diviertan a montones@en el parque de diversiones.
Touka: Que se diviertan tanto que no quieran@volver a casa.
Touka: Pero como tanta gente quiere entrar,@serán forzados a irse.
Touka: ¡A irse de este mundo!
Touka: ¡¿Estás de acuerdo en que las emociones de todos@están a punto de explotar, cierto?!
Touka: ¡¿No te parece que vamos a sacar un montón de@energía de esto?!
Touka: *Risita*
Iroha: ¡¿Realmente vas a matar a toda esa gente?!
Touka: Tenemos que hacerlo. Si no, no habrá suficiente@energía.
Iroha: ¿Energía para que? Esto no está bien...
Iroha: Arrastras gente a tus planes y les lavas@el cerebro y las amenazas.
Iroha: Incluso ahora, Tsuruno-chan es parte de un [textBlue:Rumor]@por tu culpa.
Iroha: ¡La gente como tú nunca podría hacer a las Chicas@Mágicas, a todo el mundo feliz!
Touka: ¿Acoso tú si podrías hacer felices a todos,@Iroha Tamaki?
Iroha: No tengo la respuesta a eso.
Iroha: Pero sé que lo que están haciendo está mal.
Alina: Sí, lo entendemos. Nunca estarás de acuerdo@con nosotras. Es suficiente.
Alina: Ahora tendrás que pasar por encima nuestro.
Touka: Vamooos, ¿en serio? Estaba tratando de ganar tiempo.
