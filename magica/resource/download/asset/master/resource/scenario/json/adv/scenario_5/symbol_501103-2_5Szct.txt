Tsuruno: ¡Bien! ¡Tenemos que participar en esto!
Sana: ¿Tenemos?
Tsuruno: ¡Yep!
Tsuruno: [chara:100301:effect_emotion_angry_1][se:7218_anger]¡El año apenas acaba de empezar,@y ya estoy entusiasmada!
Iroha: El volante frente a la tienda de juguetes@era el anuncio de un evento.
Iroha: Ese evento de Año Nuevo era...
Iroha: ¿¡Hanetsuki!?
Tsuruno: ¡Yep!
Iroha: Um... ¿El hanetsuki es como el bádminton?
Tsuruno: ¡Así es! ¡Excepto que usa esas paletas de@madera de la vieja escuela, y no hay red!
Tsuruno: ¡Es el juego perfecto para Año Nuevo!@¡Intentémoslo!
Iroha: ¿Eh? pero...
Tsuruno: ¡Dice que cualquiera puede participar!@¡Será como un torneo!
Iroha: Tienes razón... ¡Intentémoslo entonces!
Sana: Yo, uhm, no soy buena en los deportes...
Sana: Además, solo las Chicas Mágicas pueden verme.
Tsuruno: [chara:100301:effect_emotion_sad_0][se:7224_fall_into]Oh, ceirto.
Debido al deseo de Sana,@la gente normal no puede verla.
Sana: Sí... ¡Así que solo miraré y aprenderé!
Tsuruno: Uh, lo siento por eso...
Sana: ¡No te preocupes, está bien!@¡Ustedes dos deberían intentarlo!
＜Y así, Iroha y Tsuruno decidieron@participar en el evento. Sin embargo...＞
Sana: ¡Tsuruno, ahora!
Tsuruno: Cargandooo...
Tsuruno: [flashEffect:flashWhite2]¡Hwaaaaagh!
Tsuruno: ... ¿Eh?@... ¿Fallé?
＜Tsuruno perdió la primera partida.＞
Iroha: ¡Mi turno!
Sana: ¡Justo ahí, Iroha!
Iroha: [flashEffect:flashWhite2][se:4061_landing_jump5]¡Hyah!
Iroha: ... ¡Gané!
＜¡Iroha derrotó a 5 oponentes!＞
Tsuruno: ¡Nada mal, Iroha!
Iroha: ¡Oh, no fue nada! En serio...
Iroha: ¡Fue todo gracias a los consejos de Sana!
Tsuruno: ¡Claro que sí! ¡Era como tener una compañera!
Tsuruno: Aunque no pude usarlo a mi favor...
Sana: Oh, solo me emocioné.@Realmente no sabía de qué estaba hablando.
Tsuruno: Podría ser que...
Tsuruno: Oiga, ¿me puede prestar@unas paletas para practicar?
Claro, puedes tomar cualquiera@de las que están ahí.
Tsuruno: ¡Muchísimas gracias!
Tsuruno: ¡Toma, Sana! ¡Juega conmigo!
Sana: [chara:100401:effect_emotion_surprise_0][se:7226_shock]¿Eh?! ¿Y-yo?
Tsuruno: ¡Por favor!@¡Necesito practicar!
Sana: Uh, está bien.@Lo intentaré...
＜Y así, Tsuruno y Sana empezaron a jugar.＞
＜El arma mágica de Sana es su escudo...＞
Sana: [flashEffect:flashWhite2][se:4141_jumping]¡Wah!
Sana: [flashEffect:flashWhite2][se:4141_jumping]¡Woa, ah!
Sana: [flashEffect:flashWhite2][se:4141_jumping]¡W-w-woa!
＜¡Quizás por eso fue capaz de bloquear@y contestar todas las veces!＞
Tsuruno: [chara:100301:effect_emotion_surprise_0][se:7226_shock]¡Rayos! ¡Nooo!
＜¡Al poco tiempo, su oponente falló,@y Sana ganó con su impecable defensa!＞
Iroha: ¡Wow, Sana!
Tsuruno: ¡Eres buena!
Tsuruno: Pensé que podrías serlo,@¡pero en serio lo eres!
Sana: ¿Qué? ¡N-no lo creo!
Tsuruno: Si hubieras podido participar,@seguro que habrías ganado muchas partidas.
Sana: ¿Y-yo?
Tsuruno: ¡Si tan solo pudieras!@¡Habría sido genial!
¡Oye, eres realmente buena!
Tsuruno: ¡Lo sé! Sana es increíb—
¡No puedo creer que pudieras jugar así tú sola!
Tsuruno: ¿Sola?
Iroha: ¡Tsuruno...!
Tsuruno: ¡Oh, cierto! [chara:100301:lipSynch_0][wait:1.0][chara:100301:cheek_0][chara:100301:face_mtn_ex_030.exp.json][chara:100301:lipSynch_1]Ummm...
Tsuruno: Jaja, ¡fue pura suerte!
¡Apuesto a que podrías ganar el torneo!
Tsuruno: ¿Torneo?
