Momoko: ¡Acerquémonos y ataquemos juntas!
[se:7213_witch_noise_01]｜¡¡0｡*ｰ(●(工)●)ｰ*｡0!!｜
Momoko: Está en un buen lugar ahora.
Momoko: *Inhala*
Momoko: [flashEffect:flashWhite2][bgEffect:shakeLarge][se:6101_hit_slash_01_vh]¡¡¡Vete al carajo!!!
[flashEffect:flashRed2][se:6100_hit_slash_01_s][bgEffect:shakeLarge]｜?!,゜.:(＞(工)○):.゜,¡¿?!｜
Momoko: *Jadeo* *Jadeo*
Momoko: ¡¿Ustedes tienen suficiente gente?!
Mifuyu: Sí, las Plumas están aquí para ayudar. ¡Deberíamos@estar bien!
Momoko: ¡Okay!
Mifuyu: ¡Todas, tenemos que destruir a esta Uwasa y sacar a@las Magius de su escondite!
Feathers: ¡Sí!
Homura: (Si derrotamos a esta Uwasa, el Leberinto@desaparecerá.)
Homura: (Lo que significa que Magius tendrán que salir@y estaré de vuelta en Kamihama.)
Homura: (Cuando eso pase, Walpurgisnacht estará@ahí, justo en frente de mí.)
Madoka: ¡Homura!
Homura: ¡S-sí!
Madoka: ¡Derrotémosla y eyudemos a Iroha!
Homura: ¡Sí, vamos a destruirla pase lo que pase!
Sayaka: Tus bombas siempre me toman por sorpresa,@Homura.
Sayaka: ¡Pero esta vez, explota cosas@todo lo que quieras!
Homura: ¡Sí!
Homura: (Esta línea temporal es muy diferente.)
Homura: (Debería estar en Mitakihara, pero estoy aquí,@en Kamihama...)
???: Para cambiar tu destino, ven a Kamihama.@Aquí, las Chicas Mágicas puden ser salvadas.
Homura: (Si detenemos a las Magius, podríamos perder@la oportunidad de que eso suceda.)
Homura: (Pero talvez haya más oportunidades@en el futuro.)
Homura: (No sé que está bien o mal en esta@línea temporal...)
Homura: (Pero hay una cosa de la que estoy segura.)
Homura: (Tengo que derrotar a esta Uwasa...)
Homura: (Y proteger a Madoka de Walpurgisnacht.)
Mifuyu: ¡Ataquemos todas juntas!
Homura: ¡Ah!
Homura: ¡Yo también!
Homura: [flashEffect:flashWhite2][se:4053_landing_throw2]¡Le descargaré todo mi@arsenal! ¡No fallaré!
Homura: (¡Si combinamos nuestros poderes...!)
Momoko: [flashEffect:flashWhite2][bgEffect:shakeLarge][se:4052_landing_dive_sv][se:6101_hit_slash_01_vh][se:4004_equip_arms_magic_vh]¡Rrraaaaaagh!
Mifuyu: [flashEffect:flashWhite2][bgEffect:shakeLarge][surround:1006A02][surround:1018A05][surround:1019A04]¡Jaaaaaah!
Madoka: [flashEffect:flashWhite2][bgEffect:shakeLarge][se:5009_shooting_05_h][surround:2004A09]¡Por favor funciona!
｜…○ﾟ｡(つ(工)Ｔ)。ﾟ○…｜
Homura: (¿F-fucionó?)
[se:7213_witch_noise_01][flashEffect:flashWhite2][bgEffect:shakeLarge]｜¡¡;*(#◎(工)◎#)*;!!｜
Madoka: ¿Qué?
Homura: ¡¿...?!
Homura: ¡Madoka!
???: [flashEffect:flashWhite2][bgEffect:shakeLarge]¡Jaaaaaah!
Mami: ¡¿Estás bien, Madoka?!
Madoka: Mami...
Mami: ¡Kyoko!
Mami: ¡Yo desato los listones, tú aleja a la@Uwasa!
Kyoko: ¡Muy bien!
Kyoko: [flashEffect:flashWhite2][bgEffect:shakeLarge][jingle:2006M05]¡Kugatachi!
[bgEffect:shakeLarge][flashEffect:flashRed2][jingle:2006M10]｜¡¿?!ｰﾟ･(＞(工)＜)･ﾟｰ¡¿?!｜
Homura: Feew... Que alivio.
Madoka: Gracias... Mami, Kyoko.
Mami: Estoy tan aliviada de que estén bien.
Mami: Ow...
Sayaka: Mami, ¡¿te lastimaste?!
Mami: solo es un raspon. Estaré bien.
