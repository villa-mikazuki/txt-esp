[se:7214_witch_noise_02]｜rawr-rawr(◎ｘ◎)rawr-rawr｜
Kyoko: ¡¿Cuántos bloqueos pusieron esas@Plumas en nuestro camino?!
Kyoko: Nos va a tomar demasiado tiempo llegar@a la superficie a este pasa.
Sana: Pero si descubrimos en donde están las@Magius...
Kyoko: ¿Merodeando por aquí?
Kyoko: Se realista. Aún si las encontramos, solo somo@dos.
Kyoko: De ninguna manera voy a arriesgar mi propia vida@por alguien. No soy ninguna palomita.
Sana: ¡Entonces pelearé hasta llegar ahí yo sola!
Kyoko: Heh.
Kyoko: Me gusta tu actitud.Tienes agallas.
Kyoko: Aún así este es su territorio. No esperes que@simplemente se rindan y te dejen ganar.
[flashEffect:flashWhite2][bgEffect:shakeLarge][se:7214_witch_noise_02]｜¡rawr(#◎ｘ◎#)rawr!｜
Sana: [flashEffect:flashWhite2][surround:1004A04][bgEffect:shakeLarge]¡Kyoko! ¡Ahora!
Kyoko: ¡Bien hecho, Sana!
Kyoko: [flashEffect:flashWhite2][surround:2006A04][bgEffect:shakeLarge]¡Las voy a noquear a todas!
[flashEffect:flashRed2][surround:2006A03][bgEffect:shakeLarge][name:sprite_0:effect_shake]｜¡¡¡ra(ＴｘＴ)awrr!!!｜
Kyoko: Muy bien, esa fue la última.
Sana: Ahora podemos subir a la superficie.
Sana: ...
Kyoko: Bien, las perdimos de vista...
Kyoko: ¿De verdad crees que tenemos posibilidad de@encontrarlas ahora?
Sana: Espera.
Kyoko: ¿Ehh? ¿Que pasa?
Sana: En el piso, por ahí...
Sana: Es el listón de Iroha.
Kyoko: ¿Su listón?
Kyoko: ¡Ja ja ja! Bueno, ¿qué te parece eso? Hizo@bien.
Kyoko:¡Nos dejó una pequeña pista con la que seguirla!
Sana: ¡Entonces Iroha está en algún lugar en frente de@aquí!
Kyoko: ¡Eso parece. Hay que movernos, Sana!
Sana: ¿Vas a venir conmigo?
Kyoko: Había planeado huir, pero parece que todavía@no se pone tan mal por aquí.
Kyoko: En cuanto se empieze a sentir peligroso, pensaré@en una ruta de escape.
Sana: Gracias.
Kyoko: Tenemos los mismos enemigos, así que.
Pluma Blanca: ¡Son Sana Futaba y la chica nueva!
Kyoko: Mierda, ya nos encontraron.
Pluma Blanca: Dejende buscar a Iroha Tamaki.
Pluma Blanca: Acabo de recibir noticias de que nuestras alidas@la han entregado.
Kyoko: ¡Sí sí, dinos algo que no sepamos!
