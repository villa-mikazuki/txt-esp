Madoka: ¡Fuimos al Santuario de Mizuna@para saber nuestras fortunas!
Madoka: ¿Por qué estas fortunas en particular?@Bueno...
Miko: ¡Que pase el siguiente de la fila, por favor!
Madoka: ¡Sí!
Madoka: Um, ¡me gustaría una fortuna por favor!
Miko: Muy bien, saca un palito de aquí y@muéstrame qué número tiene.
Madoka: ¡Okey...!
Madoka: ...
Madoka: ¡Este!
Miko: Número 12. Aquí tienes.
Miko: ¿Quién sigue?
Homura: A... ¡A mí también me gustaría una fortuna!
Miko: ¡Por favor, saca un palito!
Homura: ...
Homura: ¡Okey...! ¡Este de aquí!
Miko: Número 53. Aquí tienes.
Madoka: ¡Bien, tenemos nuestras fortunas!
Homura: ¡Estoy un poco nerviosa!
Madoka: Sí...
Madoka: Después de todo,@¡estas son las "fortunas definitivas"!
Madoka: ¡Lo escuché en la televisión!@¡El Santuario de Mizuna estaba@revelando fortunas definitivas,@mayores que cualquier otra!
Madoka: ¡Y hoy es el primer día en@el que están disponibles! ¡Por eso hemos venido a@obtener nuestras fortunas!
Madoka: Pero no había información@sobre qué tipo de fortunas eran...
Homura: ¿Qué es exactamente lo que@las hace "definitivas"?
Madoka: Bueno, he oído que estas están a otro nivel@en comparación con la fortuna promedio.
Madoka: Alguien del santuario estaba hablando al respecto@en la televisión.
Homura: Hmm... Una fortuna que está a otro nivel...
Madoka: ¡Abrámoslas a la de 3!
Homura: ¡Okey!
Madoka: ¡Aquí vamos! 1... 2... ¡3!
Homura: [chara:200350:effect_emotion_surprise_0][se:7226_shock]¡...!
Homura: ¡Woa!
Madoka: [chara:210000:effect_emotion_surprise_0][se:7226_shock]¿¡Esto es...!?
