Felicia: ¡Bruja, Bruja, Bruja, BRUJA!
Felicia: ¡Voy a hacer BA-BAM, WHAM, y VOLARLE@los sesos a esa cosa!
Alina: ¡¿...?!
Alina: ¡Esa mocosa!
Felicia: [flashEffect:flashWhite2][surround:1008A02][bgEffect:shakeSmall]¡Ngyah!
Alina: ¡¿Pero qué es lo que estás haciendo?!
Alina: ¡¿Osas dañar mi obra de arte?! ¡Worse!
Felicia: ¿Obra? ¿Qué obra? ¡Esto es una Bruja, atolondrada!
Alina: ¡Incorrecto!
Alina: ¡Esta Bruja es un aspecto brillante de mi@preciada obra maestra!
Alina: ¡De solo pensar que le pondrías una mano@encima me da risa!
Felicia: ¿La Bruja es tu obra maestra?
Felicia: ¡¿Uh, qué carajo?! ¡¿Estás mal de la cabeza@o algo así?!
Alina: ¡Tú eres la que está "mal de la cabeza" si no puedes@apreciar la belleza de una Bruja!
Alina: ...[wait:0.8][chara:100800:cheek_0][chara:100800:face_mtn_ex_010.exp.json][chara:100800:lipSynch_1][chara:100800:voiceFull_fullvoice/section_101506/vo_full_101506-4-15]Jeh.
Felicia: ¡¿Q-qué?!
Alina: Oh, solo estaba pensando que te puedo dar@de comer a mi Bruja también.
Felicia: ¡Huh!
Felicia: ¡S-solo intenta ponerme en ese Laberinto!@¡Le voy a partir la cara!
Felicia: ¡¿Te parece bien eso?! ¡¿No era por eso que@intentabas detenerme?!
Alina: Nunca te perdonaría por entrar sin permiso en su@Laberinto.
Alina: Pero es completamente diferente si te@permito entrar.
Alina: ¿Quieres ir a donde está la Bruja, sí?
Alina: Well, yo te llevaré personalmente.
Felicia: ¡¿Que carajo?!
Alina: Te voy a capturar en mi propio mini@Laberinto.
Sana: [flashEffect:flashWhite2][bgEffect:shakeSmall][surround:1004A01]¡Y-yaaah!
Pluma Negra: Ugh...
Sana: *Jadeo* *Susto*...
Iroha: ¿Te encuentras bien?
Sana: Sí... estoy bien...
Iroha: ...[wait:0.8][chara:100100:lipSynch_1][chara:100100:voiceFull_fullvoice/section_101506/vo_full_101506-4-32]Oye, Sana.
Iroha: ¿Por qué las Alas de Magius protegen@Brujas?
Sana: B-bueno, lo siento pero, eh...
Sana: Yo solo estaba cautiva ahí, así que no sé@realmente...
Sana: Ai intentar entenderlo por@mucho tiempo.
Sana: Las Brujas simplemente se seguían haciendo@más fuertes, y yo nunca supe qué intentaban@hacer.
Sana: Ai dijo que era peligroso...
Sana: ¡¿...?!
Sana: [flashEffect:flashWhite2][bgEffect:shakeSmall][surround:1004A01]¡Iroha, cuidado!
Iroha: ¡Ah!
Iroha: Gracias, Sana.
Sana: Claro...
Tsukasa: Proteger Brujas...
Tsukuyo: Eso también es necesario para nuestra liberación.
Tsukasa: Brujas y [textBlue:Rumores]... Ambos son necesarios@pare el primer paso hacía ello.
Tsukuyo: Cierto.
Tsukuyo: Emociones, impurezas, maldiciones...@Todos son vitales en la búsqueda de la libertad.
Tsukasa: Así es.
Iroha: Simplemente no puedo entender lo que@quieren decir.
Iroha: Ustedes protegen [textBlue:Rumores] que lastiman a la gente...
Iroha: Ustedes protegen Brujas que MATAN a la gente...
Iroha: ¿Cómo eso puede llevar a la liberación de las@Chicas Mágicas?
Iroha: No sé qué es esta maldicíón de la que@están hablando...
Iroha: Pero si tienen que hacer sufrir a otros solo para@deshacerse de ella...
Iroha: ¿Cómo pueden estar felices con eso? ¿No les@importa en lo absoluto?
Tsukuyo: Si esto puede salvar el futuro de cientos, o@hasta miles de Chicas Mágicas...
Tsukuyo: ...todo lo que pase ahora no es más que@un pequeño sacrificio.
Tsukasa: Cierto.
