Momoko: Ya veo.
Momoko: Revisaste el Archivo de Rumores de@Yachiyo-san...
Momoko: ... y sentiste que reconocías@uno de los [textBlue:Rumores] en él.
Iroha: Sí.
Iroha: Pero eso no puede ser.
Rena: Solo olvídalo por ahora.
Rena: Secuestraron a todas tus amigas.@Ese ya es un problema bastante grande.
Rena: ¿Realmente tienes tiempo para preocuparte por@un estúpido [textBlue:Rumor]?
Momoko: ¡Oye, Rena!
Momoko: Iroha-chan estuvo quemándose las pestañas@tratando de encontrarlas.
Momoko: Este [textBlue:Rumor] en el que está@pensando es algo importante para ella también.
Momoko: Deja de atacarla, ¿está bien?
Rena: No estaba atacando a nadie.
Rena: Solo, ya sabes...
Rena: Si sigue mordiendo más de lo que puede@masticar...
Rena: El estrés podría dejarla calva.
Iroha: ¡¿Yo?! ¿Sin pelo?!
Rena: Claro que tú. ¿De quién más podría estar@hablando?
Iroha: Entonces seré calva...
Iroha: ¡Pft...pft ja ja ja ja!
Rena: ¡¿Q-Qué diablos?! ¡No dije@nada raro!
Iroha: Gracias por tu preocupación, Rena-chan.
Rena: C-como sea. No estaba preocupada ni nada.
Rena: La tenemos difícil nosotras también, así que@entiendo un poco cómo te sientes, eso es todo.
Iroha: ¿Te refieres a Kaede-chan?
Iroha: ¿Todavía no volvió a la escuela?
Rena: Sip... Estuvo escondida en su cuarto@todo este tiempo.
Momoko: Pensé que la había calmado lo suficiente,@pero...
Momoko: Creo que no pudo aguantar estar sola.
Momoko: El destino que eligió...
Momoko: Bueno, seguiré intentando animarla.@No me rendiré tan fácilmente.
Iroha: ¡Veré qué puedo hacer yo también!
Momoko: Sería horrible si no podemos traer a Tsuruno@de vuelta pronto, ahora que lo pienso...
Iroha: ¿Lo será? ¿Pasó algo?
Rena: En su escuela están desesperados desde@que ella no va.
Momoko: Desde que empezó a ir allá no llegó@tarde ni faltó ni una sola vez.
Momoko: Pasó de tener asistencia perfecta a@desaparecer por completo de golpe.
Momoko: Los profesores están en pánico, es@como MUY importante.
Iroha: Oh...
Momoko: Son 12 años de asistencia perfecta echados@a perder. Tsuruno no lo va a poder creer...
Rena: Sí, ella parece del tipo de persona que@apuntaría a tener asistencia perfecta...
Iroha: Sí.
Momoko: (No estoy segura de que hacer ahora...)
Momoko: (Sigo preocupada por Kaede.)
Momoko: (Para ser sincera, el grupo de Iroha-chan@tiene problemas mucho más serios...)
Momoko: ...
Momoko: (Supongo que los [textBlue:Rumores] y todo eso son@la especialidad de Yachiyo-san de todos modos...)
¡Momoko!
Momoko: (Quizás debería poner toda mi energía@solamente en juntar Grief Seeds...)
Rena: ¿Momoko? Oye, ¿estás escuchando?
Momoko: Ah, si, perdón, ¿qué estabas diciendo?
Rena: Estábamos hablando del nuevo parque de@diversiones que va a abrir dentro de poco.
Momoko: ¿Parque de diversiones?
Momoko: ¿En Kamihama? No tenía idea de que estaban@construyendo uno.
Rena: Recién me enteré yo también.
Rena: No puedo esperar a que esté terminado.
Momoko: No hubo ninguna noticia sobre un proyecto@grande de construcción, así que....
Momoko: Probablemente es solo un rumor.
Rena: Sí, probablemente.
Rena: ¿Pero no sería genial si fuera real?
Rena: Podríamos ir todas juntas.
Momoko: Sí, eso estaría bueno.
Momoko: Si resulta ser verdad, yo las invito a@todas.
Rena: ¡¿Lo prometes?!
Momoko: ¡Créelo!
