Iroha: ¡Hola, Mitama!
Mitama: ¡Pero si es Iroha! Bienvenida.
Iroha: Toma, son algunas Grief Seeds...@Esto es todo lo que tengo, así que...
Iroha: ¿Podría pedirte que me Fortalezcas@tanto como valgan?
Mitama: Será un placer.
Mitama: ¿Vamos a la sala de coordinación?
Iroha: Sí.
Mitama: Ahora, por favor, quítate la ropa...
Iroha: ¡D-De ninguna manera, no voy a caer en@esa broma por segunda vez...!
Mitama: ¡Oh, vaya!@Bueno, al menos lo intenté.
Iroha: Ugh...
Mitama: *Risilla* Perdón, perdón.@[chara:101700:cheek_0:face_mtn_ex_010.exp.json]@Muy bien, volviendo al tema...
Mitama: ¿Empiezo?
Iroha: ¡Sí...!
Iroha: (Creo que nunca me acostumbraré a esta@sensación...)
Iroha: (Como un dolor profundo dentro de mí...)
Iroha: (Y todos estos destellos de recuerdos@dentro de mi cabeza...)
Iroha: ¡N... nghah...!
Mitama: Filete de hamburguesa con salsa demi-glace.
Iroha: ¿Eh?
Mitama: ¿Con queso fundido por encima?
Iroha: Err...
Mitama: Un filete de hamburguesa con arroz gratinado...@Servido bien caliente ♪
Iroha: Hamb—
Iroha: Um...@¡Espera un segundo...!
Mitama: Oh, vaya.
Mitama: ¿Viste algo inusual?
Iroha: Bueno... no exactamente inusual... es solo...
Iroha: Comida como... filete de hamburguesa...@Gratinado...
¡Oh!
Iroha: ¿Eh?
Mitama: Bueno, bueno.
Momoko: Iroha.@[se:7107_arrivalone]¡Así que estás aquí!
Iroha: ¡Momoko!
Mitama: Ahhhh, oh cielos... No deberías entrar así a@la sala de coordinación.
Momoko: Siempre me haces hacer guardia en@este lugar...
Momoko: Soy casi una empleada, ¿no?
Mitama: Bueno, [chara:101700:effect_emotion_joy_0][se:7222_happy]si estás tan ansiosa por ser una@empleada, tal vez debería darte más trabajo.
Momoko: En tus sueños.
Momoko: ¿Qué tal? ¿Cómo te fue?@¿Te sientes más fuerte?
Iroha: Umm...@No estoy segura...
Mitama: Aún no hemos terminado.@Hubo un problema con la coordinación.
Iroha: ¿Problema...?
Momoko: Oh, ya entendí.@Así que ... No has almorzado todavía, ¿eh?
Mitama: ¡Correcto!
Iroha: ¿Qu...?@¿Eh? Perdón, ¿¡cómo dices...!?
Momoko: ¡Ja ja, cálmate!@¡No es como si fuera un desastre o algo así!
Momoko: Es solo que ella pierde la concentración@cuando su estómago empieza a rugir.
Iroha: Así que... ¿Por eso había ese filete de@hamburguesa con arroz gratinado...?
Mitama: *Risilla* Precisamente.
Mitama: Hoy es un buen día para comerse un buen@filete de hamburguesa con algo de gratinado.
Momoko: Cielos... Bueno, no se pueden hacer ventas@con el estómago vacío, eso es seguro.
Momoko: Aunque es un buen momento.@Yo tampoco he almorzado aún.
Momoko: ¿Por qué no tomamos un descanso@para comer?
Mitama: *Risilla* Suena bien.@[chara:101700:cheek_0:face_mtn_ex_010.exp.json]¿Ya comiste, Iroha?
Iroha: ¿Eh?@Oh, umm... ¡No...!
Mitama: ¿Te gustaría?
Iroha: ¿Eh?@Uh... ¿Um...?
