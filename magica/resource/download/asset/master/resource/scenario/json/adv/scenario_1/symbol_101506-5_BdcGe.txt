Iroha: Tengo el presentimiento de que nunca nos@entenderemos, no importa lo que diga...
Tsukasa: Bueno, no se supone que supieras esto@todavía.
Iroha: ¿Qué significa eso?
Tsukuyo: No conoces toda la verdad.
Tsukuyo: Por eso es que no entiendes.
Iroha: Si piensan que la verdad me convencerá,@entonces quiero escucharla.
Tsukasa: No podemos hacer eso.
Tsukasa: Tienes que esperar hasta el momento correcto.@O hasta que conozcas... a esa persona.
Tsukuyo: Cierto.
