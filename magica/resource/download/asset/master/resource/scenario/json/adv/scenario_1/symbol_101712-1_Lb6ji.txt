―Distrito de Daito―
Kanagi: Sí, este debería ser el camino correcto.
Kanagi: Hazle saber a tus amigas.
Iroha: Okey.
Iroha: Voy a llamar a Sana-chan y a Felicia-chan@entonces.
Yachiyo: Sí, hazlo por favor.
Yachiyo: Dada la circunstancia, me alegro de que@nos estés ayudando, Kanagi.
Yachiyo: No voy a esa Rueda de la Fortuna desde que@era una niña.
Kanagi: ¿Eso es verdad?
Kanagi: Debo decir, esto sí se siente algo extraño.
Yachiyo: ¿Sientes la magia de alguien?
Kanagi: No, no es eso.
Kanagi: Estuviste en una situación bastante precaria@cuando te estaba probando.
Kanagi: Pero ahora están siendo amables conmigo.
Yachiyo: O sea, sí que me sorprendió...
Momoko: Pero eso es, como, lo que esperamos de ti,@Kanagi-san.
Kanagi: Hm, ya veo. Simplemente así es como me ven,@supongo.
Kanagi: Dejaré de preocuparme por ello, entonces.
Momoko: Bueno, podrías preocuparte un poco...
Kanagi: Es un balance delicado.
Iroha: ¿Dónde estás ahora, Sana-chan?
Sana: Um... Recién salimos del distrito de Chuo...
Iroha: Te enviaré a donde ir en el mapa. ¿Podrías@revisarlo cuando lo recibas?
Sana: ¡Sí, esta bien!
Sana: Y, um, Iroha-san...
Iroha: ¿Qué pasa?
Sana: Parece que la gente se está empezando a mover.
Iroha: ¿Empezando a mover?
Sana: La gente que escuchó sobre el [textBlue:Rumor] se@está dirigiendo al parque de diversiones.
Iroha: ¡¿Ehh, ya?!
Sana: Sí.
Sana: Creo que tenemos que apurarnos.
Iroha: ¡Entendido! ¡Le diré a las demás!
Iroha: ¡La gente que escuchó el [textBlue:Rumor] ya se está@dirigiendo al parque!
Momoko: ¡¿Qué?! ¡¿Ya?!
Momoko: Espera.
¡Cuando el parque de diversiones abra,@deberíamos ser las primeras en la línea!
Momoko: ¡¿Maldición, iban a esperar ahí toda la noche?!@Tenemos menos tiempo del que creíamos.
Yachiyo: ¡Vamos, rápido!
Iroha: ¡Sí!
