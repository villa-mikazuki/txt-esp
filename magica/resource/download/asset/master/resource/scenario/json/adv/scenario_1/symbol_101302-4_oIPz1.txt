¿Un rumor sobre un santuario?@Nunca lo he oído.
Iroha: Ah, está bien…
¿¡Eh, cómo has dicho!?
¡Si es alguna especie de punto de emparejamiento,@tienes que contármelo todo!
Iroha: ¡¿Eh?![chara:100102:effect_emotion_surprise_0][se:7226_shock]
En realidad, creo haber oído algo sobre eso...
Iroha: ¡¿De verdad?!
Por desgracia, no me interesó mucho,@así que no recuerdo ningún detalle.
Iroha: Aww...
Iroha: Hmmm...
Iroha: (Nadie ha oído hablar al respecto...)
Iroha: (¿Tal vez sería más rápido simplemente@preguntarle a otra Chica Mágica...?)
Iroha: (Tal vez haya otras chicas como la Yachiyo-san@que estén interesadas en los rumores...)
Iroha: Pero, ¿cómo puedo encontrar una?
Iroha: (Además, no tengo nada que ofrecer a cambio...)
Iroha: ...
Iroha: Hmmm…
Iroha: ¡¿Podría alguna Chica Mágica@decirme algo sobre un rumor?!
Iroha: ...
Iroha: Si tan solo funcionara así.
¡SEGURO!
Iroha: [chara:100102:effect_emotion_surprise_0][se:7226_shock]¿Eh...?
???: ¡Cuando se trata de [textBlue:Rumores]...!
Tsuruno: ¡Yo soy tu chica! [textRed:Tsuruno Yui],@¡La Chica Mágica Más Poderosa!
Iroha: [chara:100102:effect_emotion_surprise_0][se:7226_shock]U-umm... ¡¿Quién eres tú?!
Tsuruno: Entonces, ¿qué quieres saber?@¡Te diré LO QUE SEA!
Iroha: ¿Qué? Um... ¿En serio?
Tsuruno: Los [textBlue:Rumores] son peligrosos.@¡No los busques por diversión!
Tsuruno: Osea, ¡ni siquiera pienses en hacer@algo contrario a un [textBlue:Rumor]!
Iroha: Um, eso ya lo sé...
Tsuruno: ¡Tampoco deberías intentar@deshacerte de un [textBlue:Rumor]!
Tsuruno: Porque si lo haces, aparecerán algunos@monstruos realmente extraños.
Tsuruno: ¡Y si aparecen, será un gran problema!@¡Un problema REALMENTE GRANDE!
Iroha: U-umm, ¡Como dije!@¡Ya sé todo eso!
Tsuruno: Entonces, qué hay de...
Iroha: (N-no se detiene...)
