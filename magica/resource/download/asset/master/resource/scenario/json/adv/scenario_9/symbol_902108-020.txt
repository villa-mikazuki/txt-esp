Mabayu: Oh!
Amy: ......
Mabayu: ¡Amy está delante de nuestra tienda...!
Mabayu: Jijiji... ¡No olvidé prepararme@para momentos como éste!
Mabayu: Amy, tengo un poco de@comida para gatos por aquí.
Mabayu: Es deliciosa comida para gatos con sabor a atún...
Amy: ¿Miau?
Mabayu: Vamos, ven, ven,@está delicioso. Ven aquí@y come despacio...
*Le da la espalda*
Mabayu: ¿Qué? ¿Amy-chan?
Mabayu: Aquí tienes comida@súper rica... ¿Eh?
*Se va*
Mabayu: Oh…
Mabayu: Se escapó...
Mabayu: ¿P-Por qué...?
Mabayu: ¡Incluso tenía comida para gatos@preparada!
Sakie: ¿Acaso era con sabor a atún?
Mabayu: Ah, Sakie.
Sakie: Amy tiene gustos muy refinados,@así que, si le damos algo,@¡tiene que tener sabor a pollo!
Mabayu: [chara:200902:effect_emotion_surprise_0][se:7226_shock]¿¡Qué, en serio...!?
Sakie: Jejeje.@No subestimes a la chef pastelera Sakie@cuando se trata de comida.
Mabayu: Ahh...
Sakie: De todos modos, Amy@realmente se ha recuperado.
Sakie: Es una recuperación milagrosa.@¡Mhm, mhm!
Mabayu: Un milagro, eh...
Sakie: Ahora, Mabayu.
Sakie: Me gustaría pedirte que me ayudes@en la tienda hoy también.
Mabayu: Oh, está bien.
Mabayu: ¡Muchas gracias!
Mabayu: Uf... Parece que hoy podré@tomármelo con calma.
Sakie: Ah, sí, me enteré de que hubo un accidente@durante la visita a la fábrica de@automóviles.
Mabayu: ¿Eh..? Ahh, ¿te has enterado?
Sakie: La chimenea se derrumbó@debido a un mal mantenimiento...@¿Estuviste bien, Mabayu?
Mabayu: Sí, estuve lejos.
Mabayu: (Bueno, eso es mentira.)
Mabayu: (Pero al final, estuve a salvo,@no quiero preocupar a Sakie).
Sakie: Estoy segura de que hubo un accidente@similar el año pasado.
Sakie: Ya que los estudiantes van de excursión,@me gustaría que la fábrica tuviera@más cuidado.
Mabayu: Tienes razón.
Mabayu: ... Oh, ¿eh? ¿Sakie?
Mabayu: ¿Terminaste de hacer todos los@pasteles que te encargaron?
Sakie: Oh... ¡Lo olvidé! *bleh*
Mabayu: Siempre eres así, Sakie...
Sakie: Mabayu,@¿puedes leer los detalles del pedido?
Sakie: Creo que era un pastel@para celebrar una alta del hospital.
Mabayu: Bien... Uh...
Mabayu: ¿Gyou... bi?@¿Akatsuki...?
Sakie: ¿Eh? ¿Qué pasa?
Mabayu: No sé cómo leer los kanjis@de este apellido...
Mabayu: "暁" significa mañana,@"美" significa hermoso...
Mabayu: Y su nombre en hiragana es "Homura".@En cuanto al mensaje...
"Felicidades por tu alta del hospital,@Homura Akemi."
