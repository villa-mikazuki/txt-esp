Chisato: Parece que la Cafetería LycoReco fue cambiada@con un lugar llamado Villa Mikazuki...@Entonces las chicas que vivían ahí nos llevaron@a un lugar llamado... ¿La tienda de la Coordinadora?@Algo así, de todos modos.
Chisato: Hey hey, Takina.@Todas se quedaron calladas de la nada.
Takina: ...Sí. Me pregunto si fue buena idea@seguirlas hasta aquí.
Takina: Podrían ser las mentes maestras detrás de todo@esto...
Takina: Tal vez nos trajeron a su escondite@para hacernos lo que quieran.
Chisato: Bueno, esa chica nos salvó la vida, ¿no?
Takina: Eso... Tienes razón, si lo hizo.@Pero sigue siendo muy pronto para confiar en@ellas.
Chisato: Tal vez este sea algún tipo de lugar de@reunión de superhéroes...
Kanagi: ... Mis disculpas, pero ¿puedo pedirles que@se presenten?
Chisato: Sí, me llamo Chisato Nishikigi, ¡la chica insignia@de la Cafetería LycoReco!
Takina: ... Yo me llamo Takina Inoue, una compañera@empleada de LycoReco.
Kanagi: ... ¿Es cierto o no?@Sus mentes me lo dirán todo.
Chisato: ... ¿Ehh?
Kanagi: ...[wait:1.2][chara:101600:cheek_0][chara:101600:face_mtn_ex_000.exp3.json][chara:101600:motion_200][chara:101600:lipSynch_1]Ya veo. Parece ser cierto.
Kanagi: ...Pero, ¿Qué es "Lycoris"...?
Takina: ¡¿Qué estás haciendo...?!
Kanagi: Les he leído la mente.
Kanagi: No soy quien confía fácilmente de la gente con@armas, después de todo.
Takina: ¡Ggh...!
[flashEffect:flashWhite2]*¡Swish!*
Voz de Kanagi: ¿Qu-Qué es esto...?! ¡¿Un cable?!
Chisato: ¡Takina, que carajos!@¡¿Por qué la inmovilizaste de repente?!
Takina: ¡Saben sobre las Lycoris!@¡No podemos dejar las cosas así—!
Yachiyo: Ya fue suficiente.
Takina: ¿Huh...? ¿Cuándo es que...?
Chisato: Wow... esos si son buenos movimientos.@¿Crees que deberíamos rendirnos?
Takina: Chisato... Estas personas son muy hábiles.
Yachiyo: Ustedes dos no son las únicas que se han dado@cuenta.
Yachiyo: ... Quiero decir, ¿qué tipo de persona tendría@un dispositivo de inmovilización como ese?
...
Chisato: ¡E-espera un segundo!
Yachiyo: ¿Estás intentando algo aún ahora?
Chisato: ¡No no no!@¡No queremos hacerles daño!
Chisato: Takina simplemente es demasiado seria... o más@bien, demasiado rápida en actuar...
Takina: Tengo que serlo porque tú no lo eres.
Chisato: Takina, ¡por favor cállate solo esta vez@antes de que empeores más las cosas!
Chisato: ... ¡Simplemente entramos aquí de alguna manera!
Chisato: ¡Lycoris no es una organización malvada!
Chisato: ¡Probablemente!
Mitama: ... ¿Qué piensas, Kanagi?
Kanagi: ... Así es, sus corazones parecen decir la verdad.
Yachiyo: ... Entonces empecemos desde el principio.@¿Puedes repasar tu historia otra vez?
Yachiyo: Cuéntanos sobre Lycoris.
Chisato: ...
Chisato: Ustedes pueden ser superheroínas, pero esto no@es algo de lo que pueda hablar tan fácilmente.
Chisato: Ustedes son quienes se volvieron hostiles primero@con su lectura de mentes y poderes extraños.
Chisato: Así que, antes que nada, necesito saber@quiénes son ustedes.
Chisato: Como esos poderes que me mencioné antes...@Ustedes tienen algún secreto grande...@¿me equivoco?
Chisato: Creo que es mejor que hablemos de igual a igual,@¿no crees?
Yachiyo: ... Actuamos por precaución, pero las cosas@ciertamente están desequilibradas.
Yachiyo: Muy bien. Te contaré nuestro secreto en turnos.
Chisato: Entonces, ¿ustedes son superheroínas con su@sociedad secreta... ¿algo así?
Yachiyo: No del todo.
Yachiyo: Somos Chicas Mágicas.
Yachiyo: Tenemos el deber de luchar contra las Brujas a@cambio de que se nos conceda un solo deseo.
Kanagi: En cuanto a eso, leer la mente@es mi poder característico.
Chisato: ¡Chicas Mágicas...!
Takina: Brujas...@Entonces ese monstruo que nos encontramos antes...
Felicia: ... Bueno, sí, básicamente.
Iroha: A veces las Brujas son la causa de crímenes@y accidentes.
Iroha: Derrotamos a las Brujas para evitar que@eso suceda.
Ui: Pero no queremos que todos se enteren de esto.
Chisato: Entonces eso hace que seamos algo parecidas.
Felicia: Muy bien, ya dijimos todo.@¡Ahora es su turno!
Chisato: Oh... ¿De verdad lo tenemos que decir?
Chisato: Estoy bastante segura de que correrían peligro@si se enteraran...
Kanagi: Si prefieren no hablar, puedo leer sus mentes@en su lugar.
Chisato: [chara:406101:effect_emotion_surprise_0][se:7226_shock]¡No no no! ¡Eso no!@¡Es vergonzoso!
Chisato: Mmmmmm...@Bueno... tienes razón.
Chisato: Ustedes nos dijeron su secreto... y tú nos salvaste@de esa Bruja.
Takina: ... ¿Les vas a decir?
Chisato: Mmm... ¡Pues ya qué!
Chisato: Muy bien, ¡nosotras somos Lycoris!
Felicia: ¿Y qué es Lycoris?
Takina: Somo agentes de un grupo independiente@de seguridad conocido como DA.
Takina: Es una organización secreta responsable@de mantener el orden y el orden público.
Takina: Nosotras prevenimos delitos e incidentes antes@de que ocurran,
Takina: eliminando en secreto a criminales y terroristas.
Chisato: ... ¿De verdad tenías que contarles lo de DA?
Takina: [chara:406201:effect_emotion_surprise_0][se:7226_shock]¿Eh...? ¿No acabas de decir que deberíamos?
Chisato: No, digo... podrías haber mentido un poco@o algo...
Takina: ¡Entonces debiste haber dicho algo antes!
Chisato: Ah bueno...@Pues sí, ¡ese es el resumen!
Chisato: Sin que nadie lo sepa, ¡eliminamos a aquellos@que perturban la sociedad!
Chisato: Asegurando que todos crean que viven en un@ciudad segura y pacífica...
Chisato: ¡Esa es la misión de las agentes de Lycoris!
Iroha: Agentes de una organización oficial secreta@que protege al país...
Ui: Previniendo crímenes antes de que sucedan...@[chara:101501:lipSynch_0][wait:1][chara:101501:cheek_0][chara:101501:face_mtn_ex_010.exp3.json][chara:101501:lipSynch_1]¡Eso se parece un poco a lo que hacemos!
Tsuruno: Pelear en sus uniformes escolares con armas@y eso... ¡Es increíble!
Takina: Los uniformes escolares tienen menos@probabilidades@de llamar la atención.
Chisato: Camuflaje urbano, básicamente.
Felicia: ¡Eso es super genial!@¡Es como una película!
Chisato: ¡¿Verdad que si?!
Chisato: Dicho eso, la Cafetería LycoReco es Lycoris@para individuos.
Mitama: ¿Para individuos?
Chisato: Así es, ¡ayudamos a los necesitados!@Por ejemplo, ayudamos en guarderías,
Chisato: aceptamos trabajo como profesoras de lenguas@extranjeras, e incluso servimos como@guardaespaldas.
Chisato: Es algo así como una rama de DA,@pero no lo es... ¿sabes?
Chisato: Pero aun así, estamos hablando de una@organización peligrosa.
Chisato: Harán todo los necesario para conservar el@secreto... Así que no le digan a nadie, ¿si?
Yachiyo: Sí... tienes razón. Ciertamente suena peligroso@dado lo que hemos aprendido...
Iroha: Emm... Entonces logramos compartir nuestros@secretos entre nos.
Ui: Así que... ¿Tal vez nos podamos llevar bien ahora?
Takina: ... Sí. Perdón por el ataque sorpresa de antes.
Kanagi: No, lo siento por leer su mente con@tal descaro.
Mitama: Supimos que tenían armas, así que nos@pusimos alerta.
Chisato: Ajaja... Bueno, por supuesto.
Yachiyo: ... Y como ustedes dos vinieron de otro mundo...
Yachiyo: ¿Supongo que todavía quieren regresar@a su mundo original?
Takina: ¡Sí!
Yachiyo: Estoy segura de que todas estamos de acuerdo.
Yachiyo: Mikazuki es un lugar al que queremos mucho@también.
Tsuruno: ¿Entonces por qué no nos ayudamos a recuperar@nuestros lugares?
Chisato: ¡Sí! Eso sería genial, ¿verdad, Takina?
Takina: Sí, agradeceríamos la ayuda.
Iroha: Muy bien, entonces...@Vamos a traer a Villa Mikazuki de regreso...
Chisato: ¡Y nosotras vamos a regresar a nuestro mundo@con la Cafetería LycoReco!
Yachiyo: Formemos un frente unido.
Yachiyo: Permítanme presentarme de nuevo.@Me llamo Yachiyo Nanami. Encantada de@conocerlas.
Chisato: ¡Yo me llamo Chisato Nishikigi!@¡Mucho gusto!
Takina: ... Y yo soy Takina Inoue.@Espero que podamos trabajar juntas.
