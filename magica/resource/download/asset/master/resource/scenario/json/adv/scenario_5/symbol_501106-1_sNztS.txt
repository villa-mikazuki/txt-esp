＜El primer encuentro de las@semifinales del Torneo de Hanetsuki@del Santuario de Mizuna...＞
＜¿¡Quiénes saldrán victoriosas!?＞
Iroha: [flashEffect:flashWhite2][bgEffect:shakeSmall][se:4061_landing_jump5]¡Aaaahhh!
¡Un punto!@... ¡Para un total de 4 puntos!
Felicia: ¡Je je! Puede que haya perdido@en el juego de cartas de Tsuruno...
Tsuruno: ¡Se llama Cartas Yui!
Felicia: ¡Pero no voy a perder esta vez!
Felicia: ¡Vamos a destrozarlas, Ayame!
Ayame: ¡Sí, sííí!
Iroha: ¡Lo siento, Tsuruno!
Tsuruno: Yo también... Sigo metiendo la pata.
Iroha: Estamos empatadas ahora.
Sana: (¡Por favor, consigan el siguiente punto!)
Tsuruno: ¡Todo se reduce a esto, Iroha!
Iroha: ¡Sí!
Tsuruno: Kghhh... ¡Se me pone la piel de gallina!
Felicia: Bien... ¡Aquí voy!
Felicia: [flashEffect:flashWhite2][bgEffect:shakeSmall][se:4141_jumping]¡Haaa!
Tsuruno: [flashEffect:flashWhite2][bgEffect:shakeSmall][se:4141_jumping]¡Hagh!
Ayame: [flashEffect:flashWhite2][bgEffect:shakeSmall][se:4141_jumping]¡Waaagh!
Iroha: [flashEffect:flashWhite2][bgEffect:shakeSmall][se:4141_jumping]¡Agh!
Sana: ¡Están devolviendo cada uno@de esos poderosos tiros!
Sana: ¡Buen trabajo en equipo!
Homura: N-no sé a qué equipo alentar...
Madoka: Umm... Umm...
Madoka: Uh, ¡vamos, ambos equipos, vamos!
Felicia: ¡Arggh! ¡Están devolviendo cada tiro!
Ayame: ¡Nunca terminaremos esto!
Hazuki: ¡Ayameee! ¡Feliciaaa! ¡Resistan!
Konoha: ...
Konoha: ¡Pueden hacerlo!
Iroha: *Jadeo* *Jadeo*
Felicia: ¡Gaaagh!
Iroha: [flashEffect:flashWhite2][bgEffect:shakeSmall][se:4141_jumping]... ¡Hyah!
Felicia: ¡Woa!
Felicia: ¡Mierda! ¡Me tropecé!
Ayame: [flashEffect:flashWhite2][bgEffect:shakeSmall][se:4141_jumping]¡Lo tengo!
Felicia: ¡O-oh no...!
Iroha: ¡Ahora...!
Iroha: [flashEffect:flashWhite2][bgEffect:shakeSmall][se:4061_landing_jump5]¡Huwaaah!
Ayame: Ack, ¡no puedo alcanzarlo!
Jueza: ... ¡Muy bien!
Jueza: ¡Un punto! ¡Eso hacen 5!
Tsuruno: Lo hicimos... [chara:100301:lipSynch_0][wait:1.0][chara:100301:motion_200][chara:100301:cheek_2][chara:100301:face_mtn_ex_011.exp.json][chara:100301:lipSynch_1][bgm:bgm03_story09]¡Lo hicimos, Iroha!
Iroha: *Jadeo* *Jadeo*... [chara:100101:lipSynch_0][wait:0.8][chara:100101:cheek_2][chara:100101:face_mtn_ex_011.exp.json][chara:100101:lipSynch_1]¡Lo hicimos!
Sana: ¡Buen trabajo!
Madoka: ¡Ambas estuvieron increíbles!
Homura: ¡Realmente lo estuvieron!
Felicia: Lo siento, Ayame...
Ayame: ¡Aw, eso no es propio de ti!
Ayame: ¡Además, me divertí mucho!@¡Hagámoslo de nuevo alguna vez!
Felicia: ¡Je je! ¡Por supuesto!
Kako: ¡Ambas estuvieron fantásticas!
Hazuki: ¡Sí! ¡Fue un buen juego!
Konoha: ... Muy bien entonces, vamos.
Ayame: ¿Eh? ¿A dónde?
Konoha: ¡A comprarles unas chocobananas@como recompensa por hacerlo tan bien!
Ayame: ¡Yeeey!
Felicia: ¡Sííí!
＜Tras una larga y dura batalla,@¡Iroha y Tsuruno habían ganado!＞
＜Lo que significaba que@se enfrentarían a...＞
Jueza: ¡Un punto!@... ¡Eso hacen 5!
Meiyui: ...
Meiyui: Perdimos...
Akira: ¡Eso fue intenso!
Akira: Fuimos superadas.@Debería haber entrenado más.
Rena: Lo logramos, Momoko.
Momoko: Sí...
Momoko: ¡Solo 1 más!
