Chisato: Nos leyeron la mente y descubrieron que somos Lycoris...@Las cosas se pusieron intensas por un rato,@pero pudimos demostrar nuestra inocencia.@¡Decidimos formar un frente unido con Yachiyo@y sus amigas para encontrar nuestro hogar!
Chisato: Bueno, ahora que nos hemos presentado...
Chisato: ¿Cómo le hacemos para regresar?
Tsuruno: Primero, vamos a hablar de que sucedió@cuando ustedes llegaron aquí.
Ui: Sí, si hay alguna pista,@¡ahí es donde la encontraremos!
Takina: ... Entendido. Por favor permítanme explicar.
Takina: "Todo empezó hace algunas horas...@DA nos dio órdenes, y estábamos en@camino para proteger a una chica que@estaba siendo retenida por terroristas."
Takina: "Cuando llegamos a la escena, otras Lycoris@ya se habían encargado del criminal principal,@así que todo lo que quedaba era@asegurar la seguridad de la chica."
Takina: "Sin embargo, la chica desapareció de repente...@Después de fallar la misión,@regresamos a la Cafetería LycoReco.@Estábamos a punto de irnos cuando..."
Yachiyo: La chica desapareció... ya veo. ¿Notaron algo@inusual antes o después de eso?
Chisato: Estaba al teléfono con Kusunoki, emm, alguien@de la sede de DA, cuando la llamada se cortó de@la nada.
Takina: ... Y, como suele ser el caso,
Takina: el sujeto bajo protección se podía escuchar@llorando y gritando en la escena.
Iroha: ¿Es por qué estaba... asustada?
Chisato: No sé... Puede que haya sido porque@el terrorista había sido asesinado...
Ui: Huh... ¿por qué? ¿No eran los malos que@la habían secuestrado?
Takina: Al parecer, la persona protegida había@estado en cautiverio durante mucho tiempo.
Takina: Es posible que haya desarrollado sentimientos@positivos hacia el criminal.
Tsuruno: Síndrome de Estocolmo...@He oído de algo parecido antes.
Takina: Sí... Por eso es que tuvimos que protegerla@adecuadamente y seguirla.
Chisato: Si hubiéramos llegado primero, podríamos haber@asegurado al culpable sin matarlo, pero...
Chisato: ... Por cierto, ¿puede la magia hacer que alguien@desaparezca de repente?
Chisato: ¿Es algo que ocurra mucho en@este mundo?
Kanagi: ... No es imposible. La magia podría causar una@aparición repentina también.
Mitama: Tal vez deberíamos buscar algo en este@mundo como la causa del problema.
Tsuruno: Además, sus teléfonos siguen sin poder@conectarse a nada, ¿verdad?
Takina: Sí, lo he intentado varias veces, pero ni siquiera@puedo mandar una señal desde el dron.
Ui: ¿Ese es un dron?@¡Es super genial!
Chisato: ¿Verdad? Lo hizo nuestra propia nerd residente.
Chisato: Gracias a eso, nos podemos comunicar con ella@a través del dron.
Chisato: Deberían poder monitorearnos desde aquí...
Mitama: Podría ser un problema de comunicación,@o interferencia de una Bruja o un [textBlue:Rumor].
Takina: ... Brujas son esas enemigas a las que@mencionaste antes. Los [textBlue:Rumores] entonces...@son, bueno, ¿rumores...?
Tsuruno: Aquí en la Ciudad de Kamihama, los rumores son@leyendas urbanas que cobran vida.
Chisato: ¡Otro giro de fantasía!
Iroha: Por eso es que se llaman [textBlue:Rumores].@Aunque todos ya debieron haber desaparecido...
Yachiyo: Si es algo relacionado con un [textBlue:Rumor], entonces@deberíamos ir a hablar con Nemu.
Yachiyo: Si no mal recuerdo, Mifuyu debería estar@con ellas hoy.
Ui: Y si tiene algo que ver con máquinas,@estoy segura de que Touka sabrá mucho.
Chisato: Um, ¿Y esas personas son...?
Kanagi: Un par de chicas genios... Chicas Mágicas con@las que Yachiyo a tenido algunos negocios.
Takina: ¿Están cerca? Esas chicas genio.
Iroha: Sí, han estado pasando mucho tiempo en@el radiotelescopio últimamente...
Iroha: ...Ah.
Yachiyo: Iroha, ¿acabas de...?
Iroha: Sí...
Felicia: ¿Ehh? ¿Qué pasó?
Yachiyo: ... Esa es la chica que salvamos de un Laberinto de@Bruja hace unos días.
Felicia: ¿Entonces no es una Chica Mágica?@Está bien entonces.
Iroha: Bueno, de hecho.... había una Chica Mágica ahí@dentro con ella...
[chara:800203:effect_shake]¡Uuu...aaaAAAAA!
¡Senpai! ¡Senpai...!@No... ¡NOOOO!
Tsuruno: Cuando llegamos, la Bruja ya había...
Felicia: ¿Oh? ... ¿Su Doppel no salió?
Yachiyo: Sucedió fuera de Kamihama...@Si hubiéramos llegado un poco antes...
Takina: ... ¿Qué sucede, chicas?@Todas han estado calladas por un buen rato.
Yachiyo: ... No es nada. Continuemos.
Mifuyu: Chisato y Takina están en un buen aprieto...
Nemu: Cuando se trata de "viajes de isekai"...@¿no sería esta tu área, Touka?
Touka: Sospecho que esto está relacionado con uno de@tus [textBlue:Rumores], Nemu.
Nemu: No recuerdo haber creado algún [textBlue:Rumor] que@pudiera hacer algo así.
Nemu: ... Pero no puedo estar completamente segura@de eso, así que supongo que una investigación@será necesaria.
Touka: ¡Entonces vayamos a Mikazuki y empecemos@nuestra investigación!
Nemu: ... No parece que haya [textBlue:Rumores] cerca.
Touka: Estas son circunstancias muy intrigantes@aparte de eso.
Touka: Otro edificio con un diseño diferente ha@reemplazado el interior de la Villa Mikazuki.
Touka: ¡Este flagrante desprecio por las leyes de la física@hace que me acelere el corazón!
Touka: Oh, ¡es cierto! También mencionaste tener@problemas de comunicación.
Takina: Ah, aquí... dale un vistazo...
Nemu: Oh mi... Es un dron bien hecho. ¿Supongo que@es trabajo de alguien de tu grupo?
Chisato: ¡Sí! Kurumi...@¡Es nuestra nerd informática!
Chisato: Una genio hacker, ¿sabes?
Touka: Mmm... Entonces si reparo esto, ¿podré hablar@con esta genio Kurumi?
Chisato: Debería seguir conectado, así que... ¿supongo que@sería posible?
Touka: *Risilla* Nemu, tengo un plan brillante.
Yachiyo: ... Espera. No es peligroso, ¿verdad?
Touka: Hay pocas cosas más peligrosas que cambiar de@lugares entre mundos.
Mifuyu: Tienes razón, pero...
Touka: Así que, Nemu, no puedes hacer ningún [textBlue:Rumor]@nuevo, ¿verdad?
Nemu: Correcto. Es posible que muriera si@lo intentara.
Touka: ¿Qué tal si sintetizamos un[textBlue:Rumor]@ya existente para trabajar aquí?
Nemu: No es imposible dependiendo de la extensión...@Pero no.
Touka: ¿Qué hay de ese [textBlue:Rumor]que archivamos@por su riesgo?
Nemu: ...Sí, la Uwasa del as Llamada a@Otro Mundo.
Nemu: ... Efectivamente, esa Uwasa podría estar@conectada con este dron...
Nemu: permitiéndonos hablar con esta genio@del otro mundo...
Iroha: ¿Estás segura de que está bien...?
Nemu: ... *Risita* Como con Touka, yo también estoy@intrigada con esta genio del otro mundo.
Nemu: Conectar el teléfono con otro mundo@vale la pena intentarlo.
Nemu: ... Mmm...@[chara:101471:lipSynch_0][wait:0.8][chara:101471:lipSynch_1][chara:101471:eyeClose_0]¿Qué te parece?
Chisato: ¡No puede ser! ¡¿Lo arreglaste?!
Touka: Se trataba simplemente de hacer pequeñas@reescrituras en el contenido del [textBlue:Rumor].
Touka: Hemos esencialmente convertido el dron en un@teléfono a otro mundo.
Nemu: Bueno, entonces, Chisato, ¿puedes operar el dron@por nosotras?
Chisato: ¡Claro!
... *Bip*
Kurumi: "¿...Oh? ¡OH! ¡Por fin nos conectamos!@¿Mm? ¿Me puedes escuchar?@¿Chisato? ¿Takina? ¡¿Dónde están?!@¡LycoReco está en serios problemas!"
Chisato: ¡Kurumiiiii! ¡Estamos en otro mundo~!
Kurumi: "¡¿Ehh?! ¡No seas burra!@¡Eso es completamente irrealista!"
Chisato: Pero de verdad sucedió...
Chisato: ¿Pero mencionaste que la Cafetería LycoReco@estaba en problemas?
Kurumi: "La Cafetería LycoReco se ha convertido en@una habitación completamente irreconocible.@Te enviaré un video en seguida.@Echa un vistazo."
Kurumi: "Es un desastre."
Yachiyo: Es de mala educación llamarle un desastre.@Es nuestra casa.
Kurumi: "*Suspiro* ¿Qué carajos es esto?@...¿Y quién eres de todos modos?"
Chisato: ¡Esa es la cosa! Nosotras tenemos el interior@de LycoReco aquí también.
Yachiyo: ... Y somos Chicas Mágicas. Quizás podrías decir@que somos... similares a las Lycoris.
Kurumi: "Ohh.. ¿Acabamos de aterrizar en@un montón gigante de problemas?"
Touka: *Risilla* Bien, entonces, Señorita Genio Hacker...
Touka: Tengamos una discusión.
