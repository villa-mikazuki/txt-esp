Iroha: Haa... haa... haa...
Iroha: ...
Iroha: La magia se empieza a desvanecer.
Iroha: (Tengo que apresurarme... O su pista se...)
???: Espera ahí.
Iroha: [chara:100102:effect_emotion_surprise_0][se:7226_shock]¡¿...?!
Iroha: ¡Lo siento!@¡Tengo prisa!
???: Te dije que pares.
Iroha: Umm, ¡es muy urgente!
???: Si estás decida en seguir,@tendrás que derrotarme antes.
Iroha: ¿Por qué?
???: Tú mas que nadie deberías saber por qué.
???: Fuiste tú quien perdió de tal manera@dentro de aquel Laberinto de Bruja.
Iroha: Eso quiere decir que fué usted quien...
???: Correcto. Entonces seguías consciente.
???: Si lo recuerdas, entonces iré al grano.
???: Me interrumpieron antes, pero...
???: Ahora puedo correrte de la ciudad@sin distracciones.
Iroha: ¿Correrme de la ciudad...?
???: Si.@Lo dejaste claro...
???: No tienes lo necesario para sobrevivir aquí.
Iroha: ¡¿...?!
???: Regresa por donde viniste.
Iroha: No.
Iroha: Vine aquí por una razón.@Así que—
???: ¿Y eso qué? ¿Quieres morir sin lograr@lo que viniste a hacer?
Iroha: Pero... Fuí con la Coordinadora a@ajustar mi Soul Gem...
Iroha: Así que debería estar bien.
???: Momoko de nuevo, ya veo.
???: Ah... Muy bien.
Iroha: ¡¿Me va a dejar ir?!
???: Sí, pero solo si puedes probar que@eres lo suficientemente fuerte.
Iroha: ¡¿...?!
???: Vamos.
???: Juzgaré por mí misma si es que@podrías sobrevivir aquí o no.
