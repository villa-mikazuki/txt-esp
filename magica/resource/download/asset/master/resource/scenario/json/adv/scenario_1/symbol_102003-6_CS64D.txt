Voz de Touka: ¡Ya regreseeeeé!
Nemu: Deberíamos ser capaces de encontrar una respuesta@a un ritmo acelerado, ahora que Touka está aquí.
Ui: Sí.
Touka: ¿De qué están hablando?
Touka: Espera, no... ¡Ui, no me digas!
Touka: ¿Tus pruebas encontraron algo malo, justo cuando@estabas a punto de dejar el hospital?
Touka: ¿Tus niveles de PNB, tal vez?
Nemu: Yo le pregunté lo mismo.
Ui: Los resultados de las pruebas fueron tan buenos@como siempre...
Ui: La cosa es, no estoy preocupada por mí misma.@Estoy preocupada por Iroha.
Touka: ¿Iroha...?
Ui: ¿Recuerdas el otro día, cuando la vimos@pelear?
Ui: Creo que desde entonces no ha dejado de luchar@contra esas cosas.
Ui: A veces parece que le está afectando demasiado.@No puedo soportar verla así.
Ui: De verdad quiero que deje de pelear.
Touka: Ya veo...
Nemu: Sí, es todo un dilema.
Nemu: Pero, ¿cómo la detenemos sin saber qué es lo que@queremos que detenga?
Touka: Es una Chica Mágica.
Nemu: Touka, esto es serio. Estoy preocupada por@Ui y su hermana.
Nemu: Ahora no es el momento para bromas tontas.
Touka: Yo también hablo en serio.
Nemu: ...[wait:1.0][chara:101402:lipSynch_1][chara:101402:voiceFull_fullvoice/section_102003/vo_full_102003-6-23] ¿En serio?
Touka: Pienso que es más extraño NO creer en ello,@después de haber visto ese extraño lugar.
Ui: Touka, ¡Por favor dime! ¿Qué es una Chica@Mágica?
Touka: No hay necesidad de entrar en pánico,@te lo diré. Creo que es por el bien de Iroha que@te lo diga.
Touka: Porque cuando oigan lo que tengo que decir,@se darán cuenta de lo que hay que hacer.
Touka: Hay una forma de salvarla...
Nemu: Creo que lo comprendo al 100% ahora.
Ui: Yo estoy como a 70%...
Ui: ¡No olviden que no soy tan inteligente como@ustedes dos!
Touka: Aun así, lo básico de lo que es una Chica Mágica@y las cosas que tu hermana ha estado haciendo...
Touka: Entiendes esas partes al menos, ¿cierto?
Ui: Sí, entiendo esa parte.
Ui: Y la parte de la Bruja y el Laberinto...@Y...
Ui: Entendí que si usamos el método del que@nos hablaste, y funciona...
Ui: Ayudará a todas, ¿verdad?
Touka: ¡Eso es exactamente!
Nemu: Las tres mejoraremos, y podemos ayudar@a Iroha... y muchas otras personas también.
Ui: Pero, ¿de verdad funcionará?
Nemu: Touka tiende a decir cosas que molestan a@la gente...
Nemu: Sin embargo... yo sé que nunca discutiría algo@tan desconectado de la realidad.
Nemu: Deposito toda mi confianza en el plan,@ya que es ella quien lo propuso.
Touka: ¡Me alegra mucho escucharte decir eso!
Touka: Pero me molesta un poco que dijeras que@molesto a la gente.
Nemu: Lo siento, se me escapó un poco de burla.
Touka: ¡¡¡Nyaaaaaargh!!!
Ui: ¡Chicas! ¡No se peleen!
Touka: ¡Hmph!
Touka: De todos modos, es un tema delicado, así que no@me apresuraré a ponerlo en práctica.
Touka: No tomaremos ninguna medida hasta que lo@entiendas por completo, Ui.
Ui: Entonces quiero que me lo expliques de nuevo,@¡ahora mismo!
Ui: ¡No tengo tiempo que desperdiciar!
Ui: ¡Quiero salvar a Iroha lo antes posible!
Touka: Sé cómo te sientes.
Touka: ¡Te lo explicaré tantas veces como sea necesario!
Touka: Lo primero que hay que entender es que hay@ventajas y desventajas de ser una Chica Mágica.
Touka: Las ventajas incluyen un cuerpo saludable con@mayor fuerza y resistencia.
Touka: La desventaja es que estás@obligada a pelear contra las Brujas.
Touka: También debes llevar la carga de tambien@convertirte en una Bruja, eventualmente.
Touka: Solo basándose en eso, parece que hay un@montón de desventajas.
Touka: Pero solamente tenemos que deshacernos de la parte@de convertirse en Brujas para hacerlo mucho mejor,@no solo para nosotras, sino para la humanidad.
Touka: Las Chicas Mágicas luchan contras las Brujas para poder@ganar Grief Seeds, las cuales son necesarias para@retrasar el proceso de conversión en Bruja.
Touka: ¡Pero si eliminamos ese resultado final,@no habría más necesidad de luchar contra Brujas!
Touka: ¡Al hacer eso intrínsecamente estaríamos@salvando a Iroha!
Ui: Y para hacer eso...
Ui: ¿Las tres nos tenemos que convertir en Chicas@Mágicas?
Touka: Correcto.
Touka: Nosotras tres podemos usar nuestros deseos@para eliminar los destinos de cada Chica Mágica.
Ui: E Iroha se salvará...
Nemu: También, cuando nuestros deseos se cumplan,@no tendremos que ser atormentadas por ese@destino tampoco.
Touka: ¡Todo lo contrario! Nos convertiríamos en@Chicas Mágicas y obtendríamos cuerpos fuertes.
Touka: ¡Y entonces las tres juntas con Iroha podremos@vivir felices por siempre!
Touka: ¡Felices, saludables, y capaces de perseguir@nuestros sueños como mejor nos parezca!
Nemu: Entonces... el paso más importante es el deseo@que pidamos, ¿cierto?
Touka: Sí, esa es la parte más importante.
Ui: Muy bien.
Touka: Estoy segura de que no se nos permitirá desear@como un trio algo como "borra los destinos de@las Chicas Mágicas". Violaría una ley natural.
Touka: Así que, las pediremos un deseo, una@por una, para arrebatarle uno de los poderes@que posee Kyubey.
Touka: Primero, Ui tomará el poder de "recolectar",@usado para mandar energía como impurezas o@energía de transición de fase al espacio.
Touka: Yo tomaré el poder de "convertir", usado@para transformar la energía recolectada,@así como las almas humanas en magia.
Touka: Y Nemu tomará el poder de "manifestar",@o dar a la energía una forma física, como@cuando Kyubey crea una Soul Gem.
Touka: ¡Con los tres poderes, podemos poner@nuestro plan en acción para liberar a las Chicas@Mágicas de los destinos que las condenan!
Touka: ¡Primero le toca a Ui!
Touka: Una vez que Nemu y yo tengamos suficientes impurezas,@tú puedes usar tu poder para recolectarlas.
Touka: Entonces me las puedes pasar a mí,@junto con un poco de tu magia.
Touka: ¡Entonces es mi turno!
Touka: Tomaré las impurezas y las convertiré en magia@para que las Chicas Mágicas puedan usar libremente.
Touka: Entonces le pasaré esa magia a Nemu,@con un poco de mi magia.
Touka: ¡Nemu terminará el proceso!
Touka: Ella usará su magia, junto con la magia@que yo le di, para darle una forma física a esa@energía, igual que como se crean las Soul Gems.
Touka: Ella hará un sistema automático de purificación.
Touka: Las tres usaríamos nuestra magia para crear@un sistema con todas las mismas funciones que tiene@Kyubey. Recolectaría energía automáticamente@y la enviaría al espacio exterior.
Touka: Eso también resolvería el problema del que@Kyubey hablaba: la entropía del universo.@¡Es una situación favorable por donde se le vea!
Touka: ¡De esta manera, las tres e Iroha@podremos tener un final feliz!
Touka: ¿Qué piensas?
Ui: ¡Creo que lo entendí todo esta vez!
Ui: ...
Ui: Me convertiré en una Chica Mágica con ustedes.
Touka: ¡Me da gusto que todo tengas sentido para ti!
Nemu: También podríamos ampliar el alcance de la@purificación ampliando el sistema.
Touka: Sí, ese es otro objetivo.
Touka: Si podemos hacer que Kyubey negocie con nosotras...
Touka: Podemos preguntar sobre todo tipo de cosas.@¡Incluyendo el espacio exterior! *Risilla*
Ui: ¿Espacio exterior?
