Emiri: Oye...
Hinano: ¿Ocurre algo, Emiri?
Emiri: ¡Se supone que tienes que decirlo!@¡No seas aburrida!
Iroha: ¿?
Tsuruno: ¿Decir qué...?
Emiri: Oh Dios mío, ella definitivamente es...
Emiri: Tiene que ser una copia.
Iroha: ¿¡Una copia...!?
Tsuruno: ¡No puede ser!
Hinano: ¡Oye, oye, oye!@¿¡De qué estás hablando, Emiri!?
Emiri: ¿Qué de qué estoy hablando?@¡No pretendas ser la verdadera!
Emiri: ¡Porque la única Myako-senpai@es Myako-senpai!
Hinano: Espera un segundo... ¿Qué quieres decir?
Iroha: (... Ella tiene razón. Estamos en@los Espejos Infinitos, después de todo...)
Iroha: (No hay evidencia de que esta sea@la verdadera...)
Emiri: ¡Suficiente!@¡Ahora REALMENTE me estás haciendo enojar!
Emiri: ¿¡Dónde está la verdadera Myako-senpai!?
Hinano: ¡Oye, espera un minuto!@¿Y cómo yo sé que TÚ no eres una impostora?
Emiri: ¿¡Cómo te ATREVES!?@¡E es de Emily y Emily soy YO!
Tsuruno: ¿Eh? ¿Qué? ¿Uhhh...?
Iroha: (Cierto, tampoco podemos descartar la@posibilidad de que Emily sea una farsante...)
Iroha: (Podría estar engañándonos.)
Iroha: (Pero... pero...@Ya no sé qué es real y qué es falso...)
Mitama: ...
Mitama: Te perderás de vista a ti misma si pasas@demasiado tiempo mirando al espejo...
Iroha: (¿A esto se refería Mitama?)
Emiri: ¡Rrrraa!
Emiri: [chara:300600:effect_emotion_angry_0][se:7219_strain]¡De verdad, ya basta!
Tsuruno: ¡Whoaaah!
Emiri: [chara:300600:effect_emotion_angry_0][se:7219_strain]¡Nop, no, con una ración de ni hablar y@una cobertura de NINGUNA MANERA!
Iroha: ¡E-Emily!
Emiri: ¡La conozco!
Emiri: ¡Cuando digo que es como un mono ardilla,@me golpea!
Hinano: Mono ardilla... ¿¡MONO ARDILLA!?[chara:300300:lipSynch_0][wait:0.8][chara:300300:cheek_1][chara:300300:face_mtn_ex_020.exp.json][chara:300300:lipSynch_1][chara:300300:effect_emotion_angry_0][se:7219_strain]@¡Emiriii...!
Hinano: [chara:300300:effect_emotion_angry_1][se:7218_anger]¿¡A quién estás llamando mono ardilla bebé!?@¡Estás arruinando mi imagen!
Emiri: ¡Y! ¡Yyyyyyy...! ¡Cada vez que me pierdo,@te enfadas muchísimo!
Emiri: Porque tú también te preocupas por mí...
Hinano: ¡Emiri! ¿¡Qué estabas haciendo!?@¡Eso es lo que pasa cuando te distraes así!
Hinano: ¡Cielos, no me preocupes de esa manera!
Emiri: Por eso yo sé que...
Emiri: ¡Tú definitivamente no eres@la verdadera Myako-senpai!
Tsuruno: Emily-sensei...
Iroha: Emily...
Iroha: (¡Se nota que se preocupa mucho por ella...!)
Hinano: ...
Hinano: ¿Y? ¿¡Y eso qué!?
Emiri: ¡Eeek!
Iroha: ¡Emily!
Tsuruno: ¡Whoa! ¿¡Estás bien!?
Emiri: ¡¡¡Owww!!!
¿Hinano?: ¡Todo este alboroto! ¡Qué fastidio!@¡Dibujando tantas líneas para separarnos!
¿Hinano?: ¡Solo sean unas buenas niñas y déjenme@usarlas en mis experimentos...!
Tsuruno: ¡Nngh! ¡Así que ahora muestra sus verdaderos@colores! ¡No puedes engañar a Emily-sensei!
Tsuruno: ¡Vamos, Iroha!
Iroha: ¡S-Sí...!
Emiri: Yo... estoy...
Emiri: ¡Super, SUPER enojada ahora!
