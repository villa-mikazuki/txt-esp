Sana?: ¿Merezco la felicidad, cuando soy tan inútil@y sin talento?
Sana?: Iroha eventualmente se cansará de que le@cause problemas todo el tiempo.
Sana: Ya no me preocupare más por cosas así@en Villa Mikazuki.
Sana: ¡Es el mejor lugar en el que estado en toda@mi vida! Realmente me siento segura.
Felicia?: Te olvidaste lo de vengar a tus padres. ¡Con@las Magius, ya no habrá más Brujas!
Felicia?: Y Yachiyo no te estará mandando todo@el tiempo. ¡Este bando es muuucho mejor!
Felicia: ¡Pero Iroha no está de su lado, entonces apesta!
Felicia: ¡Además, acordamos compartir nuestros problemas!
Tsuruno?: Soy la más poderosa, yo sé la verdad.
Tsuruno?: Esas chicas de Villa Mikazuki realmente no@me entienden.
Tsuruno: ¡En realidad, ya resolví eso!
Tsuruno: ¡Me aceptaron como soy, incluso con mis@debilidades!
Tsuruno: ¡Podría decir que nuestros lazos son los@más poderosos!
Yachiyo?: Solo vas a hacer que terminen matando a@tus amigas otra vez.
Yachiyo?: Solo puedo imaginar como tu@fachada ruda se@distorsionará en algo más...
Yachiyo: Estoy dispuesta a tomar ese riego. No puedo@imaginar mi vida sin ellas.
Yachiyo: Eso es lo mucho que significan para mí.
