Yachiyo: (¿Qué está planeando Mifuyu?)
Yachiyo: (Reunir a todo el mundo para asistir a una@conferencia, justo donde hay un [textBlue:Rumor]...)
Yachiyo: (La verdad detrás de las Soul Gems... y nuestro@destino de convertirnos en Brujas...)
Yachiyo: (Sé que será duro para ellas escucharlo,@pero si se niegan a aceptar la verdad...)
Yachiyo: ...
Yachiyo: (Tal vez el [textBlue:Rumor] sea para asegurarse@de que lo acepten...)
Yachiyo: (¿Pero cómo...?)
???: Pero si ves el recuerdo de otra persona,@puede que te influya...@¡Ese es el gran Rumor en el Distrito de Sakae!
Yachiyo: ¡...!
Yachiyo: Mifuyu... no se atrevería...
