Momoko: Acá es por donde la Coordinadora escuchó@sobre el Rumor.
Momoko: Rena ya debería estar aquí.
Momoko: Oh, ¡ahí está!
Momoko: ¡Oyeee! ¡Rena!
Rena: [chara:100901:effect_emotion_surprise_0][se:7226_shock]Ah, hola Momoko.
Momoko: ¡Felicidades por llegar hasta acá!
Rena: ¿Qué hay para "felicitar"? Prácticamente@me obligaste a hacer esto.
Momoko: Heh, perdón. En serio, me estás@ayudando un montón. Gracias.
Rena: Te va a costar un Mont Blanc el favor.
Momoko: [chara:101001:effect_emotion_surprise_0][se:7226_shock]¡Oye! ¡¿Cuándo te prometí eso?!
Rena: Wow, ¿no te aguantas ni un chiste?@No necesito ninguna paga.
Rena: Pero, sabes, no te diría que no si@me dieras uno.
Momoko: Tienes agallas.
Momoko: Así que, ¿compraste esa cosa que ibas a a@conseguir?
Rena: Uh-huh. Una tarta de frutas de Sunlight@Café.
Rena: Kaede las ama. Se la llevaré después.
Momoko: No dejes que se pudra esta vez.
Rena: ¡No lo haré!
Rena: De todos modos, traté de averiguar sobre el [textBlue:Rumor]@del parque de diversiones.
Momoko: Aww, sabía que podía contar contigo, Rena.@Muchas gracias.
Momoko: ¿Descubriste algo?
Rena: Mucha gente parecía saber sobre eso...
Rena: Especialmente gente de nuestra edad.
Momoko: Como lo sospeché...
Rena: ¿Qué quieres decir?
Rena: Espera, ¿cuándo empezó todo este tema sobre un@parque de diversiones de todos modos?
Momoko: Fue super reciente, creo.@Yo recién me entero.
Rena: Oye, Momoko.
Momoko: ¿Sí?
Rena: Una vez que Kaede se sienta mejor deberíamos@celebrar todas juntas.
Rena: ¿Y si vamos a ese parque de diversiones?
Momoko: ¡¿Qué?!
Rena: Whoa, ¿cual es tu problema?
Momoko: Rena, lo entendiste mal.
Momoko: Sabes que este no es un [textBlue:Rumor] ordinario, ¿cierto?@Es como la Regla de la Ruptura de Amistades.
Rena: [chara:100901:effect_emotion_surprise_0][se:7226_shock]¡¿Qué?! ¡¿Estás bromeando?!
Rena: ¡¿Por qué no me dijiste eso desde el comienzo?!
Rena: [chara:100901:effect_emotion_angry_1][se:7218_anger]¡Me perdí por completo!
Momoko: ¡Okay, mi error, mi error! ¡No te enojes!
Momoko: Cuando hablamos sobre un rumor, en general es@sobre los Rumores malos!
Rena: ¡¿Cómo se supone que sepa eso si no lo@dices?!
Momoko: Ahh, tu ganas, te compraré un Mont Blanc...
Rena: [chara:100901:effect_emotion_joy_0][se:7222_happy]*Risita* Todo está perdonado.
Momoko: Genial.
Rena: Es extraño, aún así... No escuché nada@sobre esto en la escuela.
Momoko: Ese es el tema.
Momoko: Eso es exactamente lo que me está molestando.
