*Campanada*
*Aplauso* *Aplauso*
Felicia: ...
Felicia: (Hmm, se supone que debo rezar, pero...)
Felicia: No tiene sentido hacer esa fila tan larga@solo para comprar un amuleto.
Felicia: *Estomago gruñe*
Felicia: Ah... me muero de hambre.
Felicia: Oh, ¿qué es ese olor?
Felicia: ¡Woa! ¿¡Brochetas de albóndigas de pollo!?
Felicia: ¡Se ven riquísimos!
Felicia: (Ni siquiera compré un amuleto...)
Felicia: Bueno, si tuviera que desear algo...
Felicia: (Deseo seguir viviendo en casa de Yachiyo@y comiendo comida deliciosa.)
Felicia: (Y quiero comer un buen pedazo de carne@de vez en cuando.)
Felicia: (Tal vez algo de ternera.)
Felicia: (¡Oh, y quiero ir a una granja!)
Felicia: (¿Dónde estaba esa granja a la@que fui con mamá y papá?)
Felicia: ...
Felicia: (Papá... mamá...)
Felicia: (Tengo que vengarme.)
Felicia: (Voy a DESTRUIR a esa Bruja,@y luego volveré con Yachiyo.)
Felicia: (Aparte de eso, solo quiero relajarme@y pasar el rato con las demás.)
Felicia: (Eso es todo... Gracias por escuchar.)
