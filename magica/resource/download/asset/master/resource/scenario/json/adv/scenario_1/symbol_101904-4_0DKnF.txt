Momoko: La controlamos, de alguna forma.
Rena: Ugh, realmente quisiera pasar de esta mierda.
Kaede: ¿Estás bien?
Pluma Blanca: ...
Pluma Blanca: Al final, si creemos en las Magius y@seguimos aspirando la liberación...
Pluma Blanca: Tendremos nuestros futuros arrebatados@de nosotras...
Momoko: Si, es eso lo que les espera...
Pluma Blanca: *Suspira*
Pluma Blanca: Me siento como si me despertaran de la nada.
Pluma Blanca: Lo siento.
Momoko: Hey, todas seguimos respirando, ¿verdad? Está@bien. Hablemos de nuevo cuando todo esto termine.
Momoko: Pero cuando eso ocurra, al menos déjanos@ver tu cara, ¿Okay?
Pluma Blanca: ¡Esperen!
Rena: ¿En serio? ¿Qué más quieres? ¡Estamos@algo apuradas!
Pluma Blanca: Acabo de recibir un mensaje telepático@de las otras Plumas... Capturaron a Iroha Tamaki.
Rena: ¡¿Qué?! ¡Maldita sea, ahora realmente no@tenemos tiempo!
Pluma Blanca: No serán capaces de alcanzar a esas Plumas@ahora.
Kaede: ¡¿Qué?!
Rena: ¡¿Entonces QUÉ podemos hacer?!
Pluma Blanca: Ábranse su camino.
Momoko: ¿Abrir dónde?
Pluma Blanca: Destruyan los pisos y caigan hasta abajo@Entonces lograrán llegar a tiempo.
Pluma Blanca: Deberían encontrarse a Iroha Tamaki y a las@Plumas que la capturaron.
Momoko: Pero todo este edificio es un [textBlue:Rumor].
Pluma Blanca: Yo me encargaré del Uwasa por ustedes.
Momoko: ¿Segura?
Pluma Blanca: Si, escogí estar de su lado chicas. Solo vayan.
Rena: No hay vuelta atrás, lo sabes.      
Pluma Blanca: Lo sé
Rena: ...
Rena: *Suspira* Muy bien. Romperé un hoyo por aquí.
Kaede: ¡Te ayudo!
Rena: ¡Aquí va! ¡Kaede!
Kaede: ¡Lo tienes, Rena!
Rena: [flashEffect:flashWhite2][bgEffect:shakeLarge][se:4052_landing_dive_sv]¡Rrrrrraaaaaagh!
Kaede: [flashEffect:flashWhite2][bgEffect:shakeLarge][se:4004_equip_arms_magic_vh]¡¡¡Yaaaaaaaaah!!!
[se:7214_witch_noise_02]｜rawr-rawr(◎ｘ◎)rawr-rawr｜
Pluma Blanca: [flashEffect:flashWhite2][surround:7151A01][bgEffect:shakeLarge]¡Hah!
[flashEffect:flashRed2][surround:7151A02][bgEffect:shakeLarge][name:sprite_0:effect_shake]｜?!ra(Ｏｘ＜)wr?!｜
Pluma Blanca: ¡Váyanse! ¡Ahora!
Momoko: ¡Gracias!
