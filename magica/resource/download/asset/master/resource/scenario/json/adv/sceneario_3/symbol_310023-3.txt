Yachiyo: Sé quién lastimó a Momoko...
Rena: Oye, espera... ¡No fui yo!
Rena: ¿Por qué lastimaría a Momoko?@¿Por qué estás...? ¡Oye, vamos!
Kaede: No hay manera... ¡Yachiyo, estás cometiendo@un error! ¡No puede ser Rena!
Yachiyo: La persona que atacó a Momoko...
Yachiyo: ... fuiste tú, Tsuruno.
Rena: ¿Eh?[freeze0.1+][chara:100900:effect_emotion_surprise_0][se:7226_shock]
Kaede: ¿Qué?[freeze0.1][chara:101100:effect_emotion_surprise_0][se:7226_shock]
Tsuruno: ¿Yo?![freeze0.1][chara:100300:effect_emotion_surprise_0][se:7226_shock]
Yachiyo: ¡Hmph![se:4050_landing_jump]
Tsuruno: ¡Gah![se:6252_hit_spike_01_s]
Rena: ... Espera, ¿qué está pasando?
Kaede: ... No tengo idea...
Tsuruno: ¿C-Cómo...?
Tsuruno: ... ¿Cómo lo descubriste?
¡□▽★※◎◆♯Puff!
Kaede: ¡Gyaaah!
Rena: ¡Era una Familiar...!
Yachiyo: Y ha traído algunos amigos...
¡□▽★※◎◆♯Puff!
Rena: ... ¡Entonces eso significa...!
Yachiyo: Que hemos caído en la trampa de una Bruja.
Yachiyo: Incluso trataron de dividirnos.@¡Diablillos astutos...!
Kaede: Así que este fue el plan de Tsuruno todo el@tiempo... ¿¡Era una Familiar!?
Yachiyo: ... No, ella todavía está aquí en alguna parte.@Probablemente ha sido capturada por la Bruja.
Kaede: ¿E-En serio?
Rena: ¿Cómo sabes todo esto, Yachiyo?
Yachiyo: Ustedes dos concéntrense en los enemigos@que tenemos enfrente.
Rena: ¡Wow, eso estuvo cerca!
Kaede: ¡S-Sí!
Yachiyo: ¿Realmente creyeron que podrían@ponernos unas contra otras?
Yachiyo: ... Qué plan tan despreciable ...@¡Definitivamente lo pagarán!
