Pluma Negra: [flashEffect:flashRed2][se:6100_hit_slash_01_s][bgEffect:shakeLarge][chara:715004:effect_shake]¡¡¡Augh!!!
Pluma Negra: Maldición... ¡Estábamos tan cerca!
Momoko: [flashEffect:flashWhite2][bgEffect:shakeLarge][se:6101_hit_slash_01_vh]¡Todo termina aquí!
Pluma Negra: [flashEffect:flashRed2][se:6100_hit_slash_01_s][bgEffect:shakeLarge]Urgh.
Momoko: Iroha, ¡¿Estás bien?!
Iroha: S-si... Gracias.
Momoko: Espera, ¿Por qué no estás transformada?
Iroha: Las Magius se llevaron mi Soul Gem.
Momoko: ¿En serio?
Pluma Blanca: Momoko Togame, llegaste hasta aquí...
Momoko: Más Plumas de afuera...
Iroha: E-espera, ¡Son ellas! ¡Ellas tienen mi@Soul Gem!
Momoko: ¿Esas chicas la tienen?
Iroha: ¡Si!
Pluma Negra: ¡Muévanse! ¡Al menos lleven la Soul Gem@a las Magius!    
Pluma Blanca: ¡Entendido!
Momoko: ¡Eso no es bueno!
