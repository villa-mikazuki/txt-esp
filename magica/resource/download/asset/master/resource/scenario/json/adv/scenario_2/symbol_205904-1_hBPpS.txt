Mami: Lo sé...
Mami: A estas alturas, puede que esto parezca un@pensamiento egoísta.
Mami: Aun así, detendré a Magius.
Mami: Y protegeré a esta ciudad de Walpurgisnacht.
Mami: Esa es la única cosa que puedo hacer,@como alguien que ayudó a Magius.
Pluma Negra: Dices todo eso, pero ¿qué se supone que@debo hacer? *Llanto*
Pluma Blanca: Estoy de acuerdo con Mami.
Pluma Blanca: También creo que hay una línea que no@se debe cruzar.
Pluma Negra: ¿Realmente puedes detener a Walpurgisnacht?
Pluma Negra: ¿Pueden realmente mi padre, mi madre y@mis amigos salvarse?
Pluma Blanca: ¡Un momento! ¿Qué creen que están diciendo@todas ustedes?
Pluma Blanca: ¡Ya casi somos libres!
Pluma Negra: ¡Tiene razón. Estamos muy cerca de conseguir@todo lo que queríamos!
Pluma Blanca: ¡O-oigan, cálmense!
Pluma Blanca: Puede que sea así.
Pluma Blanca: Pero no me alegraría que nos liberaran a costa@de la ciudad de Kamihama.
Pluma Blanca: Ni siquiera eres de aquí... Qué hipócrita.
Pluma Blanca: ¿Hipócrita?
Pluma Blanca: ¿Por pensar que invocar a Walpurgisnacht en@Kamihama es algo malo?
Pluma Blanca: ¿Soy una hipócrita por sentirme así?
Pluma Blanca: Bueno...
Pluma Blanca: Simplemente no puedo aprobarlo ahora que@lo sé.
Pluma Blanca: No se trata de que está bien o que está mal.@Es solo que... ya sabes.
Pluma Blanca: Es una cuestión de orgullo, y de las plegarias@que hice yo misma.
Pluma Blanca: Bien. Yo seguiré persiguiendo mis sueños. Y no@tendré piedad si se interponen en mi camino.
Pluma Negra: Cierto... Kamihama no es asunto mío.
Pluma Negra: No quiero convertirme en una Bruja...
Pluma Negra: Si una pequeña ciudad es todo lo que se@necesita para decidir si me vuelvo una o no...
Pluma Negra: No podría importarme menos.
Pluma Negra: ¡No te dejaré... No lo haré. No dejaré que@destruyas esta ciudad!
Pluma Negra: ¿No eres tú también de Kamihama?@No llores, lucha.
Pluma Negra: *Llanto* No puedo... No puedo...@Liberación o Kamihama... No puedo elegir.
Pluma Blanca: ¿Y qué hay de ti? ¿Vas a luchar por la liberación@como la Pluma Blanca que eres?
Pluma Blanca: Yo...
Pluma Blanca: Solo puedes seguir tu propio corazón.@A estas alturas no se trata del bien o del mal.
Pluma Blanca: ... Lo siento. Elijo la liberación.
Pluma Blanca: ¡Entonces vamos!
Pluma Blanca: ¡Aplastaremos a las traidoras y@nos liberaremos!
Plumas: "¡HYAAAAAAAAAAAAGH!@¡Acaben con todas las traidoras y@aquellas que se interpongan en@el camino de la liberación!"
Mami: ¡¿Qué?! [chara:200500:lipSynch_0][wait:1.5][chara:200500:cheek_0][chara:200500:face_mtn_ex_020.exp.json][chara:200500:lipSynch_1]¡E-esperen un minuto!
Pluma Blanca: Aquellas que quieran proteger su ciudad natal...@Aquellas que se preocupen por la justicia...
Pluma Blanca: ¡Síganme! ¡Las detendremos!
Plumas: "¡Protejan a Kamihama!@¡Protejan nuestra ciudad natal,@nuestras familias y@nuestros amigos!"
Mami: ¡Deténgase! No me sorprende que tengan@puntos de vista opuestos.
Mami: ¡Pero les prohíbo que usen la fuerza para@resolver esto!
Mami: Eso no resolverá na—
Mami: [flashEffect:flashWhite2][bgEffect:shakeLarge][surround:715010A01]¡¿...?!
Pluma Negra: Cállate. Ya no te necesitamos, santa.
Pluma Blanca: [flashEffect:flashWhite2][bgEffect:shakeLarge][surround:7151A01]¡Si te metes en nuestro camino, acabaremos@contigo. Y a todas las Plumas traidoras!
Pluma Blanca: [flashEffect:flashWhite2][bgEffect:shakeLarge][surround:7151A01]¡Concéntrese en nuestro objetivo! ¡Este es el@problema con el que tenemos que lidiar!
Mami: ¿Cómo puede estar pasando esto?@¿He cometido otro error?
Mami: ¿No debería haberles dicho?
Mami: (Mifuyu habría podido explicarles mejor@las cosas.)
Mami: (¿Empeoré las cosas?)
Madoka: Mami.
Mami: Madoka...
Madoka: Detengámoslas. Todavía podemos.
Sayaka: Tiene razón. ¡Nos tienes a nosotras!
Homura: ¡Yo también haré lo mejor que pueda!
Nanaka: Nosotras también ayudaremos.
Hinano: Se trata de Kamihama, después de todo.
Mami: Chicas...
Mami: Sí, tienen razón. No es momento de quedarse@de brazos cruzados.
