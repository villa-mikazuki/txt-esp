Pluma Negra: ¡Es inutíl huir de nosotras!
Iroha: *Jadeo* *Jadeo*
Pluma Negra: ¡Pedí refuerzos!
Pluma Negra: ¡No se nos van a escapar!
Iroha: *Jadeo* *Jadeo*
Sana: ¿Estás bien, Iroha?
Iroha: Sí...
Pluma Negra: *Jadeo* *Jadeo* Te atrapé.
Sana: Ay no... ¿Qué hacemos?
Pluma Negra: ¡Enfóquense en capturar a Iroha Tamaki!
Pluma Negra: ¡Yo te cubro!
Pluma Negra: ¡Déjamelo a mí!
???: Supongo que yo soportaré a este lado, entonces.
Pluma Negra: [chara:715004:effect_emotion_surprise_0][se:7226_shock]¡¿Q-quién está ahí?!
Kyoko: En serio, el momento en el que salí, ví@a un montón de Plumas volverse locas.
Kyoko: Y ahora, cuando pensé que todo se había@calmado, ¿hay una pelea dentro de la base?
Kyoko: Este lugar nunca se calma, ¿verdad?
Sana: ¿Y-y tú eres?
Iroha: ¡¿Kyoko?!
Kyoko: ¿Quién pensaría que nos veriamos de nuevo@en un lugar como este?
Pluma Negra: Tú debes ser la recluta más reciente de las@Plumas.
Pluma Negra: Ayúdamos a capturar a Iroha Tamaki.@Esa es una orden de Magius.
Kyoko: ¿Alguna vez piensan por su cuenta?
Kyoko: ¡Siempre es las Magius esto, las Magius@lo otro, siguen y siguen!
Pluma Negra: ¿Qué...?
Kyoko: Me uní a las Plumas para ver@de que se trataba.
Kyoko: Pero solo están o escupiendo cosas que les@están diciendo...
Kyoko: O intentando aprovecharse de la miseria de@la gente.
Kyoko: Así que, este no es realmente mi tipo de lugar.
Kyoko: De hecho, me enfadan un poco.
Pluma Negra: ¿Nos estás traicionando?
Kyoko: ¿De verdad necesitas que responda eso?
Iroha: Kyoko...
Kyoko: Tampoco estoy aquí para ayudarlas.
Kyoko: Solo estoy aquí para partirles el hocico a estas@babosas, porque no las soporto.
Kyoko: ¡Vamos, Chica Escudo! ¡Ayúdame!
Sana: ¡O-okay!
