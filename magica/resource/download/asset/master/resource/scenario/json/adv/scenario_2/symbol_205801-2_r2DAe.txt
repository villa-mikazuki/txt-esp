Touka: *Risilla* Me pregunto qué está pasando aquí.
Nemu: Sin duda, una situación no deseada,@se mire por donde se mire.
Kyoko: ¿Quién eres tú? Tengo algo que discutir con—
Mifuyu: Esta es una nueva Pluma.
Kyoko: ¡Oye!
Mifuyu: ¿Puedes decir si ganarías contra esas dos si se@produjera una pelea?
Mifuyu: Estoy segura de que puedes, considerando tu@nivel de experiencia.
Mifuyu: Me aseguraré de que la situación no empeore.@Sígueme la corriente.
Kyoko: ...
Kyoko: Bien, lo que sea. Hazlo.
Touka: ¿Una Pluma? Oh, ¿una Pluma Negra?
Mifuyu: Sí. Podemos confiar en ella.
Mifuyu: Sin embargo, parece que es amiga de Mami,@y el comportamiento de ésta la desconcertó.
Mifuyu: Estoy a punto de explicarle cómo Mami está@sirviendo como nuestra santa de la liberación.
Kyoko: Perdón por hacer un escándalo.
Kyoko: Fue un gran shock para mí, porque ella es tan@diferente ahora.
Touka: ¿Oooh? ¿Está seguuura?
Touka: Muy bien, siempre y cuando no interfieras.
Nemu: Mami es el problema más urgente aquí.@Arreglarla no será nada fácil.
Mami: ¿Yo... estuve mal? Entonces, ¿quién... quién@está bien? Mal... Bien... Mal... Bien...
Mifuyu: Nos iremos ahora.
Kyoko: ...
Nemu: Ahora lo ha hecho de verdad. La Uwasa está@empezando a desacoplarse de Mami.
Touka: ¡Awww! ¡Eso es muy malo!
Touka: Parece que conoce a Mami, ¿crees que haya@venido aquí para recuperarla?
Touka: ¿Podría ser quizás nuestra enemiga?
Nemu: Es una posibilidad innegable.
Nemu: Pero es probable que le incomodara el estado@actual de Mami, generando su desconfianza.
Nemu: En cualquier caso, hemos tenido suerte.
Touka: ¡Heh heh! Es cierto. ¡Parece que es muy hábil!
Touka: ¡Tal vez sea capaz de aplastar a la veterana!
Nemu: Pero por ahora, tenemos que enfocarnos...
Mami: Por favor, Touka... Nemu... Díganme,@¿estuve equivocada todo este tiempo?
Touka: Sí, será mejor que hagamos algo al respecto,@y rápido.
Nemu: Déjame ocuparme de esto.
Nemu: Mami, no has hecho nada malo.@Borraré el dolor por ti.
Nemu: [flashEffect:flashWhite2]Todo lo que necesitas es pensar en@la salvación.
Mami: ...
Nemu: ¿Cómo te sientes?
Mami: *Risita*
Mami: Me siento como si acabara de despertar.
Mami: ¿Cómo pude dudar...?
Mami: Incluso el acto del sacrificio se hace noble por@un objetivo tan alto como el nuestro.
