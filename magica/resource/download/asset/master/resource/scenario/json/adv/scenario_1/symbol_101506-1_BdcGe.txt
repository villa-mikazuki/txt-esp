Alina: No esperaba que sobrevivieran...
Sana: Alina...
Alina: Han arruinado mi diversión...
Alina: Y el Laberinto de esa Uwasa era el lugar@perfecto para esconder una Bruja.
Alina: Qué decepción... 
Yachiyo: Alina Gray... ¿No eres tú esa artista prodigio?
Alina: ¿Así que has escuchado sobre mí?
Yachiyo: Tu serie dibujada con animales carbonizados...@"The Revived Dead".
Yachiyo: Por desagradables que fueran, también fueron@excepcionalmente hermosas. Dejaron su impacto.
Yachiyo: Y terminaste siendo una Chica Mágica...
Alina: Pues, aquí estoy.
Alina: Yo, Alina, soy una artista verdadera.
Alina: Pero también soy una Chica Mágica, y una de las@Magius.
Alina: De cualquier forma, dejemos esta charla sin sentido
Alina: Ustedes destruyeron ese Uwasa...
Alina: Así que ahora tendré que buscar otro escondite@para esta Bruja.
Madoka: ¿Dónde está Mami?
Madoka: ¡Devuélvela!
Alina: Ya te lo dije, se la dí de comer a una de mis@obras. No hay nada que puedas hacer por ella.
Alina: ¿Got it? ¿Entendido?
Homura: ¿Mami de verdad... acabó de esa manera? ¿Aquí, de@todos lugares?
Alina: ¿De verdad quieren que se las regrese?
Madoka: ¡Por su puesto que sí! ¡Ella es una querida amiga!
Alina: Bueno, supongo que la podría devolver...
Alina: Me disculpo si no logran reconocerla.
Madoka: Cómo pudiste...
Tsuruno: Oye, te pasaste de la raya.
Yachiyo: Sé que somos enemigas, pero fuiste muy@lejos esta vez.
Alina: ¿Y qué vas a hacer al respecto?
Alina: Hasta donde yo sé, a ustedes, que destruyeron@mi Uwasa, son a quienes no puedo perdonar.
Iroha: Esa no excusa. ¡Ellas han estado bucando a@Mami todo este tiempo!
Iroha: ¿Cómo puedes decirles algo así?
Alina: Wow... No las soporto.
Alina: Ustedes solo van a ser un dolor de culo.@Tal vez debería borrarlas ya.
Alina: Chicas, vengan aquí y ayudénme.
Tsukasa: S-sí...
Tsukuyo: Será un placer...
Alina: Esa trampa que pusieron les saló por la culata@espectacularmente.
Alina: Ustedes dos tienen que compensar por nuestras pérdidas.
Tsukuyo: Por favor, dejanoslo a nosotras...
Alina: Muy bien entonces, yo voy a esconder esta Bruja en mi@Laberinto y haré mi salida.
Alina: ¡Good bye!
Iroha: ¡¿Absorbió el Laberinto de la Bruja?!
Tsukasa: La magia única de Alina puede crear Laberintos.
Tsukuyo: Cierto.
Tsukuyo: De cualquier forma, no nos haría bien dejar que@arruinen nuestras trampas y destruyan nuestros [textBlue:Rumores].
Tsukuyo: Sí, como Plumas Blancas, no podemos dejar que@se salgan con las suyas.
Tsukasa: Así que tendrán que pasar sobre nosotras.
