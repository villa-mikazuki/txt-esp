—Distrito de Shinsei—
Kaede: (Creo que la florería de Konomi está por aquí.)
Kaede: (¡Tengo que darme prisa y decírselo!)
Kaede: Pero, ¿y si ya la atraparon?
Kaede: *Lloriqueo*
Kaede: ...
Kaede: ¡No!
Kaede: (¡Tengo que dejar de ser tan negativa!)
Kaede: (¡Momoko y Rena me dieron muchos ánimos!)
Kaede: Siento que hay una Chica Mágica cerca...
Kaede: Creo que no la conozco. ¿Quizá está aquí porque@también ha percibido a una bruja?
Kaede: (No, espera... Se está acercando...)
Kaede: ¡Ah!
Kaede: ¡¿Qu-qué-qué está pasando?!
Pluma Negra: Urrr...
Kaede: ¡Una p-pluma negra!
Kaede: ¡Uh, um!
Kaede: Escuché que estabas intentando matar a@todo el mundo...
Pluma Negra: [flashEffect:flashWhite2][bgEffect:shakeLarge][surround:715000A02]¡Vrraaagh!
Kaede: [flashEffect:flashWhite2][bgEffect:shakeLarge]¡Ah! ¡¿Qué estás haciendo?!
Pluma Negra: Vvr...
Kaede: Ah...aay no...
Momoko: ¡Vamos, Kaede, contrólate!
Rena: ¿Vas a quedarte ahí sentada llorando?
Kaede: ¡T-tengo que luchar!
Pluma Negra: Vvv...
Kaede: Hay otra...
Kaede: ¡¿Y otra por allá?!
Kaede: Ay no, no puedo con todas yo sola...
Konomi: ¡Kaede!
Kaede: ¡K-Konomi!
Konomi: ¡¿Qué está pasando?!
Kaede: Te lo explicaré más tarde.@¿Podrías... Um... Ayudarme a luchar?!
