Me llamo Iroha Tamaki.
Soy nueva en Kamihama...@Así que aun no conozco mucho por aquí.
Tal vez podamos ayudarnos@en las cosas que desconocemos.
¡Oh! Y...um, mi pasatiempo es cocinar.
Tal vez, si te parece bien,@¿Te podría invitar a cenar alguna vez?
No soy la misma chica que alguna vez fue tan@tímida e insegura... Pero incluso ahora,@todavía quiero contarle todo a Ui algún día.
Creo que ella lo entendería sin importar en@qué tipo de persona me convierta.
Me volveré tan fuerte como sea necesario...@para poder encontrar lo que más importa.
Está bien aceptar mi debilidad, ¿verdad?
Empieza poco a poco y sigue@adelante un paso a la vez.
Tal vez he cambiado,@aunque sea solo un poquito.
No podía soportar a la chica que una vez fui,@pero no pudría haber llegado hasta aquí si no@fuera por ella...
Necesito aceptarla y@superar mis límites de antes.
Ahora que conozco a todos por aquí,@creo que por fin lo entiendo.
No quiero esconder como@me siento de mis queridos amigos...
Tengo que dejarles saber mis verdaderos@sentimientos,porque son muy preciados para mi.
No quiero mentirle a la gente que amo...
Hagamos esto juntos.
Si nos apoyamos entre todos...
Podremos llegar a lugares que seríamos@ incapaces de alcanzar solos.
¡Buenos días!@¿No te encanta decir eso?
Algo en esa frase me@ayuda a enfrentar el día.
Si todavía no has comido,@¿te gustaría que almorcemos juntos?
No te preocupes, no soy quisquillosa con la comida.
¿Te gustaría algo en particular?
Hice de cenar, ¿Quieres un poco?
Dime si le falta un poco de sal.
Estoy acostumbrada a comerlo así,
pero no sé si te gustará a ti.
¿Todavía no te vas a dormir?
Si no te importa, me podría quedar despierta@contigo... Quedarse despierto tarde...
No es bueno hacerlo todo el tiempo,@pero de vez en cuando...
De vez en cuando, creo que está bien.
Siento que mi gusto en comida es diferente@del de los demás...
Sopa de miso, vegetales al vapor...@Platillos simples pero perfectos.
Ya casi es hora de irnos, ¿cierto?
Estoy lista cuando tú lo estés.
¡No te apresures, estaré esperando aquí mismo!
No importa que tan largo pueda ser el viaje,
si aparece una Bruja simplemente@no puedo ignorarla.
Pero ¡no te preocupes!
¡Estoy casi segura de que no me perderé!
Me pregunto, ¿Qué será popular hoy en día?
Si, sé que es una pregunta rara@viniendo de alguien de mi edad.
Todos escriben tan rápido en sus@smartphones, no lo puedo creer.
Y mírame a mi, ni siquiera puedo cambiar@el fondo de pantalla de mi celular...
La comida de una madre es especial.
Tiene algo que te hace sentir@que todo va a estar bien.
¿O solo soy yo?
No les puedo seguir el ritmo a los chicos de@mi edad cuando empiezan a hablar de canciones@o series populares.
Tal vez debería empezar a leer@sobre ellas en línea...
Momoko es como esa hermana mayor confiable.
Ahora que lo pienso, ella es como@una hermana mayor para Kaede y Rena.
Yachiyo acepta todos nuestros sentimientos...
Cuando estoy con todas,@mi tristeza desaparece.
Felicidad, tristeza, todas compartimos@nuestros sentimientos...
Felicidad, tristeza, todas compartimos@nuestros sentimientos...
¡Deberías ver el manga que Felicia@está leyendo!
Está lleno de \"SLAMS\" and \"BAMS\",@¡Es muy emocionante!
Tan emocionante que no creo que@mi corazón pueda manejarlo...
Sana, y yo... pienso que nos@parecemos un poco.
Quizás sea por eso que me siento tan a gusto@cuando estamos juntas.
He escrito todas las recetas@que me ha enseñado mi mamá...
Supongo que la mayoría de los jóvenes@simplemente las escriben en la@computadora, ¿no?
¡Todo va a estar bien!
¡Menos mal que no me rendí!
Tuve fé en que todo estaría bien.
¡Tu hermana mayor lo logró!
¿Me convertí en una hermana mayor fuerte?
Me llamo Iroha Tamaki.@Por favor no dudes en preguntarme@cuando necesites ayuda...
Sé que suena como que lo digo porque si,@pero lo digo en serio...
Cada vez que me vuelvo más fuerte,@el futuro se ve más prometedor...
Es emocionante.
Logré llegar hasta aquí...@Creo que está bien sentirme segura@de mi misma ahora, ¿verdad?
¡Mmm!
 *Suspiro*
Me siento rejuvenecida.
Ahora puedo seguir adelante.
Ui es la mejor hermana menor de todo el mundo...
Quiero crecer un poco más, para que ella@pueda decirles a todos que tiene a la@mejor hermana mayor de todo el mundo.
Le mostraré a Ui una sonrisa de corazón...@Y entonces ella responderá de la misma manera.
Ahí es cuando sabré que nos hemos reunido de verdad.
