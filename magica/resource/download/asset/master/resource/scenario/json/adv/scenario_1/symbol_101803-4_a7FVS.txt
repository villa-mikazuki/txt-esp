Iroha: [bgEffect:shakeLarge]¡Sanaaaaaaaaa!
Sana: E-estoy b-bien...
Iroha: ¡No, no estás bien!
Touka: Asombroso.
Touka: El escudo de la chica invisible sí que@es resistente.
Nemu: No es poca cosa resistir uno de los@ataques de Touka.
Nemu: Creo que puedes sentirte bien con eso.
Touka: Aun así, no creo que mantenga en pie si la@golpeo de nuevo.
Touka: Mi magia única me permite transformar@la energía.
Touka: Así que mientras estemos en Kamihama,@no hay nadie que pueda enfrentarse a mí.
Touka: *Risitas*
Sana: Ugh... Perdóname, Iroha...
Iroha: ¡No tienes que lamentar nada, Sana!@Me protegiste...
Iroha: ¡Escucha, Touka, Nemu!
Iroha: Son mis recuerdos los que te confunden tanto.
Iroha: No metas a Sana en esto.
Touka: No lo entiendes en absoluto, ¿Verdad?@¿Acaso tu cerebro está conectado a tu cuerpo?
Touka: Si se va a poner en el camino por alguien a@quien queremos hacer eliminar...
Touka: Entonces es natural que tengamos que@deshacernos de ella también.
Nemu: En cualquier caso, dudo que vuelva a mover@ese escudo pronto.
Nemu: Así que ahora Iroha Tamaki es todo lo que se@interpone en nuestro camino.
Touka: ¡Sí!
Touka: Podemos tomar a la chica invisible y dársela@de comer a Eve más tarde.
Touka: Ahora, ¿vamos a acabar con Iroha Tamaki y sus@interminables divagaciones insanas?
Nemu: Sí, vamos. Es una pena tenerla actuando como si@todas estas coincidencias fueran el destino.
Nemu: Yo tampoco soporto ese tipo de fantasías@irreales.
Iroha: ¿Todavía dudas de mí?
Iroha: Esto es un [textBlue:Rumor] que me inventé.@Conozco todos los detalles.
Touka: Adelante entonces. Cuéntanoslo.
Iroha: ...
Iroha: El [textBlue:Rumor] de la Sakura Eterna va así, ¿no?
Iroha: En un campo tan ancho y extenso que todo el mundo@podía correr alrededor de él a la vez, y todos podían@sentarse cómodamente para ver las flores,@había un gran árbol de sakura.
Iroha: Un día, tres niñas atendidas en el hospital@estarán lo suficientemente sanas como@para dejar el hospital e ir allí.
Iroha: Allí se reunirán con la última niña,@que siempre venía a visitarlas.
Iroha: Y cuando eso ocurra,@el enorme árbol florecerá para celebrar@el reencuentro de las cuatro niñas.
Iroha: Así es, ¿verdad?
Iroha: Dos de esas chicas son ustedes.@Touka y Nemu.
Iroha: Pero no tienes ni idea de quiénes son las@otras dos chicas, ¿verdad?
Iroha: Por eso no puedes deshacerte de ellas...
Iroha: Las dos últimas chicas...
Iroha: Te has olvidado de ellas.
Iroha: ¡Somos yo y mi hermana pequeña, Ui!
Touka: Hmmm...
Iroha: ¿Eh? ¿Touka?
Touka: Bueno, ¿tienes alguna prueba?
Touka: ¿Alguna prueba de que tus recuerdos son@correctos?
Iroha: ¿Eh?
Nemu: El contenido del Rumor coincide, sí, pero me@temo que hará falta más que eso.
Nemu: Existe la posibilidad de que hayas escuchado@la historia de los Uwasa en alguna parte.
Nemu: El jefe del este podría incluso haber leído@nuestras mentes durante nuestra última batalla.
Nemu: Además, todas las historias que creo son@ficticias.
Nemu: Esos cuatro personajes no existen, ni siquiera@les he puesto nombre.
Iroha: Pero...
Iroha: ¿Entonces, cómo voy a convencerte?
Iroha: Me gustaría que ustedes dos pudieran recordar...
Iroha: Como cuando sostuve a userName...
Iroha: ¡¿...?!
Iroha: Tal vez userName pueda hacerlo...@Tal vez les pase lo mismo.
Iroha: ¿Qué piensas, userName?
(Player Choice): Mokyu.@(¡Intentémoslo!)
userName: Mokyu.
Iroha: Si eso hace que se acuerden de Ui y de la verdadera@historia detrás de este [textBlue:Rumor]...
(Player Choice): Mokyu, mokyu.@(Creo que podría.)
userName: Mokyu, mokyu.
Iroha: Entonces puede que sepan dónde está Ui...
(Player Choice): ¡Mokyukyu!@(¡Síii!)
userName: ¡Mokyukyu!
Iroha: Si ambas recuperan sus recuerdos, entonces@pondrán fin a todo esto.
Iroha: ...
Iroha: ¡Necesitamos al menos intentarlo!
(Player Choice): ¡Mokyu!@(¡Ahora es nuestra oportunidad!)
userName: ¡Mokyu!
Touka: ¿Así que no tienes ninguna prueba que presentar?@Entonces todo terminó para ti.
Iroha: ¡Espera!
Iroha: No puedo mostrarte ninguna prueba.
Iroha: Pero podría ser capaz...
Iroha: ¡De hacer que tú y Nemu recuerden!
Iroha: ¡Así que por favor!
Iroha: ¡Agarra este pequeño Kyubey, userName,@y abrázalo fuerte!
userName: ¡Mokkyuu!
Touka: [flashEffect:flashWhite2][bgEffect:shakeSmall]¡Nygh!
Iroha: ¡Por favor, Touka!
Touka: [flashEffect:flashWhite2][bgEffect:shakeLarge] Ugh... ¡Suficiente! ¡Aléjate de mí!@¡Mi ropa se está ensuciando!
userName: [flashEffect:flashRed2][bgEffect:shakeLarge][se:3001_chara_damage]¡Pugyu!
Iroha: ¡userName!
Nemu: Te agradecería mucho que no pusieras tus sucias@zarpas sobre nosotras.
Nemu: [flashEffect:flashWhite2][bgEffect:shakeLarge]Vete. Vuelve con tu dueña.
userName: [flashEffect:flashRed2][bgEffect:shakeLarge][se:3001_chara_damage][chara:810100:effect_shake]¡Meep!
Iroha: ¡userName! ¿Estás bien?
userName: Mo-mokyu...
Touka: Ugh.@¡No va a pasar nada si tocamos esa cosa!
Iroha: Pero, por qué...
Iroha: Cuando toqué este Kyubey, y lo tuve en mis brazos,@recordé todo...
Nemu: Ese Kyubey ha estado en Kamihama desde hace@mucho tiempo.
Nemu: Ya lo habíamos tocado mucho antes de que se@encariñara contigo, Iroha Tamaki.
Nemu: No fue una mala idea, pero por desgracia para ti,@no llegó a nada.
Touka: Sí. Qué pena.
Touka: Ninguno de las dos se cree una palabra de@lo que dices.
Nemu: Es hora de deshacerse de ellas, Touka.@Tanto de Iroha como de su Kyubey.
Touka: Sí, se te ha acabado el tiempo.
Sana: ¡Por favor! Por favor, ¡no lo hagas!
Touka: Pfft... ¡Lo siento mucho!
Iroha: Touka...
Nemu: ¡Adiós, Iroha Tamaki!
Iroha: Nemu...
Iroha: Por favor, recuerda...
