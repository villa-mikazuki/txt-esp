Felicia: Guh...hhh... *Jadear*
Pluma negra: [flashEffect:flashWhite2][surround:715000A04][bgEffect:shakeLarge]¡S-síii!
Felicia: [flashEffect:flashWhite2][bgEffect:shakeLarge][surround:0001_jump01][chara:100500:effect_shake]¡Hmph!
Felicia: Mierda... No voy a aguantar mucho más...
Pluma negra: ¡La tenemos!@¡Si atacamos ahora, podemos derribarla!
Pluma negra: [flashEffect:flashWhite2][surround:715000A04][bgEffect:shakeLarge]¡Yaaaaaaaaah!
Felicia: [flashEffect:flashRed2][surround:715000A03][bgEffect:shakeLarge][chara:100500:effect_shake]¡Nyaugh!
Tsuruno: ¡Felicia!
Felicia: ¡Estoy bien! ¡Todavía puedo luchar!
Tsuruno: Pero...
Tsuruno: Pero hemos vencido a muchas de ellas...
Tsukuyo: ¡Ahora!
Pluma negra: ¡Sí!
Kanagi: ¡Ugh! ¡A-aléjate de mí!
Tsukuyo & Tsukasa: [flashEffect:flashWhite2][bgEffect:shakeLarge][surround:1018A05]¡Flute Blossom Resonance!
Kanagi: [flashEffect:flashRed2][surround:1018A06][bgEffect:shakeLarge][chara:101600:effect_shake]¡Gwaaaaaah! ¡Ese sonido!
Tsukasa: ¡Kanagi!
Tsukasa: ¡No queremos matarte!
Tsukasa: [flashEffect:flashWhite2][bgEffect:shakeLarge][surround:1019A04]¡Lo sentimos!
Kanagi: [flashEffect:flashRed2][surround:1019A08][bgEffect:shakeLarge][chara:101600:effect_shake]¡Gaaah!
Kanagi: Kgh... *Jadear*
Kanagi: Ugh, mis piernas están a punto de ceder...
Yachiyo: [flashEffect:flashWhite2][bgEffect:shakeLarge][se:4050_landing_jump]¡Rrraaaaaaaaah!
Mami: [flashEffect:flashWhite2][surround:2500A02][bgEffect:shakeLarge]¡Yaaaaaaaaah!
Yachiyo: [flashEffect:flashWhite2][bgEffect:shakeLarge][chara:100200:effect_shake]*Jadear* *Agitarse*
Mami: [flashEffect:flashWhite2][surround:2500A02][bgEffect:shakeLarge]¡Esto termina ahora!
Yachiyo: [flashEffect:flashRed2][surround:2500A03][bgEffect:shakeLarge][chara:100200:effect_shake]¡Agh!
Yachiyo: *Agitarse* ¡N-no aún!
Mami: Qué sorpresa.
Mami: Las demás apenas son capaces de mantenerse en@pie en este momento.
Mami: Realmente eres especial, Yachiyo.
Yachiyo: Tal vez...
Yachiyo: No he estado luchando como Chica Mágica durante@los últimos 7 años por nada.
Yachiyo: ¡No hay manera de que caiga de rodillas aquí!
???: Yachiyo...
???: ¡Yachiyo-senpai!
Yachiyo: ¡¿...?!
Yachiyo: (¡¿Qué fue eso?! Me pareció oír voces...)
Yachiyo: (¿Qué está pasando? De repente siento algo...)
Yachiyo: ¿Qué es esto?
Yachiyo: Se siente cálido, y de alguna manera reconfortante.
Yachiyo: ¿Algo dentro de mí?
Yachiyo: No, no es eso. ¿La magia de alguien más?
Yachiyo: Todavía puedo luchar.
Yachiyo: No, ahora mismo podría incluso detenerte...
Mami: Es interesante que aún puedas fingir una bravuconada@como esa en el estado en el que estás.
Yachiyo: No es una bravuconada. En el fondo sé que puedo@enfrentarme a ti.
Yachiyo: Es difícil de describir, pero puedo decir@que algo me apoya...
Mami: No tengo ni idea de lo que estás hablando.
Mami: Oh, bueno. No importa.
Mami: De todas formas, tus amigas no durarán mucho más.
Mami: Es solo cuestión de tiempo que cada una de ustedes@se convierta en una ofrenda bajo mi guía.
Mami: ¡Ven, mi querida Florence!
Mami: Es demasiado tarde para rendirse, Yachiyo.
Mami: ¡Tu destino termina aquí!
Yachiyo: ¡Yo podría decir lo mismo de ti!
Yachiyo: ¡Tu tiempo como Uwasa ha terminado, Mami!
