[se:7213_witch_noise_01]｜¿¡boo(ＴωＴ)hoo?!｜
Iroha: *Jadeo* *Jadeo*
Iroha: ¡Yachiyo! ¡Tenemos que encontrar a Mifuyu y las@hermanas Amane de nuevo!
Yachiyo: Sí.
Yachiyo: Puedo sentir algo...
Felicia: Es la magia de esa chica rara.
Sana: Sí, esta tiene que ser Alina.
Tsuruno: Pero es raro.
Tsuruno: ¡No deberíamos ser capaces de sentir la magia de@NADIE aquí!
Tsuruno: Es un Laberinto.
Felicia: ¡Y hay mas de una de ellas!
Iroha: Nos quieren encerrar de nuevo...
Iroha: Lo que significa...
Yachiyo: ¡¿Que están usando una Bruja para protegerse@a sí mismas?!
Yachiyo: Me duele admitirlo...
Yachiyo: Pero hicieron muy buen trabajo en preparar@sus defensas.
Mifuyu: ¡Yachan!
Yachiyo: ¡Mifuyu! ¡¿Puedes volver a entrar?!
Mifuyu: ¡Está cerrado por todos lados!
Yachiyo: ¿Pueden romper las peredes para entrar?
Mifuyu: No parece que simplemete nos dejara afuera.@Puede que el edificio en si haya cambiado.
Mifuyu: No sabemos que tan rápido pueden reparar el@edificio o que tan fuerte es la Uwasa.
Yachiyo: ¡Pero eso siginifica que no hay forma de@volver a entrar!
Mifuyu: Haremos lo que podamos aquí afuera.
Yachiyo: ¿Qué planeas hacer?
Mifuyu: Intentaremos atraer a Magius y a Eve@afuera.
Mifuyu: No somos muchas aquí afuera.
Mifuyu: Pero si reunimos a todas las Chicas Mágicas de@Kamihama, tendremos la ventaja estratégica.
Yachiyo: Entonces... Mifuyu, ¿Eso quiere decir que vas@a destruir este [textBlue:Rumor]?
Mifuyu: Sí. Detrozaremos el Hotel Fendt Hope, y@derrotaremos a la Uwasa.
Mifuyu: Si nos podemos deshacer del [textBlue:Rumor], eso@cambiará la situación completamente.
Yachiyo: Aunque eso va a requerir casi toda tu@magia...
Mifuyu: Este [textBlue:Rumor] es su territorio. Sería@más peligroso no hacerlo.
Yachiyo: Muy bien.
Mifuyu: Yachan, deberían adentrarse al Rumor lo más@dentro que que puedan lo más rápido que puedan.
Mifuyu: Apresuren a llegar a la capilla subterránea.
Yachiyo: Estás diciendo que quieres que nos refugiemos@ahí para no quedar atrapadas en la explosión.
Mifuyu: Sí. Las esperaremos antes de@derrumbarlo.
Yachiyo: ¡Iroha!
Yachiyo: Mifuyu va a intentar atraer a las Magius y a@Eve afuera.
Yachiyo: ¡Para lograrlo, van a destruir el@[textBlue:Rumor Fendt Hope] por nosotras!
Yachiyo: No queremos quedarnos aquí cuando el@edificio se venga abajo.
Yachiyo: ¡Así que vamas a la capilla subterránea,@donde están las Magius!
Iroha: ¡Suena bien!
Iroha: Pare que muchas Brujas y Familiares terminaro@aquí también. ¡Están fuera de control!
Iroha: ¡Deberíamos atacar mientras hay caos!
Tsuruno: Felicia, saquemos a estas Brujas y@Familiares del camino.
Felicia: ¡Ah, entonces vamos a hacer un camino!
Tsuruno: ¡Sip, así es!
Yachiyo: Mifuyu...
Mifuyu: ¿Sí?
Yachiyo: Ten cuidado, ¿sí?
Mifuyu: *Risilla* Yo te debería decir eso.
Yachiyo: Tienes razón.
Yachiyo: Muy bien, no vemos después.
Mifuyu: ¡Sí, hasta luego!
Pluma Negra: Según lo que nos dijo Mami...
Pluma Negra: Este clima extremo, y los desastres que@están a punto de suceder...
Pluma Negra: Somos quienes lo causaron.
Pluma Negra: ¿Por qué Magius nos harían cometer algo@como esto?
Pluma Negra: ¿Cómo es que esto va a ayudar a liberarnos?
Pluma Negra: ¿Deberíamos seguir confiando en ellas?
Pluma Negra: ¿Estará bien tener fe en Magius@después de todo esto?
Pluma Negra: Dinos, Mifuyu...
"Debido a un rápido aumento de baja presión,@advertencias de lluvias severas e inundaciones han sido@emitidas a toda la Ciudad de Kamihama."
"Cantidades récord de lluvia@se esperan en las siguientes horas.@Aquellos que puedan moverse, deberían ir@a sus centros de evacuación designados de inmediato."
