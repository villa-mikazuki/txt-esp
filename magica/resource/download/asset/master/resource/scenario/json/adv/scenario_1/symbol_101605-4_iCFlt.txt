Yachiyo: Hmm... Mm...
Yachiyo: ¿Eh? ¿Dónde... estoy?
Iroha: ¡Yachiyo-san!@¡Menos mal que despertaste!
Yachiyo: ¡¿...?!
Yachiyo: Tamaki-san...
Iroha: No hay nadie más aquí, y tú no@despertabas...
Iroha: No sabía qué hacer...
Iroha: ¡Deprisa, salgamos de aquí!
Yachiyo: ...
Yachiyo: ¿Estás con las Alas de Magius?
Iroha: ¿Eh? ¿Yo?
Iroha: Um...
Iroha: N-no, ¡por supuesto que no!@¡Las rechacé en ese sueño!
Yachiyo: Entonces eso significa... que lo viste todo...
Iroha: Sí...
Iroha: Vi lo que le pasó a Kanae-san,@y también a Mel-chan...
Iroha: No fue como si solo lo "viera", sentí@como si estuviera allí...
Yachiyo: Interesante...
Iroha: Pero ahora lo entiendo.
Iroha: Entiendo por qué dijiste que mi Doppel@era peligroso...
Iroha: ...y por qué te esforzaste tanto en protegerme@en el Santuario Séanse.
Iroha: Sin embargo, me sorprendió un poco@despertarme y verte ahí tirada.
Yachiyo: Por supuesto que estaba allí tumbada.@Me mostraron el mismo sueño.
Iroha: Gracias por venir a ayudarnos.
Yachiyo: No me malinterpretes.@No he venido a ayudarlas.
Iroha: [chara:100101:effect_emotion_surprise_0][se:7226_shock]¿Eh?
Yachiyo: He venido a ver los recuerdos de Mifuyu y@a reevaluar mi forma de pensar actual...
Yachiyo: Y lo logré. Ahora estoy en consonancia@con cómo era hace un año.
Iroha: ¿Qué significa eso?
Rena: Siempre pensé que habías decidido dejar el@equipo por tu cuenta.
Momoko: No, no fue así.
Momoko: Aunque lo estaba pasando mal, Yachiyo-san seguía@siendo alguien a quien admiraba.
Momoko: Realmente pensé que un día podría volver@a la normalidad. Me aferré a esa esperanza.
Momoko: Pero un día, Yachiyo-san nos dijo de repente...
Yachiyo: Tamaki-san...
Momoko: Nos dijo a Tsuruno y a mí...
Yachiyo: Voy a disolver el equipo...
Momoko: Todavía no sé por qué dijo eso, incluso@hasta el día de hoy...
