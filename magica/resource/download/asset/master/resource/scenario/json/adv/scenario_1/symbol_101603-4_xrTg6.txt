Yachiyo: Ya veo, así que esa es la historia detrás del@[textRed:Museo de los Recuerdos]...
Rena: Sí, eso es todo lo que sabemos.
Kaede: ¿Sirvió de algo?
Yachiyo: Sí, gracias. Esta información debería@ser más que suficiente.
Yachiyo: Rena, Kaede... Han sido de gran ayuda.
Yachiyo: Muy bien, será mejor que me vaya.
Rena: Gracias por atendernos.
Yachiyo: Me has dado información, así que es justo.
Yachiyo: Nos vemos.
Momoko: ¡Espera, Yachiyo-san!
Yachiyo: ¿Qué pasa?
Momoko: He estado pensando...
Yachiyo: Si hay algo que quieres decir,@es mejor que lo digas ya.
Momoko: ¡¿Tienes el valor de hablarme así!?
Momoko: Ugh, no, lo siento...
Momoko: Es que hoy les contaré a Rena y Kaede...@Les voy a contar todo.
Yachiyo: ¡¿...?!
Yachiyo: Ya veo... Así que les vas a contar lo@que vimos.
Momoko: Sí. Voy a contarles todo lo que sé.
Yachiyo: Ten cuidado.
Momoko: Por supuesto...
Momoko: Tú también tienes que tener cuidado,@Yachiyo-san.
Momoko: Iroha-chan y Tsuruno estaban preocupadas@por ti.
Yachiyo: Eso no es asunto tuyo, Momoko.
Momoko: Es mi estilo hacer que sea mi asunto.
Yachiyo: Ya lo sé. Vale, adiós.
Yachiyo: Así que Momoko va a decirles a esas dos@la verdad sobre las Chicas Mágicas...
