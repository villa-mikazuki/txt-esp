Shizuku: Este es el lugar al que me dijo que viniera...
Ryo: ...
Shizuku: Ahí está... ¿pero está de mal humor?
Shizuku: Hola, Ryo.
Ryo: ¿Hm? [chara:304601:lipSynch_0][wait:1.0][chara:304601:cheek_0][chara:304601:face_mtn_ex_010.exp.json][chara:304601:lipSynch_1]Oh, hola, Shizuku.
Ryo: Siento haberte hecho venir hasta aquí...
Ryo: ¿Podrías esperar un segundo?
Shizuku: No hay problema. ¿Pero qué estás haciendo?
Ryo: Estoy haciendo el boletín para las Plumas.@Los artículos acaban de llegar, así que...
"La Uwasa de la Tierra de la Quelación@se Desmorona en Nada"@Según el último anuncio de Magius, las cuotas@de recolección de energía no se han visto afectadas@y están dentro de lo previsto.@Esperamos su próximo movimiento.
Shizuku: Así que también te ocupas de estas cosas.
Ryo: Sí, esto viene con ser parte del departamento de@relaciones públicas de las Alas de Magius.
Shizuku: ¿Una Uwasa fue destruida? ¿Eso está bien?
Ryo: Buena pregunta.
Ryo: No creo que las Magius dirían algo que@despertara preocupación, así que...
Ryo: Puede que estén maquillando un poco la verdad.
Shizuku: Ya veo...
Ryo: No creo que sea motivo de preocupación.
Ryo: No detecto nada raro, así que...
Ryo: Ya está. Supongo que voy a enviar esto ahora.
Shizuku: Bien, ahora que eso está hecho...
Ryo: Sí, siento haberte hecho esperar@después de pedirte que vinieras.
Shizuku: Ikumi me dijo de lo que querías hablar.
Shizuku: Debería ser yo quien se disculpe@por preocuparlas...
Ryo: Hacemos esto porque queremos.
Ryo: De todos modos, si ya sabes de lo que@quiero hablar, eso nos ahorra tiempo.
Ryo: Vamos, salgamos de aquí.
Ryo: Puedes ayudarme con mi próximo trabajo.
Shizuku: ¿Eh? ¿No íbamos a hablar de@pertenecer a algún lugar?
Ryo: Pertenecer no consiste solo en ubicaciones físicas,@¿verdad? ¡Vamos entonces! Date prisa.
Ryo: ¡Tenemos que acelerar el ritmo o@el Reporte de Midori nunca se imprimirá!
Shizuku: ¿El Reporte de Midori?
Ryo: Es el periódico escolar que publico.
Ryo: Abarca desde chismes cotidianos hasta los@logros de las estudiantes, e incluso corrupción.
Ryo: Es un trabajo que escribo por mí misma,@para mí misma.
Shizuku: Así que... ¿es por satisfacción propia?
Ryo: Se podría decir eso.
Ryo: Entre los más populares está el Diario del Gato@del Día, pero aún no he conseguido una foto...
Ryo: ¡Oh, hablando de eso! ¡Shizuku!
Shizuku: ¿S-sí?
Ryo: ¿Podrías hacerme un favor y usar@tu magia de Unión Espacial?
Ryo: Hay un callejón oculto en Sankyo donde se suelen@reunir gatos. ¡Solucionaría todos mis problemas!
Shizuku: Ugh...
Ayaka: ¿Unión... Espacial?
Ayaka: ¿Qué es eso?
Shizuku: Exactamente como suena...@La habilidad de unir dos espacios.
Shizuku: Es mi habilidad especial.
Ayaka: ¡Eso es INCREÍBLEEE! ¡No tenía ni idea@de que alguien pudiera hacer eso!
Ayaka: Oh, cielos...
Ayaka: ¡Podrías dormir más de la cuenta y aún así@llegar a tiempo en plan "no pasó nada"!
Shizuku: (Como en los viejos tiempos...)
Shizuku: Muy bien...
Shizuku: Solo asegúrate de no depender@de ella todo el tiempo.
Ryo: ¡Muchas gracias! ¡Realmente te debo una!
