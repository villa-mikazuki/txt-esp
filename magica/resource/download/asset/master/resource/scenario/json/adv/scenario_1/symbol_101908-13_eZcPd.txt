Iroha?: ¿Desde cuándo nos volvimos alguien obstinada?
Iroha?: Iroha Tamaki no es una persona con carácter.
Iroha: Eso no es cierto. Lo que ves frente a ti@es mi verdadero yo.
Iroha: Soy la nueva Iroha Tamaki.
Iroha?: Tu solo vagabas por el colegio, nunca desarrollaste@una relación real.
Iroha?: Tenías miedo de decir algo fuera de lugar,@así que te quedabas callada.
Iroha?: Siempre te preocupabas sobre lo que las@personas pensaban de ti, así que solo los seguías
Iroha: Es por eso que nunca tuve a alguien a quien@llamar amigo antes...
Iroha: Solo era la estudiante animada y agradable@que a nadie le importaría.
Iroha?: Pero gracias a eso, tuviste una vida estudiantil@tranquila.
Iroha: Aunque... No podía decirle la verdad a Ui.
Iroha: Le seguí mintiendo sobre el colegio. Estaba@tan emocionada por ir.
Iroha: Actuaba como si la pasaba muy bien.
Iroha: Nunca volveré a ser como era antes.
Iroha: Cuando Ui desapareció y empecé a pasar@tiempo con Yachiyo...
Iroha: Comencé a poner los pies en la tierra.
Iroha: Y todo a mi alrededor sigue cambiando.
Iroha: Conocí a muchas Chicas Mágicas, y formé@amistades irremplazables.
Iroha: Compartimos nuestro dolor y preocupaciones@entre todas.
Iroha: Es por eso que le digo adiós a la@persona insípida que era antes.
Iroha: Porque ahora, finalmente puede tener mi@frente en alto.
Iroha: Ahora tengo mejores amigas a quienes@quiero presentarle a Ui cuando la vea de nuevo.
Iroha?: Al final, es a tu hermana a la que te@aferras tanto... Es a Ui.
Iroha?: ¿Que vas a hacer si ella nunca existió?
Iroha: ¿Si Ui nunca existió?
Iroha?: Antes de tener que lidiar con el horror de@descubrir eso...
Iroha?: Deberías detener lo que estás haciendo,@como dijeron Touka y Nemu.
Iroha?: Lo que estás haciendo es inútil.
Iroha?: Esas dos no te recuerdan, y Ui ni@siquiera existe.
Iroha?: ¿Vas a seguir siendo así de egoísta?
Iroha?: ¿Seguirás causando todo este dolor?@ ¿Seguirás pisoteando los sueños de todas?
Iroha: ...
Pluma Negra: Mi hermana y yo nos rompimos por la desesperación@y deseamos escapar de nuestros destinos.
Pluma Negra: Las otras Plumas y yo solo queremos vivir@pacíficamente en nuestras ciudades.
Iroha: Lo que dices no es incorrecto.
Iroha: Algunas plumas sufren por sus hermanas, que se@convirtieron en Brujas.
Iroha: Y otras Plumas quieren vivir en paz en sus@hogares.
Iroha: Cada Pluma, cada Chica Mágica, tiene sus@propios sentimientos por esto.
Iroha: Todas en esta guerra tienen sus propias@opiniones y deseos.
Iroha: Pero creo en mis recuerdos.
Iroha: ¡Tengo que detener a Touka y Nemu de hacer@sus pecados aún más peores de lo que ya son!
Iroha: ¡Antes de que recuerden a Ui!
Iroha: (Y si las palabras de Ui trajeron a@algunas Chicas Mágicas a Kamihama...)
Iroha: (¡Eso significa que realmente está conectada@al plan de las Magius!)
Iroha: (Escuché a Ui decirme esas palabras en el@Santuario de Reunión.)
Iroha: ¡Me niego a creer que ella no existe!
Iroha: ¡Y creo que Touka y Nemu la recordarán!
