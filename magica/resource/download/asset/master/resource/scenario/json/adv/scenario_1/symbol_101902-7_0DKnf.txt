Pluma Negra: [flashEffect:flashWhite2][surround:715000A04][bgEffect:shakeLarge]¡Yaaargh!
Sana: [flashEffect:flashRed2][surround:715000A03][bgEffect:shakeLarge][chara:100400:effect_shake]¡Ah!
Iroha: ¡Sana!
Sana: ¡Está bien! ¡Todavía puedo pelear, no te preocupes!
Iroha: ¡Pero!
Pluma Negra: ¡Ahora te tengo!
Iroha: Ay no...
Sana: ¡Iroha!
Pluma Negra: Ahora, guarda silencio y ven con nosotras.
Iroha: ¡¿Por qué iría con ustedes?! ¡Eso solo sería@ponerme en peligro!
Pluma Negra: [flashEffect:flashWhite2]¡Entonces te voy a forzar!
Iroha: [flashEffect:flashWhite2]¡Agh!
Iroha: ¡...!
Iroha: ¡¿De verdad te parece bien todo esto?!
Iroha: La Magius tienen sus vidas en las palmas de@sus manos.
Pluma Negra: ¡¿...?!
Pluma Negra: ¿De qué estás hablando?
Iroha: Están confiando en gente que sacrifica a sus@aliados...con sus vidas.
Iroha: Entiendo lo que las Alas de Magius@intentan lograr y su significado.
Iroha: ¿Pero qué pasa después? ¿Cómo las@van a tratar?
Pluma Negra: He aceptado ese riesgo.
Iroha: ¿Es porque has llegado tan lejos por ellas?@¿Rendirse ahora lo haría todo un desperdicio?
Pluma Negra: Sí.
Pluma Negra: La Plumas de aquí han jurado su lealtad@a la liberación más que a Magius.
Pluma Negra: Y de la misma manera...
Pluma Negra: Tenemos fe en lo que ella cree...@Ella nos protegió.
Iroha: ¡¡¡!!!
Sana: ¡Iroha, por aquí!
Iroha: ¡Sana!
Sana: *Quejido* ¡Tenemos que irnos ahora!
Iroha: ¡S-sí!
Iroha: Todo lo que quería era mi Soul Gem de vuelta, pero@pero supongo que solo puedo huir por ahora.
