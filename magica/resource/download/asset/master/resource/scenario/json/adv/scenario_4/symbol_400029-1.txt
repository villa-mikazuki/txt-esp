Nanaka: Así que, estamos en los Espejos Infinitos.@¿Hay alguna diferencia con la última vez...?
Meiyui: ... ¿Qué pasa de repente?
Meiyui: Es básicamente lo mismo.
Meiyui: La bruja sigue negándose a moverse y@no muestra signos de atacar a la gente.
Nanaka: Ya veo.
Meiyui: ¿Sientes algo extraño?
Nanaka: ... Sí, bastante.
Nanaka: ¿Crees que puedan haber varias copias de@una misma alma?
Meiyui: Una pregunta aún más repentina...
Meiyui: Si quieres preguntar acertijos sin sentido,@te sugiero que lo hagas en otro momento.
Nanaka: No, es muy importante.
Meiyui: .......
Meiyui: Supongo que podría ser posible si clonaras@a alguien.
Nanaka: ¿Así que incluso las almas pueden ser@replicadas?
Meiyui: ¿Qué estás tratando de decir?@No me gustan este tipo de rodeos.
Nanaka: Sentí algo. La presencia de un enemigo que@quiere hacerme daño...
Nanaka: Pero eran dos presencias exactamente iguales@a la vez.
Meiyui: ... Sigo sin estar segura de lo que quieres decir.
Nanaka: Cuantos más pisos descendemos,@más fuertes se vuelven las impurezas...
Meiyui: Podría decirse que aumenta en proporción@al tamaño de la mansión.
Meiyui: [chara:301200:effect_emotion_surprise_0][se:7226_shock]¿¡...!?
Meiyui: ... No me digas. ¿Es esto lo que querías decir?
Meiyui: ¿Estás intentando decir que hay varias copias@de los Espejos?
Nanaka: Así es.
Nanaka: Apenas se nota la mayor parte del tiempo,@pero así de siniestro se siente exactamente.
Nanaka: Incluso diría que el hecho de que seamos las@únicas dos del equipo en patrullaje hoy...
Nanaka: ... fue provocado intencionadamente por@una especie de giro negativo del destino.
Meiyui: Solo fue una coincidencia que Kako tuviera@que ocuparse de algo en su casa...
Meiyui: y también lo fue que no pudiéramos contactar@con Akira por su entrenamiento en el dojo.
Nanaka: ¿Realmente lo es...?
Kako: ¡Oh, gracias a Dios! Las encontramos, Akira.
Akira: ¡Tienes razón! ¡OIGAN!
Nanaka: Oh, Kako y Akira.
Nanaka: Creía que ustedes dos tenían un@incoveniente hoy.
Akira: Nah, todo está bien.
Kako: Las cosas también se cancelaron de repente@por mi parte, así que vinimos lo antes posible.
Nanaka: Bueno, podrían habernos avisado de su llegada@un poco antes.
Nanaka: ¿No lo crees, Meiyui?
Meiyui: Cierto.
Meiyui: ... ¿No necesitaron esa flor roja después de todo?
Kako: Sí, todo fue cancelado.
Meiyui: Entonces supongo que me quedaré con@esa flor.
Kako: ¿Eh?
Meiyui: [flashEffect:flashWhite2][se:4061_landing_jump5]¡Hii-YAH!
¿Kako?: [flashEffect:flashRed2][se:6110_hit_slash_05_s][bgEffect:shakeLarge][chara:301100:effect_shake]¡¡Aaagh!!
Akira: ¿¡Meiyui!?
Akira: ¡No me digan que ustedes son clones de@los Espejos...!
Nanaka: Yo creo que esa particular esencia viene de TI,@Akira.
¿Akira?: [chara:300800:effect_emotion_surprise_0][se:7226_shock]¿¡...!?
Nanaka: Ustedes, Familiares...
Nanaka: ¡No se atrevan a hacerse pasar por ellas@otra vez...!
¿Akira?: [flashEffect:flashRed2][se:6004_hit_shooting_03][bgEffect:shakeLarge][chara:300800:effect_shake]¡Gah... aaugh..!
Nanaka: Esas copias fueron ciertamente fáciles de@reconocer.
Meiyui: No pudieron ocultar completamente el rastro@que emitían como Familiares...
Meiyui: Supongo que eso es lo mucho que ya habían@madurado.
Nanaka: Exacto...
Meiyui: .......[wait:1.0][chara:301200:cheek_0][chara:301200:face_mtn_ex_030.exp.json:motion_300][chara:301200:lipSynch_1] Clones...
Meiyui: ¿¡...!?
Nanaka: ¿¡...!?
Meiyui: ¿¡Dónde estamos!?
Nanaka: Deberíamos estar aún en los Espejos.
Nanaka: Pero esto es...
Meiyui: ¿¡Conoces este lugar!?
Nanaka: ¡Es mi casa...!
???: "¡Si alguien empieza a hacer ruido, cállenlos!@¡Si encuentran algún objeto de valor,@tráiganlo aquí!"
???: "¡Muy bien, yo cubriré por aquí!"
???: "¡Átenlos antes de que puedan gritar!"
Nanaka: ¿Qué me están mostrando...?@[chara:300500:lipSynch_0][wait:1.0][chara:300500:cheek_-1][chara:300500:face_mtn_ex_020.exp.json:motion_0][chara:300500:lipSynch_1]¿¡Quiénes son estas personas...!?
Meiyui: .......
Nanaka: ... Parece que los conoces.
Meiyui: Son mi familia...
Meiyui: Se suponía que todos se habían reformado.
Meiyui: ¡Detengan esto AHORA MISMO!
Meiyui: ¿¡Para qué creen que hemos trabajado@tan duro!?
Nanaka: ¡Meiyui, mantén la calma!
Nanaka: Esto solo es...
Nanaka: ¡Esto solo es una especie de ilusión...!
